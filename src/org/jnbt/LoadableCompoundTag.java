package org.jnbt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;

public class LoadableCompoundTag extends CompoundTag {
    
    private File saveFile;
    
    public LoadableCompoundTag(String name, File saveFile) {
        super(name);
        this.saveFile = saveFile;
    }
    
    public LoadableCompoundTag(String name, Map<String, Tag> tag, File saveFile) {
        super(name, tag);
        this.saveFile = saveFile;
    }
    
    public LoadableCompoundTag(String name, Map<String, Tag> tag) {
        super(name, tag);
        saveFile = null;
    }
    
    public void setFile(File file) {
        saveFile = file;
    }
    
    public static LoadableCompoundTag getConfigFile(File file) throws Exception {
        if (!file.exists()) {
            file.createNewFile();
            LoadableCompoundTag tag = new LoadableCompoundTag(file.getName(), file);
            return tag;
        }
        NBTInputStream in = new NBTInputStream(new FileInputStream(file));
        LoadableCompoundTag tag = (LoadableCompoundTag) in.readTag();
        tag.saveFile = file;
        in.close();
        return tag;
    }
    
    public boolean saveTag() {
        try {
            if (!saveFile.exists()) {
                saveFile.createNewFile();
            }
            NBTOutputStream out = new NBTOutputStream(new FileOutputStream(saveFile));
            out.writeTag(this);
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public String toString() {
        String name = getName();
        String append = "";
        if (name != null && !name.equals("")) {
            append = "(\"" + this.getName() + "\")";
        }
        StringBuilder bldr = new StringBuilder();
        bldr.append("TAG_LoadableCompound" + append + ": " + getValue().size() + " entries\r\n{\r\n");
        for (Map.Entry<String, Tag> entry : getValue().entrySet()) {
            bldr.append("   " + entry.getValue().toString().replaceAll("\r\n", "\r\n   ") + "\r\n");
        }
        bldr.append("}");
        return bldr.toString();
    }
    
}
