package org.jnbt;

/*
 * JNBT License
 * 
 * Copyright (c) 2010 Graham Edgecombe
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *       
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       
 *     * Neither the name of the JNBT team nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. 
 */

import java.util.HashMap;
import java.util.Map;

/**
 * The <code>TAG_Compound</code> tag.
 * 
 * @author Graham Edgecombe
 * 
 */
public class CompoundTag extends Tag {
    
    /**
     * The value.
     */
    private final Map<String, Tag> value;
    
    /**
     * Creates the tag.
     * 
     * @param name
     *            The name.
     * @param value
     *            The value.
     */
    public CompoundTag(String name, Map<String, Tag> value) {
        super(name);
        this.value = value;
    }
    
    public CompoundTag(String name) {
        super(name);
        this.value = new HashMap<String, Tag>();
    }
    
    @Override
    public Map<String, Tag> getValue() {
        return value;
    }
    
    public void setTag(Tag tag) {
        value.put(tag.getName(), tag);
    }
    
    public boolean hasValue(String name) {
        return value.containsKey(name);
    }
    
    @Override
    public String toString() {
        String name = getName();
        String append = "";
        if (name != null && !name.equals("")) {
            append = "(\"" + this.getName() + "\")";
        }
        StringBuilder bldr = new StringBuilder();
        bldr.append("TAG_Compound" + append + ": " + value.size() + " entries\r\n{\r\n");
        for (Map.Entry<String, Tag> entry : value.entrySet()) {
            bldr.append("   " + entry.getValue().toString().replaceAll("\r\n", "\r\n   ") + "\r\n");
        }
        bldr.append("}");
        return bldr.toString();
    }
    
    public void setByteArray(String name, byte[] array) {
        setTag(new ByteArrayTag(name, array));
    }
    
    public byte[] getByteArray(String name) {
        Tag tag = value.get(name);
        if (tag instanceof ByteArrayTag) {
            return (byte[]) tag.getValue();
        } else {
            return null;
        }
    }
    
    public byte[] getByteArray(String name, byte[] defalt) {
        byte[] array = getByteArray(name);
        if (array == null) { return defalt; }
        return array;
    }
    
    public void setByte(String name, byte b) {
        setTag(new ByteTag(name, b));
    }
    
    public Byte getByte(String name) {
        Tag tag = value.get(name);
        if (tag instanceof ByteTag) {
            return (byte) tag.getValue();
        } else {
            return null;
        }
    }
    
    public Byte getByte(String name, byte defalt) {
        Byte b = getByte(name);
        if (b == null) { return defalt; }
        return b;
    }
    
    public void setBoolean(String name, Boolean b) {
        setTag(new ByteTag(name, (b == true ? (byte) 1 : (byte) 0)));
    }
    
    public Boolean getBoolean(String name) {
        Tag tag = value.get(name);
        if (tag instanceof ByteTag) {
            ByteTag bt = (ByteTag) tag;
            byte b = bt.getValue();
            if (b == 1) { return true; }
            if (b == 0) { return false; }
        }
        return null;
    }
    
    public Boolean getBoolean(String name, Boolean defalt) {
        Boolean b = getBoolean(name);
        if (b == null) { return defalt; }
        return b;
    }
    
    public void setCompound(String name, CompoundTag tag) {
        setTag(tag);
    }
    
    public CompoundTag getCompoundTag(String name) {
        Tag tag = value.get(name);
        if (tag instanceof CompoundTag) {
            return (CompoundTag) tag;
        } else {
            return null;
        }
    }
    
    public void setDouble(String name, double d) {
        setTag(new DoubleTag(name, d));
    }
    
    public Double getDouble(String name) {
        Tag tag = value.get(name);
        if (tag instanceof DoubleTag) {
            return (double) tag.getValue();
        } else {
            return null;
        }
    }
    
    public Double getDouble(String name, Double defalt) {
        Double d = getDouble(name);
        if (d == null) { return defalt; }
        return d;
    }
    
    public void setFloat(String name, float f) {
        setTag(new FloatTag(name, f));
    }
    
    public Float getFloat(String name) {
        Tag tag = value.get(name);
        if (tag instanceof FloatTag) {
            return (float) tag.getValue();
        } else {
            return null;
        }
    }
    
    public Float getFloat(String name, Float defalt) {
        Float f = getFloat(name);
        if (f == null) { return defalt; }
        return f;
    }
    
    public void setInt(String name, int i) {
        setTag(new IntTag(name, i));
    }
    
    public Integer getInt(String name) {
        
        Tag tag = value.get(name);
        if (tag instanceof IntTag) {
            return (int) tag.getValue();
        } else {
            return null;
        }
    }
    
    public Integer getInt(String name, Integer defalt) {
        Integer i = getInt(name);
        if (i == null) { return defalt; }
        return i;
    }
    
    public void setList(String name, ListTag tag) {
        setTag(tag);
    }
    
    public ListTag getList(String name) {
        
        Tag tag = value.get(name);
        if (tag instanceof ListTag) {
            return (ListTag) tag;
        } else {
            return null;
        }
    }
    
    public void setLong(String name, long l) {
        setTag(new LongTag(name, l));
    }
    
    public Long getLong(String name) {
        Tag tag = value.get(name);
        if (tag instanceof LongTag) {
            return (long) tag.getValue();
        } else {
            return null;
        }
    }
    
    public Long getLong(String name, Long defalt) {
        Long l = getLong(name);
        if (l == null) { return defalt; }
        return l;
    }
    
    public void setShort(String name, short s) {
        setTag(new ShortTag(name, s));
    }
    
    public Short getShort(String name) {
        Tag tag = value.get(name);
        if (tag instanceof ShortTag) {
            return (short) tag.getValue();
        } else {
            return null;
        }
    }
    
    public Short getShort(String name, Short defalt) {
        Short s = getShort(name);
        if (s == null) { return defalt; }
        return s;
    }
    
    public void setString(String name, String s) {
        setTag(new StringTag(name, s));
    }
    
    public String getString(String name) {
        
        Tag tag = value.get(name);
        if (tag instanceof StringTag) {
            return (String) tag.getValue();
        } else {
            return null;
        }
    }
    
    public String getString(String name, String defalt) {
        String s = getString(name);
        if (s == null) { return defalt; }
        return s;
    }
    
    public void removeEntry(String name) {
        value.remove(name);
    }
    
}
