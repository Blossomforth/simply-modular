/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;

import me.corriekay.simplymodular.command.ASimpleCommandMap;
import me.corriekay.simplymodular.events.PlayerNameRemovalListener;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.SimpleWorldManagement.SimpleWorldManager;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.SimpleChestProtect;
import me.corriekay.simplymodular.module.optional.SimpleInvisibility.SimpleInvisibility;
import me.corriekay.simplymodular.utils.*;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class SimplyModular extends JavaPlugin {
    
    private static SimplyModular sm;
    private Options options;
    private FileConfiguration config;
    private ASimpleCommandMap cm;
    private PlayerNameRemovalListener nameListener;
    private SimpleLogger slogger;
    
    private final HashSet<SimpleModule> modules = new HashSet<SimpleModule>();
    
    @SuppressWarnings("deprecation")
    public void onEnable() {
        try {
            PluginDescriptionFile d = this.getDescription();
            
            Field name = PluginDescriptionFile.class.getDeclaredField("name");
            name.setAccessible(true);
            name.set(d, "SimplyModular");
            name.setAccessible(false);
            
            Field authors = PluginDescriptionFile.class.getDeclaredField("authors");
            authors.setAccessible(true);
            ArrayList<String> auth = new ArrayList<String>();
            auth.add("BlossomPone");
            authors.set(d, auth);
            authors.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sm = this;
        File configFile = new File(this.getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            InputStream in = getClassLoader().getResourceAsStream("config.yml");
            getDataFolder().mkdirs();
            try {
                config = YamlConfiguration.loadConfiguration(in);
                config.save(configFile);
            } catch (Exception e) {
                e.printStackTrace();
                getLogger().severe("Error creating plugin configuration file. Self-terminating!");
                Bukkit.getServer().getPluginManager().disablePlugin(this);
                return;
            }
        } else {
            config = YamlConfiguration.loadConfiguration(configFile);
        }
        options = new Options();
        try {
            StringContainer.loadStrings();
        } catch (SimplyModularFatalException e) {
            Debug.logVerbose(e.getMessage(), true);
            e.getException().printStackTrace();
        }
        Debug.logVerbose("Hijacking command map", true);
        try {
            cm = new ASimpleCommandMap();
        } catch (Exception e) {
            e.printStackTrace();
            Debug.log("ERROR HIJACKING COMMAND MAP. UNABLE TO BOOT. Please forward above exception to the plugin developer!", Level.SEVERE);
            Bukkit.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        nameListener = new PlayerNameRemovalListener();
        slogger = new SimpleLogger();
        try {
            //CORE MODULES
            UserManager um = new UserManager();
            modules.add(um);
            
            SimplePermissions sp = new SimplePermissions();
            modules.add(sp);
            
            SimpleWorldManager swm = new SimpleWorldManager();
            modules.add(swm);
            
            {//OPTIONAL MODULES
                List<String> activeModules = getConfig().getStringList("enabledModules");
                if (activeModules.contains("SimpleChestProtect")) {
                    SimpleChestProtect cp = new SimpleChestProtect();
                    modules.add(cp);
                }
                if (activeModules.contains("SimpleInvisibility")) {
                    SimpleInvisibility si = new SimpleInvisibility();
                    modules.add(si);
                }
                if (activeModules.contains("SimpleChatManager")) {
                    SimpleChatChannelManager sccm = new SimpleChatChannelManager();
                    modules.add(sccm);
                }
            }
        } catch (SimplyModularFatalException e) {
            Debug.logDebug(e.getMessage(), Level.SEVERE);
            e.getException().printStackTrace();
        }
    }
    
    public void onDisable() {
        
    }
    
    public static SimplyModular getSM() {
        return sm;
    }
    
    public FileConfiguration getConfig() {
        return getSM().config;
    }
    
    public static ASimpleCommandMap getCommandMap() {
        return sm.cm;
    }
    
    public Options getOptions() {
        return options;
    }
    
    public static PlayerNameRemovalListener getNameListener() {
        return sm.nameListener;
    }
    
    public static SimpleLogger getSimpleLogger() {
        return sm.slogger;
    }
}
