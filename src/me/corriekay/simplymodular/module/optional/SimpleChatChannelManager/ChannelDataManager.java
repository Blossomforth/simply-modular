/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChatChannelManager;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.ChatColor;

public class ChannelDataManager {
    private HashMap<String, SimpleChannel> channels = new HashMap<String, SimpleChannel>();
    private HashMap<String, HashSet<String>> ignores = new HashMap<String, HashSet<String>>();
    private HashMap<String, HashMap<SimpleChannel, ChatColor>> channelColors = new HashMap<String, HashMap<SimpleChannel, ChatColor>>();
    private HashSet<String> muted = new HashSet<String>();
    private SimpleChannel defaultChannel = null;
    private final HashMap<String, String> pmTargets = new HashMap<String, String>();
    private long expiretime;
    
    public SimpleChannel getDefaultChannel() {
        return defaultChannel;
    }
    
    public void setDefaultChannel(SimpleChannel chan) {
        defaultChannel = chan;
    }
    
    public HashMap<String, SimpleChannel> getChannels() {
        return channels;
    }
    
    public void addChannel(SimpleChannel chan) {
        channels.put(chan.getName(), chan);
    }
    
    public void removeChannel(SimpleChannel chan) {
        channels.remove(chan.getName());
    }
    
    public HashMap<String, HashSet<String>> getIgnores() {
        return ignores;
    }
    
    public void addIgnores(String name, HashSet<String> ignored) {
        ignores.put(name, ignored);
    }
    
    public void removeIgnores(String name) {
        ignores.remove(name);
    }
    
    public HashMap<String, HashMap<SimpleChannel, ChatColor>> getChannelColors() {
        return channelColors;
    }
    
    public void addChannelColor(String player, SimpleChannel chan, ChatColor color) {
        HashMap<SimpleChannel, ChatColor> colors = channelColors.get(player);
        if (colors == null) {
            colors = new HashMap<SimpleChannel, ChatColor>();
        }
        colors.put(chan, color);
        channelColors.put(player, colors);
    }
    
    public void removePlayerColors(String player) {
        channelColors.remove(player);
    }
    
    public HashSet<String> getMuted() {
        return muted;
    }
    
    public void setMuted(String player, boolean flag) {
        if (flag) {
            muted.add(player);
        } else {
            muted.remove(player);
        }
    }
    
    public HashMap<String, String> getPmTargets() {
        return pmTargets;
    }
    
    public long getExpiretime() {
        return expiretime;
    }
    
    public void setExpiretime(long expiretime) {
        this.expiretime = expiretime;
    }
}
