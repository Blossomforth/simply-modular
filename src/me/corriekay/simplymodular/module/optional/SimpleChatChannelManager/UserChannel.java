/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChatChannelManager;

import java.util.HashMap;
import java.util.List;

import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class UserChannel extends SimpleChannel {
    private final static HashMap<String, UserChannel> channels = new HashMap<String, UserChannel>();
    
    protected static void removeChannel(UserChannel uc) {
        channels.remove(uc.getName());
    }
    
    private String admin;
    private List<String> mods, banned;
    private long lastActivity;
    private Integer password;
    
    public UserChannel(String channelname, String quick, String icon, String permission, ChatColor color, boolean mature, String admin, List<String> moderators, List<String> banned, long lastActivity, Integer password, SimpleChatChannelManager manager) {
        super(channelname, quick, icon, permission, color, mature, manager);
        setAdmin(admin);
        setMods(moderators);
        setBanned(banned);
        setLastActivity(lastActivity);
        setPassword(password);
        channels.put(channelname, this);
    }
    
    public static boolean isAdminOfAnyChannel(String playername) {
        for (UserChannel chan : channels.values()) {
            if (chan.getAdmin().equals(playername)) { return true; }
        }
        return false;
    }
    
    public String getAdmin() {
        return admin;
    }
    
    public void setAdmin(String admin) {
        this.admin = admin;
    }
    
    public long getLastActivity() {
        return lastActivity;
    }
    
    public void setLastActivity(long lastActivity) {
        
        boolean save = ((Long) this.lastActivity != null) && (this.lastActivity + (1000 * 60 * 5) < System.currentTimeMillis());
        this.lastActivity = lastActivity;
        if (save) save();
    }
    
    public List<String> getBanned() {
        return banned;
    }
    
    public void setBanned(List<String> banned) {
        this.banned = banned;
    }
    
    public List<String> getMods() {
        return mods;
    }
    
    public void setMods(List<String> mods) {
        this.mods = mods;
    }
    
    public Integer getPassword() {
        return password;
    }
    
    public void setPassword(Integer password) {
        this.password = password;
    }
    
    public void save() {
        SimpleChatChannelManager.getModule().saveChannel(this);
    }
    
    public void setColor(ChatColor color) {
        super.color = color;
    }
    
    public boolean isPrivateChannel() {
        return true;
    }
    
    @SuppressWarnings("deprecation")
    public void messageStaff(String message) {
        Player admin = Bukkit.getPlayer(this.admin);
        if (admin != null) {
            admin.sendMessage(getChannelColor() + getIcon() + " " + message);
        }
        for (String modd : mods) {
            Player mod = Bukkit.getPlayer(modd);
            if (mod != null) {
                mod.sendMessage(getChannelColor() + getIcon() + " " + message);
            }
        }
    }
    
    public void messageStaff(StringType type, Object... args) {
        messageStaff(StringContainer.getMessage(type, args));
    }
    
    public void saveChannelInfo(FileConfiguration config) {
        ConfigurationSection section = config.createSection("userChannels." + getName());
        section.set("quick", getQuick());
        section.set("icon", getIcon());
        section.set("color", getChannelColor().name());
        section.set("mature", isMatureFlagged());
        section.set("admin", admin);
        section.set("moderators", mods);
        section.set("banned", banned);
        section.set("lastActivity", lastActivity);
        section.set("password", password);
    }
    
}
