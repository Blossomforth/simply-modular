/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChatChannelManager;

import java.util.*;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.ASimpleCommandMap;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.*;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.*;
import org.jnbt.*;

public class SimpleChatChannelManager extends SimpleModule {
    
    private final ChannelDataManager data = new ChannelDataManager();
    
    public ChannelDataManager getData() {
        return data;
    }
    
    private static SimpleChatChannelManager module;
    
    public static SimpleChatChannelManager getModule() {
        return module;
    }
    
    public SimpleChatChannelManager() throws SimplyModularFatalException {
        super("Channel Manager");
        module = this;
        loadResourceConfig("chatchannels", false);
        registerCommand(new ChannelCommand(this));
        registerCommand(new JoinChannel(this));
        registerCommand(new LeaveChannel(this));
        registerCommand(new MeCommand(this));
        registerCommand(new MuteCommand(this));
        registerCommand(new PmCommand(this));
        registerCommand(new RCommand(this));
        registerCommand(new IgnoreCommand(this));
        registerCommand(new IgnoredCommand(this));
        registerCommand(new PMSpyCommand(this));
        registerCommand(new ChannelColorCommand(this));
        registerCommand(new UCCommand(this));
        initializeChannels();
        initializePlayers();
    }
    
    private void initializeChannels() throws SimplyModularFatalException {
        ASimpleCommandMap map = SimplyModular.getCommandMap();
        for (String channel : getConfig().getConfigurationSection("serverChannels").getKeys(false)) {
            ChatColor color = ChatColor.valueOf(getConfig().getString("serverChannels." + channel + ".color"));
            String icon = getConfig().getString("serverChannels." + channel + ".icon");
            String permission = getConfig().getString("serverChannels." + channel + ".permission", null);
            String quick = getConfig().getString("serverChannels." + channel + ".quick");
            boolean setDefault = getConfig().getBoolean("serverChannels." + channel + ".default", false);
            boolean mature = getConfig().getBoolean("serverChannels." + channel + ".mature", false);
            if (setDefault) {
                if (data.getDefaultChannel() != null) {
                    Debug.logVerbose("Channel " + channel + " attempted to register itsself as the default channel, but there is already a default channel!: " + data.getDefaultChannel().getName() + ". Default flag removed from channel " + channel, true);
                    setDefault = false;
                } else if (permission != null) {
                    Debug.logVerbose("Default channel cannot have a permission! Permission removed from channel " + channel, true);
                    permission = null;
                }
            }
            SimpleChannel c = new SimpleChannel(channel, quick, icon, permission, color, mature, this);
            if (setDefault) {
                data.setDefaultChannel(c);
            }
            data.addChannel(c);
            map.setAlias("channel", c.getQuick());
            map.setAlias("channel", c.getName());
        }
        for (String userChannel : getConfig().getConfigurationSection("userChannels").getKeys(false)) {
            ConfigurationSection section = getConfig().getConfigurationSection("userChannels." + userChannel);
            ChatColor color = ChatColor.valueOf(section.getString("color", "WHITE"));
            String icon = section.getString("icon");
            String quick = section.getString("quick");
            String administrator = section.getString("admin");
            List<String> moderators = section.getStringList("moderators");
            List<String> banned = section.getStringList("banned");
            long lastActivity = section.getLong("lastActivity");
            boolean mature = section.getBoolean("mature");
            Integer password = section.getInt("password");
            UserChannel uc = new UserChannel(userChannel, quick, icon, null, color, mature, administrator, moderators, banned, lastActivity, password, this);
            data.addChannel(uc);
            if (!map.setAlias("channel", uc.getQuick()) || !map.setAlias("channel", uc.getName())) { throw new SimplyModularFatalModuleException("UNABLE TO INITIALIZE PRIVATE CHANNEL, ALIASES ALREADY EXIST FOR CHANNEL NAME: " + uc.getName() + ", OR QUICK: " + uc.getQuick() + ". PLEASE REMOVE THIS CHANNEL MANUALLY", new IllegalArgumentException(), this); }
        }
        if (data.getDefaultChannel() == null) { throw new SimplyModularFatalModuleException("Null Default Channel. Please provide a default channel!", new NullPointerException(), this); }
    }
    
    private void initializePlayers() throws SimplyModularFatalModuleException {
        for (Player player : Bukkit.getOnlinePlayers()) {
            initializePlayer(player);
        }
    }
    
    private void initializePlayer(Player player) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        CompoundTag tag = user.getCompoundTag(this);
        setMuted(player.getName(), tag.getBoolean("muted"));
        SimpleChannel chan = getChannel(user.getCompoundTag(this).getString("chattingChannel"));
        if (chan != null && !chan.joinChat(player, false)) {
            data.getDefaultChannel().joinChat(player, false);
        }
        for (Tag stringTag : tag.getList("listeningChannels").getValue()) {
            String s = ((StringTag) stringTag).getValue();
            SimpleChannel listenChan = getChannel(s);
            if (listenChan != null) {
                listenChan.joinListening(player, false);
            }
        }
        ListTag listens = new ListTag("listeningChannels", StringTag.class);
        for (SimpleChannel c : getListeningChannels(player)) {
            listens.addTag(new StringTag("", c.getName()));
        }
        tag.setList("listeningChannels", listens);
        user.save();
        updateIgnored(player);
        updateChannelColors(user, player.getName());
    }
    
    public void updateChannelColors(Player player) throws SimplyModularFatalModuleException {
        updateChannelColors(getUser(player), player.getName());
    }
    
    public void updateChannelColors(User user, String name) throws SimplyModularFatalModuleException {
        data.removePlayerColors(name);
        CompoundTag colorsCompound = user.getCompoundTag(this).getCompoundTag("channelColors");
        Map<String, Tag> colorsMap = colorsCompound.getValue();
        for (String channelName : colorsMap.keySet()) {
            SimpleChannel channel = getChannel(channelName);
            if (channel == null) {
                continue;
            }
            ChatColor color = ChatColor.valueOf(((StringTag) colorsMap.get(channelName)).getValue());
            if (color == null) {
                continue;
            }
            data.addChannelColor(name, channel, color);
        }
    }
    
    /*
     * Bookmark Checkers
     */
    
    public boolean doSendMessage(Player sender, Player receiver) throws SimplyModularFatalModuleException {
        Group group = getGroupByPlayer(sender);
        if (group.isOpType()) { return true; }
        boolean senderBlock, receiverBlock;
        senderBlock = data.getIgnores().get(sender.getName()).contains(receiver.getName());
        receiverBlock = data.getIgnores().get(receiver.getName()).contains(sender.getName());
        return !(senderBlock || receiverBlock);
    }
    
    public boolean isMuted(Player player) {
        return data.getMuted().contains(player.getName());
    }
    
    /*
     *Bookmark Getters
     */
    public SimpleChannel getChannel(String name) {
        for (SimpleChannel chan : data.getChannels().values()) {
            if (chan.getName().equalsIgnoreCase(name) || chan.getQuick().equalsIgnoreCase(name)) { return chan; }
        }
        return null;
    }
    
    public SimpleChannel getChattingChannel(CommandSender sender) {
        return sender instanceof Player ? (getChattingChannel((Player) sender)) : (getChattingChannel((ConsoleCommandSender) sender));
    }
    
    public SimpleChannel getChattingChannel(ConsoleCommandSender console) {
        return data.getDefaultChannel(); //TODO PLACEHOLDER. Make sure to change once console chat channel has been implemented
    }
    
    public SimpleChannel getChattingChannel(Player player) {
        for (SimpleChannel c : data.getChannels().values()) {
            if (c.isChatting(player)) { return c; }
        }
        return null;
    }
    
    public ArrayList<SimpleChannel> getListeningChannels(Player player) {
        ArrayList<SimpleChannel> channelz = new ArrayList<SimpleChannel>();
        for (SimpleChannel channel : data.getChannels().values()) {
            if (channel.isListening(player)) {
                channelz.add(channel);
            }
        }
        return channelz;
    }
    
    public ChatColor getPlayerColor(String name, SimpleChannel channel) {
        try {
            return data.getChannelColors().get(name).get(channel);
        } catch (NullPointerException e) {
            return null;
        }
    }
    
    public HashSet<String> getIgnoredPlayers(Player player) {
        return data.getIgnores().get(player.getName());
    }
    
    /*
     * Bookmark Setters
     */
    
    public void setMuted(String name, boolean flag) {
        if (flag) {
            data.getMuted().add(name);
        } else {
            data.getMuted().remove(name);
        }
    }
    
    public void updatePMs(String name, String name2) {
        data.getPmTargets().put(name, name2);
        data.getPmTargets().put(name2, name);
    }
    
    public void saveChannel(UserChannel channel) {
        channel.saveChannelInfo(getConfig());
        saveConfig();
    }
    
    /*
     * Bookmark Utils
     */
    public boolean chat(SimpleChannel channel, Player player, String message, boolean log) throws SimplyModularFatalModuleException {
        if (isMuted(player)) {
            StringContainer.sendMessage(player, StringType.chatManagerAttemptChatGlobalMute);
            return false;
        }
        if (channel == null) {
            StringContainer.sendMessage(player, StringType.chatManagerAttemptChatNoChattingChannel);
            return false;
        }
        //TODO channel message event?
        if (channel.broadcastToChannel(player.getName(), player, message, true) == null && channel.isPrivateChannel()) {
            StringContainer.sendMessage(player, StringType.chatManagerAttemptChatChannelMute, channel.getFormattedName());
            return false;
        }
        return true;
    }
    
    public void retireChannel(UserChannel uc) {
        uc.broadcastRawMessage(StringContainer.getMessage(StringType.channelManagerUserChannelRetiringBroadcast), false);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (uc.isChatting(player)) {
                uc.leaveChat(player, true, true);
            }
            if (uc.isListening(player)) {
                uc.leaveListen(player, true);
            }
        }
        data.removeChannel(uc);
        UserChannel.removeChannel(uc);
        getConfig().set(uc.getName(), null);
        saveConfig();
    }
    
    public void updateChannels(Player player) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        SimpleChannel chan = getChattingChannel(player);
        if (chan != null) {
            user.getCompoundTag(this).setString("chattingChannel", chan.getName());
        } else {
            user.getCompoundTag(this).setString("chattingChannel", "null");
        }
        ListTag tag = user.getCompoundTag(this).getList("listeningChannels");
        tag.getValue().clear();
        for (SimpleChannel listen : getListeningChannels(player)) {
            tag.addTag(new StringTag("channel", listen.getName()));
        }
        user.getCompoundTag(this).setList("listeningChannels", tag);
        user.save();
    }
    
    public void updateIgnored(Player player) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        CompoundTag tag = user.getCompoundTag(this);
        HashSet<String> ignores = new HashSet<String>();
        for (Tag ignoretag : tag.getList("ignored").getValue()) {
            ignores.add(((StringTag) ignoretag).getValue());
        }
        data.addIgnores(player.getName(), ignores);
    }
    
    @Override
    public void initializeModuleCompound(CompoundTag tag, User user) {
        tag.setString("chattingChannel", data.getDefaultChannel().getName());
        ListTag listeningChannels = new ListTag("listeningChannels", StringTag.class);
        listeningChannels.addTag(new StringTag("", data.getDefaultChannel().getName()));
        tag.setList("listeningChannels", listeningChannels);
        tag.setBoolean("muted", false);
        tag.setList("ignored", new ListTag("ignored", StringTag.class));
        tag.setBoolean("whiteNames", false);
        tag.setBoolean("pmspy", false);
        tag.setBoolean("matureAccepted", false);
        tag.setBoolean("matureBlocked", false);
        CompoundTag channelColors = new CompoundTag("channelColors");
        tag.setCompound("channelColors", channelColors); //new StringTag(<channelName>, <chatcolor>);
    }
    
    /*
     * Bookmark Listeners
     */
    @SimpleEvent(priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled()) { return; }
        event.setCancelled(true);
        Player player = event.getPlayer();
        SimpleChannel channel = getChattingChannel(player);
        chat(channel, player, event.getMessage(), true);
    }
    
    @SimpleEvent
    public void onJoin(PlayerJoinEvent event) throws SimplyModularFatalModuleException {
        initializePlayer(event.getPlayer());
    }
    
    @SimpleEvent
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        data.getPmTargets().remove(player.getName());
        for (String key : data.getPmTargets().keySet()) {
            String value = data.getPmTargets().get(key);
            if (value == null) {
                continue;
            }
            if (value.equals(player.getName())) {
                data.getPmTargets().put(key, null);
            }
        }
        for (SimpleChannel chan : data.getChannels().values()) {
            chan.leaveChat(player, false, true);
        }
        data.removePlayerColors(player.getName());
    }
}
