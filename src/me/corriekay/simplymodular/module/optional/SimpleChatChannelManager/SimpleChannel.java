/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChatChannelManager;

import java.util.HashSet;
import java.util.logging.Level;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.events.ChatChannelMessageEvent;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

@SuppressWarnings("deprecation")
public class SimpleChannel {
    
    private String channelName, quick, icon, permission;
    protected ChatColor color;
    private final HashSet<String> chatters = new HashSet<String>();
    private final HashSet<String> listeners = new HashSet<String>();
    private final SimpleChatChannelManager chatManager;
    private final ChatColor w = ChatColor.WHITE;
    private boolean mature = false;
    
    public SimpleChannel(String channelname, String quick, String icon, String permission, ChatColor color, boolean mature, SimpleChatChannelManager manager) {
        channelName = channelname;
        this.quick = quick;
        this.icon = icon;
        this.permission = permission;
        this.color = color;
        this.mature = mature;
        chatManager = manager;
    }
    
    /*
     * Bookmark Broadcasters
     */
    public String broadcastToChannel(String who, Player sender, String message, boolean log) throws SimplyModularFatalModuleException {
        String rawmessage = message;
        User user = null;
        if (who != "Server") {
            user = chatManager.getUser(who);
        }
        if (chatManager.getConfig().getBoolean("shortenLinks", false)) {
            //TODO Shorten Links
        }
        if (chatManager.getConfig().getBoolean("greentext", false)) {
            if (message.startsWith(">")) { //TODO implement player choice to disable greentext
                message = ChatColor.GREEN + message;
            }
        }
        String formattedWho = who.equals("Server") ? "Server" : Bukkit.getPlayerExact(who).getDisplayName();//TODO get nickname
        String consoleMessage = color + icon + " " + w + formattedWho + ": " + color + message;
        for (String name : listeners) {
            ChatColor finalColor = getFinalColor(name);
            String finalMessage = finalColor + icon + " " + w + formattedWho + w + ": " + finalColor + message;
            Player player = Bukkit.getPlayerExact(name);
            if (player == null) {
                Debug.logDebug("Chat desync! Player Null: " + name, Level.SEVERE);
                continue;
            }
            if (who.equals("Server") || chatManager.doSendMessage(sender, player)) {
                player.sendMessage(finalMessage);
            }
        }
        Bukkit.getConsoleSender().sendMessage(consoleMessage);
        Bukkit.getPluginManager().callEvent(new ChatChannelMessageEvent(user, rawmessage, this, log));
        if (this instanceof UserChannel) {
            UserChannel uc = (UserChannel) this;
            uc.setLastActivity(System.currentTimeMillis());
        }
        return consoleMessage;
    }
    
    public void broadcastRawMessage(String message, boolean log) {
        if (chatManager.getConfig().getBoolean("shortenLinks", false)) {
            //TODO parse for links
        }
        for (String name : listeners) {
            Player player = Bukkit.getPlayerExact(name);
            if (player == null) {
                Debug.logDebug("Chat desync! Player Null: " + name, Level.SEVERE);
                continue;
            }
            player.sendMessage(message);
        }
        Bukkit.getConsoleSender().sendMessage(message);
        if (this instanceof UserChannel) {
            UserChannel uc = (UserChannel) this;
            uc.setLastActivity(System.currentTimeMillis());
        }
    }
    
    public void broadcastEmote(String message, String name, boolean log) {
        //TODO parsefor links
        String finalEmote = buildEmoteString(message);
        for (String listener : listeners) {
            Player player = Bukkit.getPlayerExact(listener);
            if (player != null) {
                ChatColor c = getFinalColor(player.getName());
                player.sendMessage(c + "* " + name + c + finalEmote);
            }
        }
        Bukkit.getConsoleSender().sendMessage(color + "* " + name + color + finalEmote);
        if (this instanceof UserChannel) {
            UserChannel uc = (UserChannel) this;
            uc.setLastActivity(System.currentTimeMillis());
        }
    }
    
    private String buildEmoteString(String emote) {
        StringBuilder builder = new StringBuilder();
        if (!emote.startsWith("'s")) {
            builder.append(" ");
        }
        builder.append(emote);
        return builder.toString();
    }
    
    /*
     * Bookmark Checkers
     */
    public boolean canJoin(Player player) throws SimplyModularFatalModuleException {
        User user = chatManager.getUser(player);
        if (mature && user.getCompoundTag(chatManager).getBoolean("matureBlocked")) { return false; }
        if (mature && !user.getCompoundTag(chatManager).getBoolean("matureAccepted")) { return false; }
        if (permission == null) { return true; }
        return player.hasPermission(permission);
    }
    
    public boolean isChatting(Player p) {
        return chatters.contains(p.getName());
    }
    
    public boolean isListening(Player p) {
        return listeners.contains(p.getName());
    }
    
    public boolean isPrivateChannel() {
        return false;
    }
    
    public boolean isMatureFlagged() {
        return mature;
    }
    
    /*
     * Method should be reserved for subchannel types as public channels do not mute players, but rely on global mute system to mute players.
     */
    public boolean isMuted(String name) {
        return false;
    }
    
    /*
     * Bookmark Joiners and Leavers
     */
    public boolean joinChat(Player player, boolean notify) throws SimplyModularFatalModuleException {
        if (canJoin(player) && !isChatting(player)) {
            chatters.add(player.getName());
            if (notify) {
                StringContainer.sendMessage(player, StringType.chatManagerJoinChannelChat, getFormattedName());
            }
            if (!isListening(player)) {
                joinListening(player, false);
            }
            return true;
        }
        return false;
    }
    
    public boolean joinListening(Player player, boolean notify) throws SimplyModularFatalModuleException {
        if (canJoin(player) && !isListening(player)) {
            listeners.add(player.getName());
            if (notify) {
                StringContainer.sendMessage(player, StringType.chatManagerJoinChannelListen, getFormattedName());
            }
            return true;
        }
        return false;
    }
    
    public boolean leaveChat(Player player, boolean notify, boolean leaveListen) {
        if (isChatting(player)) {
            chatters.remove(player.getName());
            if (notify) {
                StringContainer.sendMessage(player, StringType.chatManagerLeaveChannelChat, getFormattedName());
            }
            if (leaveListen) {
                leaveListen(player, false);
            }
            return true;
        } else if (leaveListen) {
            leaveListen(player, false);
        }
        return false;
    }
    
    public boolean leaveListen(Player player, boolean notify) {
        if (isListening(player)) {
            listeners.remove(player.getName());
            if (notify) {
                StringContainer.sendMessage(player, StringType.chatManagerLeaveChannelListen, getFormattedName());
            }
            return true;
        }
        return false;
    }
    
    /*
     * Bookmark Getters
     */
    protected HashSet<String> getChatters() {
        return chatters;
    }
    
    protected HashSet<String> getListeners() {
        return listeners;
    }
    
    protected ChatColor getFinalColor(String name) {
        ChatColor finalColor = chatManager.getPlayerColor(name, this);
        return finalColor == null ? color : finalColor;
    }
    
    public ChatColor getChannelColor() {
        return color;
    }
    
    public String getName() {
        return channelName;
    }
    
    public String getFormattedName() {
        return color + getName();
    }
    
    public String getQuick() {
        return quick;
    }
    
    public String toString() {
        return getName();
    }
    
    public String getIcon() {
        return icon;
    }
    
    public String getPermission() {
        return permission;
    }
    
    /*
     * Bookmark Setters
     */
    protected void setName(String nname) {
        channelName = nname;
    }
    
    public void setQuick(String qquick) throws SimplyModularFatalException {
        if (!quick.equals(channelName)) {
            SimplyModular.getCommandMap().removeAlias(quick);
        }
        quick = qquick;
        setIcon("[" + quick + "]");
        SimplyModular.getCommandMap().setAlias("channel", quick);//TODO get actual command name
    }
    
    private void setIcon(String iicon) {
        icon = iicon;
        
    }
    
    protected boolean setMature(boolean flag) throws SimplyModularFatalModuleException {
        if (flag) {
            for (String listener : listeners) {
                User user = chatManager.getUser(listener);
                if (user.getCompoundTag(chatManager).getBoolean("matureBlocked") || !user.getCompoundTag(chatManager).getBoolean("matureAccepted")) {
                    Player player = Bukkit.getPlayerExact(listener);
                    if (player != null) {
                        if (!leaveChat(player, false, true)) {
                            leaveListen(player, false);
                        }
                    }
                }
            }
        }
        mature = flag;
        return flag;
    }
}
