/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChestProtect;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Protection {
    protected String owner;
    protected List<String> havePermission = new ArrayList<String>();
    protected LockType type;
    protected String world;
    
    protected Location loc;
    
    public static enum LockType {
        PUBLIC, PRIVATE, DONATION;
    }
    
    public String getOwner() {
        return owner;
    }
    
    public void setOwner(String name) {
        owner = name;
        
        //TODO make sure to remove the protection from block Possibly by deleting protection, and then adding a new protection?
    }
    
    public List<String> getHasPermission() {
        return havePermission;
    }
    
    public LockType getType() {
        return type;
    }
    
    public void setLockType(LockType type) {
        this.type = type;
    }
    
    public boolean canAccess(Player player) {
        return canModify(player) || havePermission.contains(player.getName());
    }
    
    public boolean canModify(Player player) {
        if (player.getName().equals(owner) || player.hasPermission("simplymodular.simplechestprotect.admin")) { return true; }
        return false;
    }
}
