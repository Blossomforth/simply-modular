/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChestProtect;

import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.simplymodular.utils.Debug;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class ChestProtectOptions {
    private final FileConfiguration config = SimpleChestProtect.getSCP().getConfig();
    
    private final HashSet<String> ignoredWorlds = new HashSet<String>();
    
    public boolean isIgnoredWorld(String worldname) {
        return ignoredWorlds.contains(worldname);
    }
    
    public void addIgnoredWorld(String worldname) {
        ignoredWorlds.add(worldname);
    }
    
    public void removeIgnoredWorld(String worldname) {
        ignoredWorlds.remove(worldname);
    }
    
    private int secondsToClearCache;
    
    public void setCacheClear(int seconds) {
        secondsToClearCache = seconds;
    }
    
    public int getCacheClear() {
        return secondsToClearCache;
    }
    
    private final HashSet<Material> allowedTypes = new HashSet<Material>();
    
    public boolean isAllowedBlockType(Material type) {
        return allowedTypes.contains(type);
    }
    
    public void addAllowedType(Material type) {
        allowedTypes.add(type);
    }
    
    public void removeAllowedType(Material type) {
        allowedTypes.remove(type);
    }
    
    private final HashMap<String, Integer> limitations = new HashMap<String, Integer>();
    
    public int getGlobalLimit() {
        Integer glimit = limitations.get("GLOBAL");
        if (glimit == null) {
            return 0;
        } else if (glimit > 0) {
            return Integer.MAX_VALUE;
        } else {
            return glimit;
        }
    }
    
    public int getSpecificLimit(Material type) {
        Integer limit = limitations.get(type.name());
        if (limit == null) {
            return getGlobalLimit();
        } else if (limit < 0) {
            return Integer.MAX_VALUE;
        } else {
            return limit;
        }
    }
    
    public void setGlobalLimit(int limit) {
        limitations.put("GLOBAL", limit);
    }
    
    public void setOtherLimit(Material type, int limit) {
        limitations.put(type.name(), limit);
    }
    
    private boolean unregisterProhibitedTypes;
    
    public boolean unregisterProbhibitedTypes() {
        return unregisterProhibitedTypes;
    }
    
    public ChestProtectOptions() {
        for (String type : config.getStringList("options.types")) {
            Material material = Material.valueOf(type);
            addAllowedType(material);
        }
        setGlobalLimit(config.getInt("options.limitations.Global", 0));
        for (String type : config.getConfigurationSection("options.limitations.blocks").getKeys(false)) {
            Material material = Material.valueOf(type);
            if (!isAllowedBlockType(material)) {
                Debug.logVerbose("Block type \"" + type + "\" registered, but is not allowed type. Ignoring!", true);
            } else {
                setOtherLimit(material, config.getInt("options.limitations.blocks." + type));
            }
        }
        for (String world : config.getStringList("options.ignoredWorlds")) {
            addIgnoredWorld(world);
        }
        setCacheClear(config.getInt("options.secondsToClearInactiveCache"));
        unregisterProhibitedTypes = config.getBoolean("options.removeOnLoadInvalidBlock", true);
    }
}
