/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChestProtect;

import java.util.HashMap;

import me.corriekay.simplymodular.utils.Debug;

import org.bukkit.Location;

public class ChunkProtectionCache {
    HashMap<String, Protection> protectionCache = new HashMap<String, Protection>();
    private long timer;
    
    public ChunkProtectionCache() {
        updateTimer();
    }
    
    public void addProtection(Protection prot) {
        Debug.logDebug("Adding protection, location string: " + getLocationString(prot.loc));
        protectionCache.put(getLocationString(prot.loc), prot);
    }
    
    public void addSecondaryProtection(Protection prot, Location blocklocation) {
        Debug.logDebug("Adding secondary protection for multiblock protection: " + getLocationString(prot.loc));
        protectionCache.put(getLocationString(blocklocation), prot);
    }
    
    public Protection getProtection(Location loc) {
        return protectionCache.get(getLocationString(loc));
    }
    
    public void removeProtection(Location loc) {
        protectionCache.remove(getLocationString(loc));
    }
    
    private String getLocationString(Location loc) {
        StringBuilder sb = new StringBuilder();
        sb.append(loc.getWorld().getName());
        sb.append(loc.getX());
        sb.append(loc.getY());
        sb.append(loc.getZ());
        return sb.toString();
    }
    
    protected long getTimer() {
        return timer;
    }
    
    protected void updateTimer() {
        timer = System.currentTimeMillis();
    }
    
    protected int DEBUGgetProtectionCount() {
        return protectionCache.size();
    }
}
