/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChestProtect;

import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.simplymodular.SimplyModular;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitTask;

public class ChunkProtectionCacheManager {
    
    private final long secondsToClearCache;
    private BukkitTask task;
    private boolean activeTask = false;
    
    private final HashMap<String, ChunkProtectionCache> activecache = new HashMap<String, ChunkProtectionCache>();
    private final HashMap<String, ChunkProtectionCache> inactivecache = new HashMap<String, ChunkProtectionCache>();
    
    public ChunkProtectionCacheManager(int clearCache) {
        secondsToClearCache = clearCache * 1000;
        
    }
    
    public ChunkProtectionCache getActiveCachedProtections(Chunk chunk) {
        return activecache.get(chunkToString(chunk));
    }
    
    protected void loadChunkProtectionsIntoCache(Chunk chunk, ChunkProtectionCache cache) {
        activecache.put(chunkToString(chunk), cache);
    }
    
    public void cacheChunk(Chunk chunk) {
        String chunkstring = chunkToString(chunk);
        ChunkProtectionCache cache = activecache.get(chunkstring);
        if (cache == null) { return; }
        cache.updateTimer();
        activecache.remove(chunkstring);
        inactivecache.put(chunkstring, cache);
        startTask();
    }
    
    public void startTask() {
        if (activeTask) {
            return;
        } else {
            activeTask = true;
        }
        task = Bukkit.getScheduler().runTaskTimer(SimplyModular.getSM(), new Runnable() {
            HashSet<String> removes = new HashSet<String>();
            
            public void run() {
                if (inactivecache.size() == 0) {
                    task.cancel();
                    activeTask = false;
                    return;
                }
                removes.clear();
                for (String chunkString : inactivecache.keySet()) {
                    ChunkProtectionCache cache = inactivecache.get(chunkString);
                    long lastUpdate = cache.getTimer();
                    lastUpdate += secondsToClearCache;
                    if (lastUpdate < System.currentTimeMillis()) {
                        removes.add(chunkString);
                    }
                }
                for (String remove : removes) {
                    inactivecache.remove(remove);
                }
            }
        }, 0, 100);
    }
    
    protected boolean reviveCache(Chunk chunk) {
        String chunkString = chunkToString(chunk);
        ChunkProtectionCache cache = inactivecache.get(chunkString);
        if (cache == null) { return false; }
        cache.updateTimer();
        activecache.put(chunkString, cache);
        inactivecache.remove(chunkString);
        return true;
    }
    
    protected void forceUnload(Chunk chunk, Boolean unloadActively) {
        String chunkstring = chunkToString(chunk);
        inactivecache.remove(chunkstring);
        if (unloadActively) {
            activecache.remove(chunkstring);
        }
    }
    
    public static String chunkToString(Chunk chunk) {
        return chunk.getX() + "," + chunk.getZ();
    }
    
    public static String blockToString(Block block) {
        return block.getX() + "," + block.getY() + "," + block.getZ();
    }
}
