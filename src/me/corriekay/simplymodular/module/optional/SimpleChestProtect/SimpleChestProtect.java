/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleChestProtect;

import java.util.*;

import me.corriekay.simplymodular.command.commands.SimpleChestProtect.*;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.Protection.LockType;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.jnbt.*;

@SuppressWarnings("deprecation")
public class SimpleChestProtect extends SimpleModule {
    
    //World, Manager
    private final HashMap<String, ChunkProtectionCacheManager> cacheManagers = new HashMap<String, ChunkProtectionCacheManager>();
    
    //Options Manager
    private final ChestProtectOptions cpo;
    
    public static ChestProtectOptions getOptions() {
        return scp.cpo;
    }
    
    //Chest Protect Object
    private static SimpleChestProtect scp;
    
    public static SimpleChestProtect getSCP() {
        return scp;
    }
    
    public SimpleChestProtect() throws SimplyModularFatalModuleException {
        super("SimpleChestProtect");
        scp = this;
        loadResourceConfig("chestprotections", true);
        getConfig().options().header("DO NOT EDIT \"chunks\" VALUE");
        cpo = new ChestProtectOptions();
        
        for (String s : getConfig().getStringList("options.ignoredWorlds")) {
            cpo.addIgnoredWorld(s);
        }
        initializeChunks();
        registerCommand(new Protect(this));
        registerCommand(new Unprotect(this));
        registerCommand(new BuildProtection(this));
    }
    
    private void initializeChunks() throws SimplyModularFatalModuleException {
        for (World world : Bukkit.getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                loadChunkProtections(chunk);
            }
        }
    }
    
    public void reloadChunkProtections(Chunk chunk) throws SimplyModularFatalModuleException {
        ChunkProtectionCacheManager cpcm = cacheManagers.get(chunk.getWorld().getName());
        cpcm.forceUnload(chunk, true);
        loadChunkProtections(chunk);
        
    }
    
    public void loadChunkProtections(Chunk chunk) throws SimplyModularFatalModuleException {
        String worldname = chunk.getWorld().getName();
        if (cpo.isIgnoredWorld(worldname)) { return; }
        
        ChunkProtectionCacheManager cm = loadOrCreateWorldManager(chunk.getWorld());
        cm.forceUnload(chunk, true);
        if (cm.reviveCache(chunk)) { return; }
        
        ChunkProtectionCache chunkCache = new ChunkProtectionCache();
        
        String chunkstring = chunk.getX() + "," + chunk.getZ();
        ConfigurationSection chunks = getConfig().getConfigurationSection("chunks");
        
        List<String> zCoords = chunks.getStringList(chunk.getWorld().getName() + "." + chunk.getX() + "." + chunk.getZ());
        for (String name : zCoords) {
            User user = getUser(name);
            CompoundTag chunktag = user.getCompoundTag(this).getCompoundTag(chunk.getWorld().getName()).getCompoundTag(chunkstring);
            
            for (Tag tag : chunktag.getValue().values()) {
                CompoundTag protectiontag = (CompoundTag) tag;
                Protection p = new Protection();
                p.owner = name;
                p.type = LockType.valueOf(protectiontag.getString("locktype"));
                
                List<String> perms = new ArrayList<String>();
                for (Tag t : protectiontag.getList("havePermission").getValue()) {
                    perms.add(((StringTag) t).getValue());
                }
                p.havePermission = perms;
                
                Location loc = new Location(chunk.getWorld(), protectiontag.getInt("x"), protectiontag.getInt("y"), protectiontag.getInt("z"));
                
                p.loc = loc;
                
                chunkCache.addProtection(p);
            }
        }
        cm.loadChunkProtectionsIntoCache(chunk, chunkCache);
        
        int removedProtections = 0;
        for (Protection protection : chunkCache.protectionCache.values()) {
            Block block = protection.loc.getBlock();
            if (!cpo.isAllowedBlockType(block.getType())) {
                deleteBlockProtection(protection, false, false);
                removedProtections++;
            }
        }
        if (removedProtections > 0) {
            reloadChunkProtections(chunk);
        }
    }
    
    private ChunkProtectionCacheManager loadOrCreateWorldManager(World w) {
        if (cpo.isIgnoredWorld(w.getName())) { return null; }
        ChunkProtectionCacheManager cm = cacheManagers.get(w.getName());
        if (cm == null) {
            cm = new ChunkProtectionCacheManager(getConfig().getInt("options.secondsToClearInactiveCache"));
            cacheManagers.put(w.getName(), cm);
        }
        return cm;
    }
    
    /**
     * Checks if a player can protect additional blocks.
     * @param player The player who is being tested against
     * @param world The world that the player is in
     * @param block The block that the player is attempting to protect. If no block specified, pass in null.
     * @param feedback Whether or not the player should be told  why they cannot protect additional blocks
     * @return whether or not a player can protect additional blocks. note; if no block specified, the method does not check against specific block types.
     * @throws SimplyModularFatalModuleException This shouldnt happen, but if it does, its not the fault of this module.
     */
    public boolean canProtectAdditionalBlock(Player player, World world, Block block, boolean feedback) throws SimplyModularFatalModuleException {
        if (cpo.isIgnoredWorld(world.getName())) {
            if (feedback) {
                StringContainer.sendMessage(player, StringContainer.StringType.protectIgnoredWorld, world.getName());
            }
            return false;
        }
        if (cpo.getGlobalLimit() <= getPlayerGlobalProtectionCount(player)) {
            if (feedback) {
                StringContainer.sendMessage(player, StringContainer.StringType.protectGlobalCapViolated, cpo.getGlobalLimit());
            }
            return false;
        }
        if (block != null) {
            Material type = block.getType();
            if (cpo.getSpecificLimit(type) <= getPlayerBlocktypeProtectionCounts(type, player)) {
                if (feedback) {
                    StringContainer.sendMessage(player, StringContainer.StringType.protectSpecificCapViolated, type.name(), cpo.getSpecificLimit(type));
                }
                return false;
            }
        }
        return true;
    }
    
    public int getPlayerGlobalProtectionCount(Player player) throws SimplyModularFatalModuleException {
        int protections = 0;
        User user = getUser(player);
        CompoundTag tag = user.getCompoundTag(this);
        for (Tag worldTag : tag.getValue().values()) {
            CompoundTag worldCTag = (CompoundTag) worldTag;
            for (Tag chunktag : worldCTag.getValue().values()) {
                CompoundTag chunkCTag = (CompoundTag) chunktag;
                protections += chunkCTag.getValue().size();
            }
        }
        return protections;
    }
    
    public int getPlayerBlocktypeProtectionCounts(Material type, Player player) throws SimplyModularFatalModuleException {
        int protections = 0;
        User user = getUser(player);
        CompoundTag tag = user.getCompoundTag(this);
        for (Tag worldTag : tag.getValue().values()) {
            CompoundTag worldCTag = (CompoundTag) worldTag;
            for (Tag chunktag : worldCTag.getValue().values()) {
                CompoundTag chunkCTag = (CompoundTag) chunktag;
                for (Tag blocktag : chunkCTag.getValue().values()) {
                    CompoundTag blockCTag = (CompoundTag) blocktag;
                    Location l = new Location(Bukkit.getWorld(worldTag.getName()), blockCTag.getInt("x"), blockCTag.getInt("y"), blockCTag.getInt("z"));
                    if (l.getBlock().getType() == type) {
                        protections++;
                    }
                }
            }
        }
        return protections;
    }
    
    public void addBlockProtection(String player, Block block, boolean feedback, Protection.LockType locktype, List<String> havePermission) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        CompoundTag basetag = user.getCompoundTag(this);
        String blockstring = ChunkProtectionCacheManager.blockToString(block);
        
        World world = block.getWorld();
        CompoundTag worldtag = basetag.getCompoundTag(world.getName());
        if (worldtag == null) {
            worldtag = new CompoundTag(world.getName());
            basetag.setCompound(world.getName(), worldtag);
        }
        
        Chunk chunk = block.getChunk();
        String chunkstring = ChunkProtectionCacheManager.chunkToString(chunk);
        CompoundTag chunktag = worldtag.getCompoundTag(chunkstring);
        if (chunktag == null) {
            chunktag = new CompoundTag(chunkstring);
            worldtag.setCompound(chunkstring, chunktag);
        }
        
        CompoundTag blocktag = new CompoundTag(blockstring);
        ListTag permissiontag = new ListTag("havePermission", StringTag.class);
        for (String havePerm : havePermission) {
            permissiontag.addTag(new StringTag("havePermission", havePerm));
        }
        blocktag.setString("locktype", locktype.name());
        blocktag.setList("havePermission", permissiontag);
        blocktag.setInt("x", block.getX());
        blocktag.setInt("y", block.getY());
        blocktag.setInt("z", block.getZ());
        
        chunktag.setCompound(blockstring, blocktag);
        user.save();
        
        OfflinePlayer oplayer = Bukkit.getOfflinePlayer(player);
        if (feedback && oplayer.isOnline()) {
            StringContainer.sendMessage((Player) oplayer, StringContainer.StringType.protectSuccess, block.getX() + ":" + block.getY() + ":" + block.getZ(), block.getType().name());
        }
        
        List<String> list = getConfig().getStringList("chunks." + chunk.getWorld().getName() + "." + chunk.getX() + "." + chunk.getZ());
        if (!list.contains(player)) {
            list.add(player);
        }
        getConfig().set("chunks." + chunk.getWorld().getName() + "." + chunk.getX() + "." + chunk.getZ(), list);
        saveConfig();
        reloadChunkProtections(chunk);
    }
    
    @SuppressWarnings("rawtypes")
    public void deleteBlockProtection(Protection protection, boolean notifyOwner, boolean reload) throws SimplyModularFatalModuleException {
        User user = getUser(protection.owner);
        Block block = protection.loc.getBlock();
        Chunk chunk = block.getChunk();
        CompoundTag baseTag = user.getCompoundTag(this);
        CompoundTag worldtag = baseTag.getCompoundTag(chunk.getWorld().getName());
        CompoundTag chunktag = worldtag.getCompoundTag(ChunkProtectionCacheManager.chunkToString(chunk));
        
        chunktag.removeEntry(locationToString(protection.loc.getBlock()));
        
        if (chunktag.getValue().size() == 0) {
            getConfig().set("chunks." + block.getWorld().getName() + "." + chunk.getX() + "." + chunk.getZ(), null);
            List chunkx = getConfig().getList("chunks." + block.getWorld().getName() + "." + chunk.getX());
            if (chunkx == null || chunkx.isEmpty()) {
                getConfig().set("chunks." + block.getWorld().getName() + "." + chunk.getX(), null);
                List worldList = getConfig().getList("chunks." + block.getWorld().getName());
                if (worldList == null || worldList.isEmpty()) {
                    getConfig().set("chunks." + block.getWorld().getName(), null);
                }
            }
            saveConfig();
            worldtag.removeEntry(chunktag.getName());
        }
        if (worldtag.getValue().size() == 0) {
            baseTag.removeEntry(worldtag.getName());
        }
        
        user.save();
        
        if (notifyOwner) {
            Player owner = Bukkit.getPlayer(protection.owner);
            if (owner.isOnline()) {
                StringContainer.sendMessage(owner, StringType.protectDestroyProtectionNotifyOwner, block.getType().name(), locationToString(block));
            }
        }
        if (reload) {
            reloadChunkProtections(chunk);
        }
    }
    
    @SimpleEvent
    public void onChunkLoad(ChunkLoadEvent event) throws SimplyModularFatalModuleException {
        Chunk chunk = event.getChunk();
        loadChunkProtections(chunk);
    }
    
    @SimpleEvent
    public void onChunkUnload(ChunkUnloadEvent event) {
        Chunk chunk = event.getChunk();
        ChunkProtectionCacheManager cm = loadOrCreateWorldManager(chunk.getWorld());
        if (cm != null) {
            cm.cacheChunk(chunk);
        }
    }
    
    public Protection getBlockProtection(Block block, boolean alreadyRecursed) throws SimplyModularFatalModuleException {
        if (!cpo.isIgnoredWorld(block.getWorld().getName()) && cpo.isAllowedBlockType(block.getType())) {
            Chunk chunk = block.getChunk();
            World world = block.getWorld();
            ChunkProtectionCacheManager cpcm = this.cacheManagers.get(world.getName());
            ChunkProtectionCache cpc = cpcm.getActiveCachedProtections(chunk);
            if (cpc == null) {
                this.loadChunkProtections(chunk);
                cpc = cpcm.getActiveCachedProtections(chunk);
            }
            Protection protection = cpc.getProtection(block.getLocation());
            
            if (protection == null && !alreadyRecursed) {
                if (block.getType() == Material.CHEST) {
                    Chest chest = (Chest) block.getState();
                    if (chest.getInventory().getHolder() instanceof DoubleChest) {
                        DoubleChest dc = (DoubleChest) chest.getInventory().getHolder();
                        Block target = ((Chest) dc.getLeftSide()).getLocation().equals(block.getLocation()) ? ((Chest) dc.getRightSide()).getBlock() : ((Chest) dc.getLeftSide()).getBlock();
                        return getBlockProtection(target, true);
                    }
                }
            }
            
            return protection;
        }
        return null;
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled()) { return; }
        Protection p = getBlockProtection(event.getBlock(), false);
        if (p != null) {
            Player player = event.getPlayer();
            String playername = player.getName();
            //TODO modify when modular permissions system added.
            if (p.owner.equals(playername) || player.hasPermission("simplechestprotect.admin")) {
                if (event.getBlock().getState() instanceof Chest && ((Chest) event.getBlock().getState()).getInventory().getHolder() instanceof DoubleChest) {
                    DoubleChest dc = (DoubleChest) ((Chest) event.getBlock().getState()).getInventory().getHolder();
                    Block left = ((Chest) dc.getLeftSide()).getBlock();
                    Block right = ((Chest) dc.getRightSide()).getBlock();
                    Block protectme;
                    if (left.getLocation().equals(event.getBlock().getLocation())) {
                        protectme = right;
                    } else {
                        protectme = left;
                    }
                    
                    Protection lProt, rProt;
                    lProt = getBlockProtection(left, false);
                    rProt = getBlockProtection(right, false);
                    
                    if (lProt.loc.equals(left.getLocation())) {
                        deleteBlockProtection(getBlockProtection(left, false), false, true);
                    }
                    if (rProt.loc.equals(right.getLocation())) {
                        deleteBlockProtection(getBlockProtection(right, false), false, true);
                    }
                    
                    addBlockProtection(p.owner, protectme, false, p.type, p.havePermission);
                } else {
                    deleteBlockProtection(p, p.owner.equals(playername), true);
                }
            } else {
                StringContainer.sendMessage(player, StringType.protectDestroyProtectionDenyNotOwner);
                event.setCancelled(true);
            }
        }
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void interact(EntityInteractEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled()) { return; }
        if (cpo.isAllowedBlockType(event.getBlock().getType())) {
            Block block = event.getBlock();
            Protection protection = getBlockProtection(block, false);
            if (protection != null) {
                if (protection.type == LockType.PRIVATE) {
                    event.setCancelled(true);
                }
            }
        }
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void playerInteract(PlayerInteractEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled()) { return; }
        if (event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (cpo.isAllowedBlockType(event.getClickedBlock().getType())) {
                Protection protection = getBlockProtection(event.getClickedBlock(), false);
                if (protection != null) {
                    Player player = event.getPlayer();
                    if (protection.type == LockType.PRIVATE) {
                        if (!(protection.owner.equals(player.getName()) || protection.havePermission.contains(player.getName()))) {
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void explosion(EntityExplodeEvent event) throws SimplyModularFatalModuleException {
        if (event.blockList().isEmpty() || event.isCancelled() || cpo.isIgnoredWorld(event.getLocation().getWorld().getName())) { return; }
        HashSet<Block> removeMe = new HashSet<Block>();
        for (Block block : event.blockList()) {
            Protection protection = getBlockProtection(block, false);
            if (protection != null) {
                removeMe.add(block);
            }
        }
        for (Block block : removeMe) {
            event.blockList().remove(block);
        }
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void blockBurn(BlockBurnEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled() || cpo.isIgnoredWorld(event.getBlock().getWorld().getName())) { return; }
        Protection protection = getBlockProtection(event.getBlock(), false);
        if (protection != null) {
            event.setCancelled(true);
        }
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void openInventory(InventoryOpenEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled()) { return; }
        Block block = null;
        if (event.getInventory().getHolder() instanceof BlockState) {
            block = ((BlockState) event.getInventory().getHolder()).getBlock();
        } else if (event.getInventory().getHolder() instanceof DoubleChest) {
            block = ((DoubleChest) event.getInventory().getHolder()).getLocation().getBlock();
        } else {
            return;
        }
        Protection protection = getBlockProtection(block, false);
        if (protection != null) {
            if (!(event.getPlayer() instanceof Player)) {
                event.setCancelled(true);
            }
            Player player = (Player) event.getPlayer();
            if (protection.type == LockType.PRIVATE) {
                if (!(protection.getOwner().equals(player.getName()) || protection.getHasPermission().contains(player.getName()))) {
                    event.setCancelled(true);
                }
            }
        }
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void clickInventory(InventoryClickEvent event) throws SimplyModularFatalModuleException {
        if (event.isCancelled()) { return; }
        Block block = null;
        if (event.getInventory().getHolder() instanceof BlockState) {
            block = ((BlockState) event.getInventory().getHolder()).getBlock();
        } else if (event.getInventory().getHolder() instanceof DoubleChest) {
            block = ((DoubleChest) event.getInventory().getHolder()).getLocation().getBlock();
        }
        if (block != null) {
            Protection protection = getBlockProtection(block, false);
            if (protection == null) { return; }
            if (!(event.getWhoClicked() instanceof Player)) {
                event.setCancelled(true);
                return;
            }
            Player player = (Player) event.getWhoClicked();
            if (protection.type == LockType.PRIVATE) {
                if (protection.canAccess(player)) { return; }
            }
            if (protection.type == LockType.DONATION) {
                if (event.getSlot() != event.getRawSlot()) {//Props to hidendra; nice trick!
                    return;
                }
                if (event.getSlotType() == InventoryType.SlotType.CONTAINER) {// @formatter:off
					InventoryAction action = event.getAction();
					if ((action == InventoryAction.SWAP_WITH_CURSOR || //If action is prohibited for a donation chest, and the player does not have permission to access, deny.
							action == InventoryAction.MOVE_TO_OTHER_INVENTORY || 
							action == InventoryAction.COLLECT_TO_CURSOR ||
							action == InventoryAction.HOTBAR_MOVE_AND_READD ||
							action == InventoryAction.HOTBAR_SWAP ||
							action == InventoryAction.PICKUP_ALL ||
							action == InventoryAction.PICKUP_HALF ||
							action == InventoryAction.PICKUP_ONE ||
							action == InventoryAction.PICKUP_SOME)
							&& !protection.canAccess(player)) {
						event.setCancelled(true);
						return;
					}// @formatter:on
                }
            }
        }
    }
    
    public void initializeModuleCompound(CompoundTag tag, User user) {}
    
    public String chunkToString(Chunk chunk) {
        return ChunkProtectionCacheManager.chunkToString(chunk);
    }
    
    public static String locationToString(Block block) {
        return block.getX() + "," + block.getY() + "," + block.getZ();
    }
    
}
