/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.optional.SimpleInvisibility;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.commands.SimpleInvisibility.Hide;
import me.corriekay.simplymodular.command.commands.SimpleInvisibility.SetInvisLevel;
import me.corriekay.simplymodular.events.*;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.*;
import org.jnbt.CompoundTag;

public class SimpleInvisibility extends SimpleModule {
    
    private final List<String> invisiblePlayers = new ArrayList<String>();
    private final SimpleListener targetListener = new SimpleListener() {
        @SimpleEvent
        public void onTarget(EntityTargetEvent event) {
            if (event.getTarget() instanceof Player) {
                if (isHidden(((Player) event.getTarget()).getName())) {
                    event.setCancelled(true);
                }
            }
        }
    };
    
    private void updateTargetListener() {
        if (invisiblePlayers.size() > 0) {
            targetListener.registerEvents();
        } else {
            targetListener.unregister();
        }
    }
    
    private final List<String> noPickup = new ArrayList<String>();
    private final List<String> pickupFlagAutoModified = new ArrayList<String>();
    private final SimpleListener pickupListener = new SimpleListener() {
        @SimpleEvent
        public void onPickup(PlayerPickupItemEvent event) {
            if (noPickup.contains(event.getPlayer().getName())) {
                event.setCancelled(true);
            }
        }
    };
    
    private void updatePickupListener() {
        if (noPickup.size() > 0) {
            pickupListener.registerEvents();
        } else {
            pickupListener.unregister();
        }
    }
    
    private static SimpleInvisibility si;
    
    public static SimpleInvisibility getModule() {
        return si;
    }
    
    public SimpleInvisibility() throws SimplyModularFatalException {
        super("SimpleInvisibility");
        si = this;
        loadResourceConfig("invisibility", false);
        
        String alias;
        ArrayList<String> labels;
        
        alias = getConfig().getString("fakeHideAlias", "fakehide");
        labels = SimpleCommandMetadata.hide.getLabels();
        if (!labels.contains(alias)) {
            labels.add(alias);
            SimpleCommandMetadata.hide.setLabels(labels);
        }
        registerCommand(new Hide(alias, this));
        
        alias = getConfig().getString("defaultInvisLevelAlias", "defaultinvislevel");
        labels = SimpleCommandMetadata.setinvislevel.getLabels();
        if (!labels.contains(alias)) {
            labels.add(alias);
            SimpleCommandMetadata.setinvislevel.setLabels(labels);
        }
        registerCommand(new SetInvisLevel(alias, this));
        calculateInvisibilityAll(true, false);
    }
    
    public void calculateInvisibilityAll(boolean loggingIn, boolean permissionsChanged) throws SimplyModularFatalModuleException {
        for (Player player : Bukkit.getOnlinePlayers()) {
            calculateInvisibility(player, false, loggingIn, permissionsChanged);
        }
    }
    
    public boolean isPickingUp(String player) {
        return !noPickup.contains(player);
        
    }
    
    public void togglePickup(Player player, boolean notify) {
        pickupFlagAutoModified.remove(player.getName());
        if (isPickingUp(player.getName())) {
            pickupOff(player, notify);
        } else {
            pickupOn(player, notify);
        }
    }
    
    private void pickupOff(Player player, boolean notify) {
        noPickup.add(player.getName());
        if (notify) StringContainer.sendMessage(player, StringType.pickupDisabled);
        updatePickupListener();
    }
    
    private void pickupOn(Player player, boolean notify) {
        noPickup.remove(player.getName());
        if (notify) StringContainer.sendMessage(player, StringType.pickupEnabled);
        updatePickupListener();
    }
    
    public boolean isHidden(String player) {
        if (invisiblePlayers.contains(player)) {
            return true;
        } else return false;
    }
    
    public void toggleInvisibility(Player player, boolean silent, boolean notify, boolean loggingIn) throws SimplyModularFatalModuleException {
        if (isHidden(player.getName())) {
            turnOff(player, silent, notify, loggingIn, false);
        } else {
            turnOn(player, silent, notify, loggingIn, false);
        }
    }
    
    private void calculateInvisibility(Player player, boolean notify, boolean loggingIn, boolean permissionsChanged) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        if (user.getCompoundTag(this).getBoolean("invisible") && player.hasPermission(SimpleCommandMetadata.hide.getPermission())) {
            turnOn(player, true, notify, loggingIn, permissionsChanged);
        } else {
            turnOff(player, true, notify, loggingIn, permissionsChanged);
        }
    }
    
    private void turnOn(Player player, boolean silent, boolean notify, boolean loggingIn, boolean permissionsChanged) throws SimplyModularFatalModuleException {
        //Debug.logDebug("Turning invisibility on for " + player.getName());
        invisiblePlayers.add(player.getName());
        User user = getUser(player);
        if (notify) {
            StringContainer.sendMessage(player, StringType.invisibilityTurnedOn);
        }
        if (isPickingUp(player.getName())) {
            pickupFlagAutoModified.add(player.getName());
            pickupOff(player, notify);
        }
        for (Player player2 : Bukkit.getOnlinePlayers()) {
            if (player2.equals(player)) {
                continue;
            }
            if (!canSeeInvisible(player2.getName(), player.getName())) {
                player2.hidePlayer(player);
                if (!silent) {
                    StringContainer.sendMessage(player2, StringType.leaveServer, player.getDisplayName());
                }
            } else {
                player2.showPlayer(player);
                if (!loggingIn && !permissionsChanged) {
                    StringContainer.sendMessage(player2, StringType.invisibilityTurnedOnAlert, player.getDisplayName());
                }
            }
        }
        List<Entity> nearby = player.getNearbyEntities(100, 100, 100);
        for (Entity entity : nearby) {
            if (entity instanceof Creature) {
                Creature creature = (Creature) entity;
                Entity entity2 = creature.getTarget();
                if (entity2 instanceof Player) {
                    Player player2 = (Player) entity2;
                    if (player.equals(player2)) {
                        creature.setTarget(null);
                    }
                }
            }
        }
        user.getCompoundTag(this).setBoolean("invisible", true);
        user.save();
        updateTargetListener();
    }
    
    private void turnOff(Player player, boolean silent, boolean notify, boolean loggingIn, boolean permissionsChanged) throws SimplyModularFatalModuleException {
        //Debug.logDebug("Turning invisibility off for " + player.getName());
        invisiblePlayers.remove(player.getName());
        User user = getUser(player);
        if (pickupFlagAutoModified.contains(player.getName())) {
            pickupFlagAutoModified.remove(player.getName());
            pickupOn(player, notify);
        }
        invisiblePlayers.remove(player.getName());
        if (player.hasPermission(SimpleCommandMetadata.hide.getPermission())) {
            if (notify) {
                StringContainer.sendMessage(player, StringType.invisibilityTurnedOff);
            }
        }
        for (Player player2 : Bukkit.getOnlinePlayers()) {
            player2.showPlayer(player);
            if (player2.equals(player)) {
                continue;
            }
            if (!canSeeInvisible(player2.getName(), player.getName())) {
                if (!silent) {
                    StringContainer.sendMessage(player2, StringType.reenterServer, player.getDisplayName());
                }
            } else if (!loggingIn && !permissionsChanged) {
                StringContainer.sendMessage(player2, StringType.invisibilityTurnedOffAlert, player.getDisplayName());
            }
        }
        user.getCompoundTag(this).setBoolean("invisible", false);
        user.save();
        updateTargetListener();
    }
    
    @SimpleEvent
    public void onGroupChange(GroupChangeEvent event) throws SimplyModularFatalModuleException {
        User user = event.getUser();
        if (!user.getCompoundTag(this).getBoolean("manualInvisLevel")) {
            user.getCompoundTag(this).setInt("invisibilityLevel", event.getNewGroup().getInvisibilityLevel());
            user.save();
        }
        calculateInvisibilityAll(false, true);
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void join(PlayerJoinEvent event) throws SimplyModularFatalModuleException {
        Player player = event.getPlayer();
        User user = getUser(player);
        if (user.getCompoundTag(this).getBoolean("invisible") && player.hasPermission(SimpleCommandMetadata.hide.getPermission())) {
            event.setJoinMessage(null);
            for (Player seer : getPlayersCanSee(player)) {
                StringContainer.sendMessage(seer, StringType.invisibilityLoggedOnAlert, player.getDisplayName());
            }
        }
        calculateInvisibilityAll(true, false);
    }
    
    @SimpleEvent(priority = EventPriority.HIGHEST)
    public void quit(PlayerQuitEvent event) throws SimplyModularFatalModuleException {
        Player player = event.getPlayer();
        if (isHidden(player.getName())) {
            event.setQuitMessage(null);
            for (Player seer : getPlayersCanSee(player)) {
                StringContainer.sendMessage(seer, StringType.invisibilityLoggedOffAlert, player.getDisplayName());
            }
        }
        invisiblePlayers.remove(player.getName());
        noPickup.remove(player.getName());
        pickupFlagAutoModified.remove(player.getName());
        updatePickupListener();
        updateTargetListener();
    }
    
    public ArrayList<Player> getPlayersCanSee(Player invisible) throws SimplyModularFatalModuleException {
        ArrayList<Player> players = new ArrayList<Player>();
        User user1 = getUser(invisible);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.equals(invisible)) {
                User user2 = getUser(player);
                if (canSee(user2, user1)) {
                    players.add(player);
                }
            }
        }
        return players;
    }
    
    public boolean canSeeInvisible(String viewer, String invisible) throws SimplyModularFatalModuleException {
        User user1, user2;
        user1 = getUser(viewer);
        user2 = getUser(invisible);
        return canSee(user1, user2);
    }
    
    public boolean canCurrentlySee(Player viewer, Player target) throws SimplyModularFatalModuleException {
        if (!isHidden(target.getName())) { return true; }
        if (canSeeInvisible(viewer.getName(), target.getName())) { return true; }
        return false;
    }
    
    private boolean canSee(User viewer, User invisible) throws SimplyModularFatalModuleException {
        int level1, level2;
        level1 = viewer.getCompoundTag(this).getInt("invisibilityLevel");
        level2 = invisible.getCompoundTag(this).getInt("invisibilityLevel");
        return level1 >= level2;
    }
    
    public void initializeModuleCompound(CompoundTag tag, User user) throws SimplyModularFatalModuleException {
        tag.setBoolean("invisible", false);
        tag.setBoolean("manualInvisLevel", false);
        int invisLevel = 0;
        if (SimplePermissions.isEnabled()) {
            Group g = getGroupByUser(user);
            invisLevel = g.getInvisibilityLevel();
        }
        tag.setInt("invisibilityLevel", invisLevel);
    }
    
}
