/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module;

import java.io.File;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.events.SimpleListener;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.utils.*;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.jnbt.CompoundTag;

public abstract class SimpleModule extends SimpleListener implements
        NamedObject {
    
    protected static ConsoleCommandSender console = Bukkit.getConsoleSender();
    private FileConfiguration config = null;
    private String configName = null;
    
    private final String name;
    
    public String getName() {
        return name;
    }
    
    public SimpleModule(String name) throws SimplyModularFatalModuleException {
        this.name = name;
    }
    
    protected void registerCommand(SimpleCommand command) {
        SimplyModular.getCommandMap().registerCommand(command);
    }
    
    //Get player objects
    
    public Player getOnlinePlayer(String pName, CommandSender requester) {
        return getOnlinePlayer(pName, requester, false);
    }
    
    public Player getOnlinePlayer(String pName, CommandSender requester, boolean canNull) {
        return UserManager.getManager().getOnlinePlayer(pName, requester, canNull);
    }
    
    public String getOfflinePlayer(String pName, CommandSender requester, boolean feedback) {
        return UserManager.getManager().getOfflinePlayer(pName, requester, feedback);
    }
    
    public OfflinePlayer getOnlineOfflinePlayer(String pName, CommandSender requester, boolean feedback) {
        return UserManager.getManager().getOnlineOfflinePlayer(pName, requester, feedback);
    }
    
    //Get user objects
    
    public User getUser(Player player) throws SimplyModularFatalModuleException {
        return UserManager.getManager().getUser(player);
    }
    
    public User getUser(String name) throws SimplyModularFatalModuleException {
        return UserManager.getManager().getUser(name);
    }
    
    //Get group objects
    
    public Group getGroupByPlayer(Player player) throws SimplyModularFatalModuleException {
        return SimplePermissions.getPermissionsManager().getGroupByPlayer(player);
    }
    
    public Group getGroupByName(String name) throws SimplyModularFatalModuleException {
        return SimplePermissions.getPermissionsManager().getGroupByName(name);
    }
    
    public Group getGroupByUser(User user) throws SimplyModularFatalModuleException {
        return SimplePermissions.getPermissionsManager().getGroupByUser(user);
    }
    
    // Utils
    public void saveConfig() {
        try {
            config.save(new File(SimplyModular.getSM().getDataFolder(), configName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean loadConfig() {
        return loadConfig(getName());
    }
    
    @SuppressWarnings("deprecation")
    public boolean loadResourceConfig(String name, boolean fix) {
        File file = new File(SimplyModular.getSM().getDataFolder(), name + ".yml");
        boolean created = false;
        configName = name + ".yml";
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {}
            created = true;
            System.out.println("Getting resource: " + name + ".yml");
            config = YamlConfiguration.loadConfiguration(getClass().getClassLoader().getResourceAsStream(name + ".yml"));
            saveConfig();
        } else {
            config = YamlConfiguration.loadConfiguration(file);
            if (fix) {
                config.setDefaults(YamlConfiguration.loadConfiguration(getClass().getClassLoader().getResourceAsStream(name + ".yml")));
                config.options().copyDefaults(true);
                saveConfig();
            }
        }
        return created;
    }
    
    public boolean loadConfig(String name) {
        File file = new File(SimplyModular.getSM().getDataFolder(), name + ".yml");
        boolean created = false;
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {}
            created = true;
        }
        config = YamlConfiguration.loadConfiguration(file);
        configName = name + ".yml";
        return created;
    }
    
    public FileConfiguration getConfig() {
        return config;
    }
    
    public abstract void initializeModuleCompound(CompoundTag tag, User user) throws SimplyModularFatalModuleException;
    
    public void deactivate() throws SimplyModularFatalException {}
}
