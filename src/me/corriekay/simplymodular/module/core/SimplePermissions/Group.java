/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.SimplePermissions;

import java.util.ArrayList;
import java.util.HashMap;

public class Group {
    
    private String name, perm2add2, prefix, listName;
    private boolean requiresConsole, isOpType, restricted, defaultGroup;
    private int invisibilityLevel, listNumber;
    private ArrayList<Group> inherits = new ArrayList<Group>();
    private PermissionsNode globalPerms = new PermissionsNode();
    private HashMap<String, PermissionsNode> worldPerms = new HashMap<String, PermissionsNode>();
    
    public PermissionsNode getGlobalPermissions() {
        return globalPerms;
    }
    
    public String getPerm2add2() {
        return perm2add2;
    }
    
    public void setPerm2add2(String perm2add2) {
        this.perm2add2 = perm2add2;
    }
    
    public String getPrefix() {
        return prefix;
    }
    
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    public String getListName() {
        return listName;
    }
    
    public void setListName(String listName) {
        this.listName = listName;
    }
    
    public boolean isRequiresConsole() {
        return requiresConsole;
    }
    
    public void setRequiresConsole(boolean requiresConsole) {
        this.requiresConsole = requiresConsole;
    }
    
    public boolean isOpType() {
        return isOpType;
    }
    
    public void setOpType(boolean isOpType) {
        this.isOpType = isOpType;
    }
    
    public boolean isRestricted() {
        return restricted;
    }
    
    public void setRestricted(boolean restricted) {
        this.restricted = restricted;
    }
    
    public int getInvisibilityLevel() {
        return invisibilityLevel;
    }
    
    public void setInvisibilityLevel(int invisibilityLevel) {
        this.invisibilityLevel = invisibilityLevel;
    }
    
    public int getListNumber() {
        return listNumber;
    }
    
    public void setListNumber(int listNumber) {
        this.listNumber = listNumber;
    }
    
    public void addGroupInherits(Group g) {
        if (g != this && !inherits.contains(g)) {
            inherits.add(g);
        }
    }
    
    public void removeGroupInherits(Group g) {
        inherits.add(g);
    }
    
    public ArrayList<Group> getInheritedGroups() {
        return inherits;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public PermissionsNode getWorldPermissions(String world) {
        return worldPerms.get(world);
    }
    
    public void setWorldPermissions(String world, PermissionsNode node) {
        worldPerms.put(world, node);
    }
    
    public boolean isDefaultGroup() {
        return defaultGroup;
    }
    
    public void setDefaultGroup(boolean defaultGroup) {
        this.defaultGroup = defaultGroup;
    }
    
}
