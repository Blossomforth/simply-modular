/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.SimplePermissions;

import java.util.HashMap;

public class PermissionsNode {
    private HashMap<String, Boolean> permissions = new HashMap<String, Boolean>();
    
    public void setPermission(String perm, boolean bool) {
        permissions.put(perm, bool);
    }
    
    @SuppressWarnings("unchecked")
    public HashMap<String, Boolean> getPermissionMap() {
        return (HashMap<String, Boolean>) permissions.clone();
    }
}
