/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.SimplePermissions;

import java.util.*;
import java.util.logging.Level;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.commands.SimplePermissions.*;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.Debug;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.jnbt.*;

public class SimplePermissions extends SimpleModule {
    
    private static SimplePermissions perms;
    private static boolean enabled = false;
    
    private Group defaultGroup;
    private final HashMap<String, Group> groups = new HashMap<String, Group>();
    private final HashMap<String, PermissionAttachment> playerPerms = new HashMap<String, PermissionAttachment>();
    private final HashMap<String, HashMap<String, Permission>> playerPermissionHistory = new HashMap<String, HashMap<String, Permission>>();
    
    public SimplePermissions() throws SimplyModularFatalModuleException {
        super("SimplePermissions");
        perms = this;
        SimplyModular.getNameListener().registerMapKey(playerPerms);
        SimplyModular.getNameListener().registerMapKey(playerPermissionHistory);
        Debug.logVerbose("Simple Permissions enabling! Loading permissions...", true);
        enabled = true;
        loadResourceConfig("permissions", false);
        reloadPermissions();
        registerCommand(new HasPerm(this));
        registerCommand(new ReloadPermissions(this));
        registerCommand(new me.corriekay.simplymodular.command.commands.SimplePermissions.List(this));
        registerCommand(new GroupAddPerm(this));
        registerCommand(new GroupDelPerm(this));
        registerCommand(new SetGroup(this));
        registerCommand(new UserAddPerm(this));
        registerCommand(new UserDelPerm(this));
    }
    
    public static SimplePermissions getPermissionsManager() {
        return perms;
    }
    
    public Group getGroupByPlayer(Player player) throws SimplyModularFatalModuleException {
        return getGroupByUser(getUser(player));
    }
    
    public Group getGroupByName(String name) throws SimplyModularFatalModuleException {
        return groups.get(name.toLowerCase());
    }
    
    public Group getGroupByUser(User user) throws SimplyModularFatalModuleException {
        return getGroupByName(user.getCompoundTag(this).getString("group"));
    }
    
    public Collection<Group> getGroups() {
        return groups.values();
    }
    
    public static boolean isEnabled() {
        return enabled;
    }
    
    public Permission getPlayerPermissionHistory(Player player, String permission) {
        return playerPermissionHistory.get(player.getName()).get(permission);
    }
    
    public void reloadPermissions() throws SimplyModularFatalModuleException {
        groups.clear();
        defaultGroup = null;
        Debug.logHighlyVerbose("Reloading permissions systems", false);
        for (String group : getConfig().getConfigurationSection("Groups").getKeys(false)) {
            Debug.logHighlyVerbose("Reloading permissions for " + group, false);
            ConfigurationSection gConf = getConfig().getConfigurationSection("Groups." + group);
            Group g = new Group();
            groups.put(group.toLowerCase(), g);
            g.setName(group);
            g.setDefaultGroup(gConf.getBoolean("default", false));
            if (g.isDefaultGroup()) {
                if (defaultGroup != null) {
                    Debug.logDebug("DEFAULT GROUP ALREADY SET TO " + defaultGroup.getName().toUpperCase() + ", CANNOT MAKE " + g.getName().toUpperCase() + " DEFAULT GROUP!", Level.SEVERE);
                    g.setDefaultGroup(false);
                } else {
                    defaultGroup = g;
                }
            }
            g.setPerm2add2(gConf.getString("perm2add2", null));
            g.setRequiresConsole(gConf.getBoolean("requiresConsole", false));
            g.setPrefix(ChatColor.translateAlternateColorCodes('&', gConf.getString("prefix", "")));
            g.setInvisibilityLevel(gConf.getInt("invisibilityLevel", 0));
            g.setOpType(gConf.getBoolean("isOPType", false));
            g.setRestricted(gConf.getBoolean("restricted", false));
            g.setListName(ChatColor.translateAlternateColorCodes('&', gConf.getString("listName", group)));
            for (String gPerms : gConf.getStringList("globalPermissions")) {
                Debug.logHighlyVerbose("Setting global permissions", false);
                boolean flag = true;
                if (gPerms.startsWith("-")) {
                    flag = false;
                    gPerms = gPerms.substring(1, gPerms.length());
                }
                g.getGlobalPermissions().setPermission(gPerms, flag);
            }
            ConfigurationSection worldConfig = gConf.getConfigurationSection("worldPermissions");
            for (String world : worldConfig.getKeys(false)) {
                Debug.logHighlyVerbose("setting permissions for world " + world, false);
                PermissionsNode worldPerms = new PermissionsNode();
                for (String permission : worldConfig.getStringList(world)) {
                    boolean flag = true;
                    if (permission.startsWith("-")) {
                        flag = false;
                        permission = permission.substring(1, permission.length());
                    }
                    worldPerms.setPermission(permission, flag);
                }
                g.setWorldPermissions(world, worldPerms);
            }
        }
        if (defaultGroup == null) { throw new SimplyModularFatalModuleException("No default group selected.", new NullPointerException(), this); }
        for (Group g : groups.values()) {
            String group = g.getName();
            Debug.logHighlyVerbose("Parsing " + group + " for inherited groups", false);
            for (String inheritMe : getConfig().getStringList("Groups." + group + ".inherits")) {
                Debug.logHighlyVerbose("Found " + inheritMe, false);
                Group inherited = groups.get(inheritMe.toLowerCase());
                if (inherited == null) {
                    Debug.logHighlyVerbose("Group " + inheritMe + " not found!", false);
                } else {
                    g.addGroupInherits(inherited);
                    Debug.logHighlyVerbose(group + " successfully inherited permissions from " + inheritMe, false);
                }
            }
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            Debug.logHighlyVerbose("Reloading permissions for player " + player.getName(), false);
            reloadPermissionsForPlayer(player);
        }
    }
    
    public void reloadPermissionsForPlayer(Player player) throws SimplyModularFatalModuleException {
        Debug.logDebug("Reloading permissions for Player " + player.getName());
        playerPermissionHistory.remove(player.getName());
        PermissionAttachment pa = playerPerms.get(player.getName());
        if (pa != null) {
            player.removeAttachment(pa);
        }
        
        pa = player.addAttachment(SimplyModular.getSM());
        playerPerms.remove(player.getName());
        playerPerms.put(player.getName(), pa);
        HashMap<String, Permission> permissions = new HashMap<String, Permission>();
        User user = getUser(player);
        Group g = groups.get(user.getCompoundTag(this).getString("group").toLowerCase());
        if (g == null) {
            Debug.logDebug("Group not found for player! Group string " + user.getCompoundTag(this).getString("group") + ". Setting users group to default group.");
            user.getCompoundTag(this).setString("group", defaultGroup.getName());
            user.save();
            g = defaultGroup;
        }
        {//INHERITED GLOBAL GROUP PERMISSIONS
            for (Group inheritedGroup : g.getInheritedGroups()) {
                HashMap<String, Boolean> permMap = inheritedGroup.getGlobalPermissions().getPermissionMap();
                HashSet<String> negPerms = new HashSet<String>();
                for (String permission : permMap.keySet()) {
                    Permission perm = permissions.get(permission);
                    if (!permMap.get(permission)) {
                        negPerms.add(permission);
                    } else {
                        if (perm == null) {
                            perm = new Permission(permission);
                            permissions.put(permission, perm);
                        }
                    }
                    parsePermission(true, perm, g.getName() + "'s inherited group, " + inheritedGroup.getName() + " global permission flagging.");
                }
                for (String negPerm : negPerms) {
                    Permission perm = permissions.get(negPerm);
                    if (perm == null) {
                        perm = new Permission(negPerm);
                        permissions.put(negPerm, perm);
                    }
                    parsePermission(false, perm, g.getName() + "'s inherited group, " + inheritedGroup.getName() + " global permission flagging.");
                }
            }
        }
        
        {//GLOBAL GROUP PERMISSIONS
            HashMap<String, Boolean> permMap = g.getGlobalPermissions().getPermissionMap();
            HashSet<String> negPerms = new HashSet<String>();
            for (String permission : permMap.keySet()) {
                Permission perm = permissions.get(permission);
                if (!permMap.get(permission)) {
                    negPerms.add(permission);
                } else {
                    if (perm == null) {
                        perm = new Permission(permission);
                        permissions.put(permission, perm);
                    }
                    parsePermission(true, perm, g.getName() + "'s global permission flagging.");
                }
            }
            for (String negPerm : negPerms) {
                Permission perm = permissions.get(negPerm);
                if (perm == null) {
                    perm = new Permission(negPerm);
                    permissions.put(negPerm, perm);
                }
                parsePermission(false, perm, g.getName() + "'s global permission flagging");
            }
        }
        
        {//INHERITED WORLD GROUP PERMISSIONS
            String wName = player.getWorld().getName();
            for (Group inheritedGroup : g.getInheritedGroups()) {
                PermissionsNode wNodes = inheritedGroup.getWorldPermissions(wName);
                if (wNodes == null) {
                    continue;
                }
                HashMap<String, Boolean> permMap = wNodes.getPermissionMap();
                HashSet<String> negPerms = new HashSet<String>();
                for (String permission : permMap.keySet()) {
                    Permission perm = permissions.get(permission);
                    if (!permMap.get(permission)) {
                        negPerms.add(permission);
                    } else {
                        if (perm == null) {
                            perm = new Permission(permission);
                            permissions.put(permission, perm);
                        }
                        parsePermission(true, perm, g.getName() + "'s inherited group, " + inheritedGroup.getName() + "'s world permission flagging, world name: " + wName);
                    }
                }
                for (String negPerm : negPerms) {
                    Permission perm = permissions.get(negPerm);
                    if (perm == null) {
                        perm = new Permission(negPerm);
                        permissions.put(negPerm, perm);
                    }
                    parsePermission(false, perm, g.getName() + "'s inherited group, " + inheritedGroup.getName() + "'s world permission flagging, world name: " + wName);
                }
            }
        }
        
        {//WORLD GROUP PERMISSIONS
            String wName = player.getWorld().getName();
            PermissionsNode wNodes = g.getWorldPermissions(wName);
            if (wNodes != null) {
                HashMap<String, Boolean> permMap = wNodes.getPermissionMap();
                HashSet<String> negPerms = new HashSet<String>();
                for (String permission : permMap.keySet()) {
                    Permission perm = permissions.get(permission);
                    if (!permMap.get(permission)) {
                        negPerms.add(permission);
                    } else {
                        if (perm == null) {
                            perm = new Permission(permission);
                            permissions.put(permission, perm);
                        }
                        parsePermission(true, perm, g.getName() + "'s world permission flagging, world name: " + wName);
                    }
                }
                for (String negPerm : negPerms) {
                    Permission perm = permissions.get(negPerm);
                    if (perm == null) {
                        perm = new Permission(negPerm);
                        permissions.put(negPerm, perm);
                    }
                    parsePermission(false, perm, g.getName() + "'s world permission flagging, world name: " + wName);
                }
            }
        }
        
        {//PERSONAL GLOBAL PERMISSIONS
            for (Tag permissionTag : user.getCompoundTag(this).getList("personalPermissions").getValue()) {
                String permission = ((StringTag) permissionTag).getValue();
                Permission perm = permissions.get(permission);
                if (perm == null) {
                    perm = new Permission(permission);
                    permissions.put(permission, perm);
                }
                parsePermission(permission, perm, player.getName() + "'s personal permissions");
            }
        }
        playerPermissionHistory.put(player.getName(), permissions);
        for (String perm : permissions.keySet()) {
            boolean flag = permissions.get(perm).flag;
            pa.setPermission(perm, flag);
        }
        Debug.logDebug("Permissions for player " + player.getName() + " reloaded successfully");
    }
    
    private void parsePermission(String permission, Permission perm, String changer) {
        boolean flag = true;
        if (permission.startsWith("-")) {
            flag = false;
        }
        perm.setFlagging(flag, changer + ". Flag set to " + flag + ".");
    }
    
    private void parsePermission(boolean flag, Permission perm, String changer) {
        perm.setFlagging(flag, changer + ". flag set to " + flag + ".");
    }
    
    @SimpleEvent(priority = EventPriority.LOW)
    //TODO if player somehow doesnt have a group, make sure to default here.
    public void onJoin(PlayerJoinEvent event) throws SimplyModularFatalModuleException {
        Player player = event.getPlayer();
        reloadPermissionsForPlayer(player);
    }
    
    public void initializeModuleCompound(CompoundTag tag, User user) {
        Debug.logVerbose("Initializing group settings for player ", false);
        tag.setString("group", defaultGroup.getName());
        Debug.logVerbose("setting default group: " + tag.getString("group"), false);
        tag.setList("personalPermissions", new ListTag("personalPermissions", StringTag.class, new ArrayList<Tag>()));
    }
}
