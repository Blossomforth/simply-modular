/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.usermanager;

import java.io.*;
import java.util.List;
import java.util.UUID;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.utils.Debug;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.entity.Player;
import org.jnbt.*;

public class User {
    
    private final File datFile;
    private final CompoundTag c;
    
    //	private HashMap<String, Inventory> remoteChests = new HashMap<String, Inventory>(),
    //			enderChests = new HashMap<String, Inventory>();
    //	private HashMap<Inventory, String> rcWorlds = new HashMap<Inventory, String>(),
    //			ecWorlds = new HashMap<Inventory, String>();
    
    protected User(Player player) throws Exception {
        this(player.getUniqueId());
    }
    
    protected User(UUID player) throws Exception {
        this(new File(SimplyModular.getSM().getDataFolder() + File.separator + "Players", player.toString() + ".nbt"));
    }
    
    protected User(File file) throws Exception {
        datFile = file;
        NBTInputStream in = new NBTInputStream(new FileInputStream(datFile));
        c = (CompoundTag) in.readTag();
        in.close();
        Debug.logHighlyVerbose("Not found in cache, loading directly from file", false);
        
        // TODO load ender/remote chests
    }
    
    // retrieval methods
    public CompoundTag getCompoundTag(SimpleModule module) throws SimplyModularFatalModuleException {
        CompoundTag tag = c.getCompoundTag(module.getName());
        if (tag != null) {
            return tag;
        } else {
            tag = new CompoundTag(module.getName());
            c.setCompound(module.getName(), tag);
            module.initializeModuleCompound(tag, this);
            save();
            return tag;
        }
    }
    
    public void setCompound(SimpleModule module, CompoundTag tag) {
        Debug.logVerbose("setting the compound for " + module.getName(), false);
        c.setCompound(module.getName(), tag);
    }
    
    // utility methods
    public boolean save() {
        Debug.logVerbose("Saving user file", false);
        try {
            NBTOutputStream out = new NBTOutputStream(new FileOutputStream(datFile));
            out.writeTag(c);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public static boolean exists(UUID id) {
        return new File(SimplyModular.getSM().getDataFolder() + File.separator + "Players", id.toString() + ".nbt").exists();
    }
    
    public static User loadUser(Player player) throws SimplyModularFatalModuleException {
        if (!exists(player.getUniqueId())) { return null; }
        try {
            return new User(player);
        } catch (Exception e) {
            throw new SimplyModularFatalModuleException("IO EXCEPTION: Unable to load player file that exists. Please check for file permissions in the server directory!", e, UserManager.getManager());
        }
    }
    
    public static User loadUser(UUID id) throws SimplyModularFatalModuleException {
        if (id == null) { return null; }
        try {
            return new User(id);
        } catch (Exception e) {
            throw new SimplyModularFatalModuleException("IO EXCEPTION: Unable to load player file that exists. Please check for file permissions in the server directory!", e, UserManager.getManager());
        }
    }
    
    public static void create(Player player) throws SimplyModularFatalModuleException {
        try {
            File datFile = new File(SimplyModular.getSM().getDataFolder() + File.separator + "Players", player.getUniqueId().toString() + ".nbt");
            datFile.createNewFile();
            Debug.logHighlyVerbose("Creating file", false);
            CompoundTag c = new CompoundTag("datafile");
            NBTOutputStream out = new NBTOutputStream(new FileOutputStream(datFile));
            Debug.logHighlyVerbose("Writing tag to newly created file", false);
            out.writeTag(c);
            out.close();
        } catch (Exception e) {
            throw new SimplyModularFatalModuleException("IO Exception: Unable to create new file! Please check for file permissions in the server directory!", e, UserManager.getManager());
        }
    }
    
    protected void changedNames(String newname) throws SimplyModularFatalModuleException {
        getCompoundTag(UserManager.getManager()).setString("name", newname);
        CompoundTag ctag = getCompoundTag(UserManager.getManager());
        List<Tag> stringlist = ctag.getList("usernameHistory").getValue();
        for (Tag tag : stringlist) {
            if (((StringTag) tag).getValue().equals(newname)) { return; }
        }
        stringlist.add(new StringTag(newname, newname));
    }
}
