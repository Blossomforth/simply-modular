/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.usermanager;

import java.io.*;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.entity.Player;

public class UsernameManager {
    
    private final File nameDB = new File(SimplyModular.getSM().getDataFolder() + File.separator + "Players.DB");
    private final ConcurrentHashMap<UUID, String> registeredUsernames = new ConcurrentHashMap<UUID, String>();
    private final ConcurrentHashMap<String, UUID> registeredUUIDs = new ConcurrentHashMap<String, UUID>();
    
    public UsernameManager() throws SimplyModularFatalModuleException {
        if (!nameDB.exists()) {
            try {
                nameDB.createNewFile();
            } catch (IOException e) {
                throw new SimplyModularFatalModuleException("Unable to create username database file!", e, UserManager.getManager());
            }
        }
        try {
            BufferedReader reader = new BufferedReader(new FileReader(nameDB));
            String line = reader.readLine();
            while (line != null) {
                String[] first = line.split("-");
                String name = first[0];
                UUID uuid = UUID.fromString(first[1]);
                registeredUsernames.put(uuid, name);
                registeredUUIDs.put(name, uuid);
            }
            reader.close();
        } catch (Exception e) {
            throw new SimplyModularFatalModuleException("Unable to read username database file!", e, UserManager.getManager());
        }
        
    }
    
    public UUID getKnownUUID(String playername) {
        return registeredUUIDs.get(playername);
    }
    
    public String getKnownUser(UUID id) {
        return registeredUsernames.get(id);
    }
    
    public void verifyPlayer(Player player) {
        UUID id = player.getUniqueId();
        if (registeredUsernames.containsKey(id)) {//UUID in file
            if (registeredUsernames.get(id).equals(player.getName())) {
                return; //everything is A-OK
            } else {
                String oldname = registeredUsernames.get(id);
                registeredUsernames.remove(id);
                registeredUUIDs.remove(oldname);
                registeredUsernames.put(id, player.getName());
                registeredUUIDs.put(player.getName(), id);
            }
        } else {//UUID does not exist, new player coming to the server.
            if (registeredUUIDs.containsKey(player.getName())) {//Yet playername does exist. Needs a player name update.
            
            }
        }
    }
}
