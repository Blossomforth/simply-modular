/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.SimpleWorldManagement;

import java.io.File;
import java.io.FileOutputStream;

import me.corriekay.simplymodular.command.commands.SimpleWorldManager.*;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.events.SimpleListener;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.*;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.jnbt.CompoundTag;
import org.jnbt.NBTOutputStream;

public class SimpleWorldManager extends SimpleModule {
    
    private static SimpleWorldManager swm;
    
    public static SimpleWorldManager getManager() {
        return swm;
    }
    
    @SuppressWarnings("unused")
    private final SimpleListener worldTransferListener = new SimpleListener() {
        @SimpleEvent(priority = EventPriority.MONITOR)
        public void teleport(PlayerChangedWorldEvent event) throws SimplyModularFatalException {
            World from, to;
            from = event.getFrom();
            to = event.getPlayer().getWorld();
            
            SimpleWorld sfrom, sto;
            sfrom = SimpleWorld.getWorld(from).getOverworld();
            sto = SimpleWorld.getWorld(to).getOverworld();
            
            if (!sfrom.equals(sto)) {//overworlds are inequal, changing universes
                Player player = event.getPlayer();
                sfrom.setWorldInventory(player.getInventory(), player.getName());
                sfrom.saveToWorld(player, false);
                sto.copyInventory(player.getInventory(), player.getName());
                sto.setWorldStats(player);
            }
        }
    };
    
    public SimpleWorldManager() throws SimplyModularFatalException {
        super("SimpleWorldManager");
        swm = this;
        if (loadConfig("worldmanager")) {
            World world = Bukkit.getWorlds().get(0);
            writeMeta(world);
            ConfigurationSection section = getConfig().createSection("worlds." + world.getName());
            section.set("default", true);
            section.set("spawn", Utils.getArrayLoc(world.getSpawnLocation()));
            section.set("pvp", world.getPVP());
            section.set("gamemode", GameMode.SURVIVAL.name());
            section.set("difficulty", world.getDifficulty().name());
            section.set("hostilemobs", world.getMonsterSpawnLimit());
            section.set("neutralmobs", world.getAnimalSpawnLimit());
            World nether, end;
            nether = Bukkit.getWorld(world.getName() + "_nether");
            end = Bukkit.getWorld(world.getName() + "_the_end");
            if (nether != null) {
                section.set("nether", nether.getName());
                writeMeta(nether);
            }
            if (end != null) {
                section.set("end", end.getName());
                writeMeta(end);
            }
            section.set("backpack", true);
            section.set("enderchest", true);
            saveConfig();
        }
        for (String werld : getConfig().getConfigurationSection("worlds").getKeys(false)) {
            ConfigurationSection section = getConfig().getConfigurationSection("worlds." + werld);
            SimpleWorld.setupUniverse(section);
        }
        if (SimpleWorld.getDefaultWorld() == null) {
            Debug.logVerbose("No default world specified. Designating world 0 (default minecraft world) as default world.", true);
            SimpleWorld.setDefaultWorld(SimpleWorld.getWorld(Bukkit.getWorlds().get(0)));
        }
        boolean unregisteredworld = false;
        StringBuilder sb = new StringBuilder();
        for (World world : Bukkit.getWorlds()) {
            if (SimpleWorld.getWorld(world) == null) {
                sb.append(world.getName() + ", ");
                unregisteredworld = true;
            }
        }
        if (unregisteredworld) { throw new SimplyModularFatalModuleException("WORLDS EXIST UNREGISTERED. Worlds: " + sb.substring(0, sb.length() - 2) + ". Either these worlds are being loaded by another multiworld system (oh god why are you doing this) and you should also register them with Simply Modular, or you are not registering default bukkit/minecraft worlds. Please do so.", new NullPointerException(), this); }
        saveWorldsConfig();
        registerCommand(new WorldCommand(this));
        registerCommand(new SetWorldSpawnCommand(this));
        registerCommand(new CreateWorldCommand(this));
        registerCommand(new RemoveWorldCommand(this));
    }
    
    public static void writeMeta(World world) throws SimplyModularFatalException {
        try {
            //Debug.logDebug("Writting world metadata for world " + world.getName());
            File meta = new File(Bukkit.getWorldContainer() + File.separator + world.getName(), "metadata.nbt");
            meta.createNewFile();
            CompoundTag ctag = new CompoundTag("metadata container");
            ctag.setString("worldtype", world.getWorldType().name());
            ctag.setString("environment", world.getEnvironment().name());
            ctag.setBoolean("structures", world.canGenerateStructures());
            ctag.setLong("seed", world.getSeed());
            ctag.setList("spawn", Utils.getListTagLoc(world.getSpawnLocation(), "spawn"));
            NBTOutputStream stream = new NBTOutputStream(new FileOutputStream(meta));
            stream.writeTag(ctag);
            stream.close();
        } catch (Exception e) {
            throw new SimplyModularFatalException("Unable to create metadata worldmanager simpleworld metadata file", e);
        }
    }
    
    public void saveWorldsConfig() throws SimplyModularFatalException {
        getConfig().set("worlds", null);
        for (SimpleWorld world : SimpleWorld.getWorlds()) {
            if (world.isOverworld()) {
                world.writeToFile(getConfig());
            }
        }
        saveConfig();
    }
    
    @SimpleEvent
    public void teleport(PlayerChangedWorldEvent event) {
        SimpleWorld world = SimpleWorld.getWorld(event.getPlayer().getWorld().getName());
        String gamemodestring = world.getGameMode().name();
        switch (world.getGameMode()) {
            case ADVENTURE:
                gamemodestring = ChatColor.DARK_RED + gamemodestring;
            case SURVIVAL:
                gamemodestring = ChatColor.RED + gamemodestring;
            case CREATIVE:
                gamemodestring = ChatColor.GREEN + gamemodestring;
        }
        StringContainer.sendMessage(event.getPlayer(), StringType.worldManagerNotifyChangeWorld, "\"" + world.getName() + "\"", (world.getPVP() ? ChatColor.RED + "ON" : ChatColor.GREEN + "OFF"), gamemodestring);
    }
    
    @SimpleEvent
    public void portal(PlayerPortalEvent event) {
        System.out.println(event.getCause().name());
        SimpleWorld fromWorld = SimpleWorld.getWorld(event.getPlayer().getWorld().getName());
        SimpleWorld toWorld;
        double ratio = 0;
        if (event.getCause() == TeleportCause.END_PORTAL) {
            event.useTravelAgent(false);
            if (fromWorld.isOverworld() && fromWorld.hasEnd()) {
                toWorld = fromWorld.getEnd();
                event.setTo(toWorld.getWorld().getSpawnLocation());
            } else if (fromWorld.isEnd()) {
                event.setTo(fromWorld.getSpawn());
            }
            return;
        }
        if (fromWorld.isOverworld()) {
            ratio = 0.125;
            if (!fromWorld.hasNether()) {
                return;
            } else {
                toWorld = fromWorld.getNether();
            }
        } else if (fromWorld.isNether()) {
            ratio = 8.0;
            toWorld = fromWorld.getOverworld();
        } else {
            return;
        }
        Location from = event.getFrom();
        Location finalLocation = new Location(toWorld.getWorld(), from.getX() * ratio, from.getY(), from.getZ() * ratio);
        event.setTo(finalLocation);
        event.getPortalTravelAgent().setCanCreatePortal(true);
        event.useTravelAgent(true);
    }
    
    @SimpleEvent
    public void joinEvent(PlayerJoinEvent event) throws SimplyModularFatalException {
        Player player = event.getPlayer();
        for (SimpleWorld world : SimpleWorld.getWorlds()) {
            if (world.isOverworld()) {
                world.loadInventories(player.getName());
            }
        }
        SimpleWorld world = SimpleWorld.getWorld(player);
        User user = UserManager.getManager().getUser(player);
        if (!world.getName().equals(user.getCompoundTag(this).getString("worldname"))) {
            world = world.getOverworld();
            player.teleport(world.getSpawn());
        }
        world.copyInventory(player.getInventory(), player.getName());
        world.setWorldStats(player);
    }
    
    @SimpleEvent
    public void quitEvent(PlayerQuitEvent event) throws SimplyModularFatalException {
        for (SimpleWorld world : SimpleWorld.getWorlds()) {
            if (world.isOverworld()) {
                world.saveToWorld(event.getPlayer(), true);
            }
        }
        User user = UserManager.getManager().getUser(event.getPlayer());
        user.getCompoundTag(this).setString("worldname", event.getPlayer().getWorld().getName());
        user.save();
    }
    
    @Override
    public void initializeModuleCompound(CompoundTag tag, User user) throws SimplyModularFatalModuleException {
        tag.setString("worldname", SimpleWorld.getDefaultWorld().getName());
    }
    
    public void deactivate() throws SimplyModularFatalException {
        saveWorldsConfig();
    }
}
