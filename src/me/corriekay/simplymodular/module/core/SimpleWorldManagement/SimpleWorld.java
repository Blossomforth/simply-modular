/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.SimpleWorldManagement;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.*;

import me.corriekay.simplymodular.utils.*;
import net.minecraft.server.v1_7_R3.NBTTagCompound;

import org.bukkit.*;
import org.bukkit.World.Environment;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_7_R3.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.*;
import org.bukkit.potion.PotionEffect;
import org.jnbt.*;

@SuppressWarnings("deprecation")
public class SimpleWorld {
    
    //Static
    private static HashMap<String, SimpleWorld> worlds = new HashMap<String, SimpleWorld>();
    
    public static Collection<SimpleWorld> getWorlds() {
        return worlds.values();
    }
    
    private static SimpleWorld defaultWorld = null;
    
    public static SimpleWorld getDefaultWorld() {
        return defaultWorld;
    }
    
    public static void setDefaultWorld(SimpleWorld world) {
        defaultWorld = world;
    }
    
    public static SimpleWorld getWorld(Entity entity) {
        return getWorld(entity.getWorld());
    }
    
    public static SimpleWorld getWorld(World world) {
        return getWorld(world.getName());
    }
    
    public static SimpleWorld getWorld(String name) {
        return worlds.get(name);
    }
    
    public static boolean worldExistsInFile(String name) {
        File dir = new File(Bukkit.getWorldContainer(), name);
        dir = new File(dir, "metadata.nbt");
        return dir.exists();
    }
    
    public static ArrayList<String> getUnloadedWorldsOfType(Environment type) throws SimplyModularFatalException {
        String typestring = type.name();
        ArrayList<String> worlds = new ArrayList<String>();
        for (File possibleWorld : Bukkit.getWorldContainer().listFiles()) {
            if (worldExistsInFile(possibleWorld.getName()) && getWorld(possibleWorld.getName()) == null) {
                CompoundTag metaFile = getWorldMeta(possibleWorld.getName());
                if (metaFile.getString("environment").equals(typestring)) {
                    worlds.add(possibleWorld.getName());
                }
            }
        }
        return worlds;
    }
    
    public static boolean worldtypeMatches(String name, WorldType type) throws SimplyModularFatalException {
        if (worldExistsInFile(name)) { return getWorldMeta(name).getString("environment").equals(type.name()); }
        return false;
    }
    
    private static final HashSet<String> reservedNames = new HashSet<String>(Arrays.asList(new String[] { "crash-reports", "plugins", "logs" }));
    
    public static boolean isValidWorldname(String name) {
        return !reservedNames.contains(name);
    }
    
    public static void setupUniverse(ConfigurationSection config) throws SimplyModularFatalException {
        if (worldExistsInFile(config.getName())) {//If it exists, we want to make sure that we're not trying to load an incorrect world as an overworld.
            Debug.logDebug("getting metadata for world: " + config.getName());
            CompoundTag tag = getWorldMeta(config.getName());
            if (!tag.getString("environment").equals("NORMAL")) { throw new SimplyModularFatalException("Attempted to generate normal world using non-normal worldtype. Worldname: \"" + config.getName() + "\"", new InvalidParameterException()); }
        }
        SimpleWorld overworld, nether = null, end = null;
        overworld = loadOrCreateWorld(config.getName(), Environment.NORMAL, config);
        if (config.contains("nether")) {//If we've got a nether world to generate, and the nether exists on file, we want to make sure we're not trying to load it as anything but Nether. Then, load that sumbich.
            if (worldExistsInFile(config.getString("nether")) && !getWorldMeta(config.getString("nether")).getString("environment").equals("NETHER")) { throw new SimplyModularFatalException("Attempted to generate nether world using non-nether worldtype. Worldname: \"" + config.getName() + "\"", new IllegalArgumentException()); }
            nether = loadOrCreateWorld(config.getString("nether"), Environment.NETHER, config);
        }
        if (config.contains("end")) {//If we've got an end world to generate, and the end exists in file, we want to make sure we're not trying to load it as anything but end. Then, load that sumbich.
            if (worldExistsInFile(config.getString("end")) && !getWorldMeta(config.getString("end")).getString("environment").equals("THE_END")) { throw new SimplyModularFatalException("Attempted to generate nether world using non-nether worldtype. Worldname: \"" + config.getName() + "\"", new IllegalArgumentException()); }
            end = loadOrCreateWorld(config.getString("end"), Environment.THE_END, config);
        }
        overworld.overworld = overworld;
        overworld.nether = nether;
        overworld.end = end;
        if (nether != null) {
            nether.overworld = overworld;
            nether.nether = nether;
            nether.end = end;
        }
        if (end != null) {
            end.overworld = overworld;
            end.nether = nether;
            end.end = end;
        }
        Location l = Utils.getLoc((ArrayList<String>) config.getStringList("spawn"));
        overworld.setSpawn(l == null ? overworld.world.getSpawnLocation() : l);
        overworld.gamemode = GameMode.valueOf(config.getString("gamemode", "SURVIVAL"));
        if (config.getBoolean("default")) {
            if (defaultWorld != null) {
                Debug.logVerbose("Attempting to set another default world. Multiple default worlds are NOT allowed, so second world will be ignored.", true);
            } else {
                defaultWorld = overworld;
            }
        }
    }
    
    public static CompoundTag getWorldMeta(String worldname) throws SimplyModularFatalException {
        try {
            File dir = new File(Bukkit.getWorldContainer() + File.separator + worldname);
            if (!dir.exists()) { return null; }
            File meta = new File(dir, "metadata.nbt");
            if (!meta.exists()) { return null; }
            CompoundTag tag;
            NBTInputStream in = new NBTInputStream(new FileInputStream(meta));
            tag = (CompoundTag) in.readTag();
            in.close();
            return tag;
        } catch (Exception e) {
            throw new SimplyModularFatalException("Unable to read metadata for world: \"" + worldname + "\"", e);
        }
    }
    
    private static SimpleWorld loadOrCreateWorld(String name, Environment environment, ConfigurationSection config) throws SimplyModularFatalException {
        if (worlds.containsKey(name)) { throw new SimplyModularFatalException("Attempting to load or create a world that is already loaded. World attempting to load: \"" + name + "\" into universe: \"" + config.getName() + "\"", new IllegalArgumentException()); }
        SimpleWorld world;
        if (worldExistsInFile(name)) {
            world = loadWorld(name, getWorldMeta(name), config);
        } else {
            WorldGeneratorOptions wgo = new WorldGeneratorOptions(config, name, environment);
            world = createWorld(name, environment, wgo);
        }
        if (!world.isOverworld()) { return world; }
        for (Player player : Bukkit.getOnlinePlayers()) {
            world.loadInventories(player.getName());
        }
        
        return world;
    }
    
    private static SimpleWorld loadWorld(String name, CompoundTag tag, ConfigurationSection config) throws SimplyModularFatalException {
        return new SimpleWorld(loadWorld(name, Environment.valueOf(tag.getString("environment")), WorldType.valueOf(tag.getString("worldtype")), tag.getBoolean("structures"), tag.getString("seed"), config.getBoolean("pvp", false), config.getInt("hostilemobs", 70), config.getInt("neutralmobs", 15), Difficulty.valueOf(config.getString("difficulty", "NORMAL")), false), config.getBoolean("backpack", true), config.getBoolean("enderchest", true));
    }
    
    private static SimpleWorld createWorld(String name, Environment environment, WorldGeneratorOptions wgo) throws SimplyModularFatalException {
        return new SimpleWorld(loadWorld(name, environment, wgo.getWorldType(), wgo.getStructures(), wgo.getSeed(), wgo.getPVP(), wgo.gethostileMobSpawnCount(), wgo.getNeutralMobSpawnCount(), wgo.getDifficulty(), true), wgo.getBackpack(), wgo.getEnderchest());
    }
    
    private static World loadWorld(String name, Environment env, WorldType type, boolean structures, String seed, boolean pvp, int hostilemobs, int neutralmobs, Difficulty dif, boolean fixSpawn) throws SimplyModularFatalException {
        if (!isValidWorldname(name)) { throw new SimplyModularFatalException("Unable to use the name " + name + ", Worldname is reserved.", new IllegalArgumentException()); }
        WorldCreator wc = new WorldCreator(name);
        wc.environment(env);
        wc.type(type);
        wc.generateStructures(structures);
        if (seed != null) {
            wc.seed(seed.hashCode());
        }
        World world = wc.createWorld();
        world.setPVP(pvp);
        world.setMonsterSpawnLimit(hostilemobs);
        world.setAnimalSpawnLimit(neutralmobs);
        world.setAmbientSpawnLimit(neutralmobs);
        world.setWaterAnimalSpawnLimit(neutralmobs);
        world.setSpawnFlags(hostilemobs > 0, neutralmobs > 0);
        ListTag spawn;
        spawn = worldExistsInFile(name) ? getWorldMeta(world.getName()).getList("spawn") : null;
        Location l = spawn == null ? world.getSpawnLocation() : Utils.getLoc(spawn);
        //Debug.logDebug(world.getName() + " Spawn is null: " + (spawn == null ? "true :<" : "False. Locationc coordinates: " + world.getSpawnLocation().toString()));
        world.setSpawnLocation(l.getBlockX(), fixSpawn ? Utils.transformHighestYAt(l).getBlockY() : l.getBlockY(), l.getBlockZ());
        world.setDifficulty(dif);
        SimpleWorldManager.writeMeta(world);
        return world;
    }
    
    public static boolean unloadWorld(SimpleWorld world) {
        if (!verifyCanRemoveWorld(world)) { return false; }
        if (world.isOverworld()) {
            world = world.getOverworld();
            if (world.hasNether()) {
                if (unloadWorld(world.getNether().getWorld())) { return false; }
            }
            if (world.hasEnd()) {
                if (unloadWorld(world.getEnd().getWorld())) { return false; }
            }
            if (!unloadWorld(world.getWorld())) { return false; }
            if (world.hasNether()) {
                worlds.remove(world.getNether().getName());
                world.getNether().world = null;
            }
            if (world.hasEnd()) {
                worlds.remove(world.getEnd().getName());
                world.getEnd().world = null;
            }
            SimpleWorldManager.getManager().getConfig().set("worlds." + world.getName(), null);
            SimpleWorldManager.getManager().saveConfig();
            worlds.remove(world.getName());
            world.world = null;
            return true;
        } else {
            if (unloadWorld(world.world)) { return false; }
            SimpleWorld overworld = world.getOverworld();
            if (world.isNether()) {
                SimpleWorldManager.getManager().getConfig().set("worlds." + overworld.getName() + ".nether", null);
                overworld.nether = null;
                if (overworld.hasEnd()) {
                    overworld.getEnd().nether = null;
                }
            }
            if (world.isEnd()) {
                SimpleWorldManager.getManager().getConfig().set("worlds." + overworld.getName() + ".end", null);
                overworld.end = null;
                if (overworld.hasNether()) {
                    overworld.getNether().end = null;
                }
            }
            SimpleWorldManager.getManager().saveConfig();
            worlds.remove(world.getName());
            world.world = null;
            return true;
        }
    }
    
    private static boolean unloadWorld(World world) {
        for (Player player : world.getPlayers()) {
            player.teleport(getDefaultWorld().getSpawn());
        }
        return Bukkit.unloadWorld(world, true);
    }
    
    public static boolean verifyCanRemoveWorld(SimpleWorld world) {
        if (world.isOverworld()) {
            if (world.hasNether() && !verifyCanRemoveWorld(world.getNether())) { return false; }
            if (world.hasEnd() && !verifyCanRemoveWorld(world.getEnd())) { return false; }
            return !Bukkit.getWorlds().get(0).equals(world.world);
        } else if (world.isNether()) {
            return !Bukkit.getWorlds().get(1).equals(world.world);
        } else if (world.isEnd()) {
            return !Bukkit.getWorlds().get(2).equals(world.world);
        } else {
            return true;
        }
    }
    
    //non-static
    private World world;
    private Location spawn;
    private GameMode gamemode;
    private SimpleWorld overworld, nether, end;
    private final File worldDirectory, playerFiles;
    private boolean backpack, enderchest;
    private final HashMap<String, Inventory> inventories, enderchests,
            backpacks;
    private final HashMap<String, ItemStack[]> armorContents = new HashMap<String, ItemStack[]>();
    
    public SimpleWorld(World world, boolean backpack, boolean enderchest) {
        this.world = world;
        worldDirectory = new File(Bukkit.getWorldContainer() + File.separator + world.getName());
        if (worldDirectory.exists()) {
            worldDirectory.mkdirs();
        }
        playerFiles = new File(worldDirectory, "SimplyModularPlayerFiles");
        if (!playerFiles.exists()) {
            playerFiles.mkdirs();
        }
        this.backpack = backpack;
        this.enderchest = enderchest;
        worlds.put(world.getName(), this);
        inventories = new HashMap<String, Inventory>();
        enderchests = new HashMap<String, Inventory>();
        backpacks = new HashMap<String, Inventory>();
    }
    
    public void setPVP(boolean flag) throws SimplyModularFatalException {
        overworld.world.setPVP(flag);
        if (nether != null) {
            nether.world.setPVP(flag);
        }
        if (end != null) {
            end.world.setPVP(flag);
        }
        writeToFile(SimpleWorldManager.getManager().getConfig());
        SimpleWorldManager.getManager().saveConfig();
    }
    
    public boolean getPVP() {
        return overworld.world.getPVP();
    }
    
    public void setDifficulty(Difficulty flag) throws SimplyModularFatalException {
        overworld.world.setDifficulty(flag);
        if (nether != null) {
            nether.world.setDifficulty(flag);
        }
        if (end != null) {
            end.world.setDifficulty(flag);
        }
        writeToFile(SimpleWorldManager.getManager().getConfig());
        SimpleWorldManager.getManager().saveConfig();
        
    }
    
    public Difficulty getDifficulty() {
        return overworld.world.getDifficulty();
    }
    
    public void setHostileMobSpawnCount(int flag) throws SimplyModularFatalException {
        overworld.world.setMonsterSpawnLimit(flag);
        if (nether != null) {
            nether.world.setMonsterSpawnLimit(flag);
        }
        if (end != null) {
            end.world.setMonsterSpawnLimit(flag);
        }
        writeToFile(SimpleWorldManager.getManager().getConfig());
        SimpleWorldManager.getManager().saveConfig();
        
    }
    
    public int getHostileMobSpawnCount() {
        return overworld.world.getAnimalSpawnLimit();
    }
    
    public void setNeutralMobSpawnCount(int flag) throws SimplyModularFatalException {
        overworld.world.setAnimalSpawnLimit(flag);
        overworld.world.setAmbientSpawnLimit(flag);
        overworld.world.setWaterAnimalSpawnLimit(flag);
        if (nether != null) {
            nether.world.setAnimalSpawnLimit(flag);
            nether.world.setAmbientSpawnLimit(flag);
            nether.world.setWaterAnimalSpawnLimit(flag);
        }
        if (end != null) {
            end.world.setAnimalSpawnLimit(flag);
            end.world.setAmbientSpawnLimit(flag);
            end.world.setWaterAnimalSpawnLimit(flag);
        }
        writeToFile(SimpleWorldManager.getManager().getConfig());
        SimpleWorldManager.getManager().saveConfig();
    }
    
    public int getNeutralMobSpawnCount() {
        return overworld.world.getAnimalSpawnLimit();
    }
    
    //Getters/checkers
    public Location getSpawn() {
        return getOverworld().spawn;
    }
    
    public GameMode getGameMode() {
        return getOverworld().gamemode;
    }
    
    public boolean hasBackpack() {
        return getOverworld().backpack;
    }
    
    public boolean hasEnderchest() {
        return getOverworld().enderchest;
    }
    
    public WorldType getWorldType() {
        return world.getWorldType();
    }
    
    public Environment getEnvironment() {
        return world.getEnvironment();
    }
    
    public boolean spawnsStructures() {
        return world.canGenerateStructures();
    }
    
    public long getSeed() {
        return world.getSeed();
    }
    
    public SimpleWorld getOverworld() {
        return overworld;
    }
    
    public boolean isOverworld() {
        return world.getEnvironment() == Environment.NORMAL;
    }
    
    public SimpleWorld getNether() {
        return nether;
    }
    
    public boolean isNether() {
        return world.getEnvironment() == Environment.NETHER;
    }
    
    public boolean hasNether() {
        return nether != null;
    }
    
    public SimpleWorld getEnd() {
        return end;
    }
    
    public boolean isEnd() {
        return world.getEnvironment() == Environment.THE_END;
    }
    
    public boolean hasEnd() {
        return end != null;
    }
    
    public World getWorld() {
        return world;
    }
    
    public Location getWorldspawn() {
        return world.getSpawnLocation();
    }
    
    public void setSpawn(Location location) {
        if (validateContainsLocation(location)) {
            getOverworld().spawn = location;
        }
    }
    
    public String getName() {
        return world.getName();
    }
    
    private boolean validateContainsLocation(Location location) {
        if (overworld.world.equals(location.getWorld())) { return true; }
        if (hasNether() && getNether().getWorld().equals(location.getWorld())) { return true; }
        if (hasEnd() && getEnd().getWorld().equals(location.getWorld())) { return true; }
        return false;
    }
    
    public void writeToFile(FileConfiguration config) throws SimplyModularFatalException {
        if (!isOverworld()) {
            getOverworld().writeToFile(config);
            return;
        }
        ConfigurationSection section = config.createSection("worlds." + getOverworld().getWorld().getName());
        if (defaultWorld != null && defaultWorld.equals(this)) {
            section.set("default", true);
        }
        section.set("spawn", Utils.getArrayLoc(getSpawn()));
        section.set("pvp", getOverworld().getWorld().getPVP());
        section.set("gamemode", getOverworld().gamemode.name());
        section.set("difficulty", getOverworld().getWorld().getDifficulty().name());
        section.set("hostilemobs", getOverworld().getWorld().getMonsterSpawnLimit());
        section.set("neutralmobs", getOverworld().getWorld().getAnimalSpawnLimit());
        section.set("nether", hasNether() ? getNether().getWorld().getName() : null);
        section.set("end", hasEnd() ? getEnd().getWorld().getName() : null);
        section.set("backpack", backpack);
        section.set("enderchest", enderchest);
        SimpleWorldManager.writeMeta(getOverworld().world);
        if (hasNether()) SimpleWorldManager.writeMeta(getNether().world);
        if (hasEnd()) SimpleWorldManager.writeMeta(getEnd().world);
    }
    
    public Inventory getInventory(String player) {
        if (!isOverworld()) { return getOverworld().getInventory(player); }
        return inventories.get(player);
    }
    
    public Inventory getEnderchest(String player) {
        if (!isOverworld()) { return getOverworld().getEnderchest(player); }
        return enderchests.get(player);
    }
    
    public Inventory getBackpack(String player) {
        if (!isOverworld()) { return getOverworld().getBackpack(player); }
        return backpacks.get(player);
    }
    
    protected void copyInventory(PlayerInventory i, String playername) {
        i.clear();
        Inventory pi = getInventory(playername);
        i.setArmorContents(armorContents.get(playername));
        i.setContents(pi.getContents());
    }
    
    protected void setWorldInventory(PlayerInventory i, String playername) {
        Inventory pi = getInventory(playername);
        pi.clear();
        armorContents.put(playername, i.getArmorContents());
        pi.setContents(i.getContents());
    }
    
    public void saveToWorld(Player player, boolean unload) throws SimplyModularFatalException {
        try {
            File playerFile = new File(playerFiles, player.getUniqueId().toString() + ".nbt");
            playerFile.createNewFile();
            CompoundTag inv, ender, backpack;
            inv = new CompoundTag("inventory");
            ender = new CompoundTag("enderchest");
            backpack = new CompoundTag("backpack");
            
            writeToInventoryTag(getWorld(player).getOverworld().equals(this) ? player.getInventory() : getInventory(player.getName()), inv, player.getName(), false);
            writeToInventoryTag(getEnderchest(player.getName()), ender, player.getName(), false);
            writeToInventoryTag(getBackpack(player.getName()), backpack, player.getName(), false);
            
            if (getWorld(player).getOverworld().equals(this)) {
                CompoundTag worldstats = getWorldStats(player);
                
                CompoundTag tag = new CompoundTag("");
                tag.setTag(inv);
                tag.setTag(ender);
                tag.setTag(backpack);
                tag.setTag(worldstats);
                
                NBTOutputStream out = new NBTOutputStream(new FileOutputStream(playerFile));
                out.writeTag(tag);
                out.close();
            }
            if (unload) {
                inventories.remove(player.getName());
                enderchests.remove(player.getName());
                backpacks.remove(player.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            //throw new SimplyModularFatalException("Unable to create player file for world " + world.getName() + "!", e);
        }
    }
    
    private void writeToInventoryTag(Inventory inventory, CompoundTag tag, String playername, boolean newTag) {
        ListTag list = new ListTag("contents", CompoundTag.class);
        tag.setInt("inventorySize", inventory.getSize());
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack item = inventory.getItem(i);
            if (item != null) {
                net.minecraft.server.v1_7_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy((CraftItemStack) item);
                CompoundTag jnbtTag = (CompoundTag) Utils.jnbtFROMnms("", nmsItem.save(new NBTTagCompound()));
                jnbtTag.setInt("index", i);
                list.addTag(jnbtTag);
            }
        }
        Debug.logDebug("Inventory name: " + tag.getName());
        if (tag.getName().equals("inventory") && !newTag) {
            Debug.logDebug("Saving armor ");
            ListTag armor = new ListTag("armor", CompoundTag.class);
            ItemStack[] items = inventory instanceof PlayerInventory ? ((PlayerInventory) inventory).getArmorContents() : armorContents.get(playername);
            for (int i = 0; i < 4; i++) {
                ItemStack item = items[i];
                if (item == null || item.getType() == Material.AIR) {
                    Debug.logDebug("Armor is null, passing");
                    continue;
                }
                Debug.logDebug("armor not null, writing...");
                CompoundTag jnbtTag = (CompoundTag) Utils.jnbtFROMnms("", CraftItemStack.asNMSCopy((CraftItemStack) item).save(new NBTTagCompound()));
                jnbtTag.setInt("index", i);
                armor.addTag(jnbtTag);
            }
            //			for (ItemStack piece : armorContents.get(playername)) {
            //				net.minecraft.server.v1_7_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy((CraftItemStack) piece);
            //				CompoundTag jnbtTag = (CompoundTag) Utils.jnbtFROMnms("", nmsItem.save(new NBTTagCompound()));
            //				armor.addTag(jnbtTag);
            //			}
            tag.setList("armor", armor);
        }
        tag.setList("contents", list);
    }
    
    protected void loadInventories(String player) throws SimplyModularFatalException {
        CompoundTag tag = getPlayerCompoundTag(player);
        inventories.put(player, getInventory(tag.getCompoundTag("inventory"), player, true));
        enderchests.put(player, getInventory(tag.getCompoundTag("enderchest"), player, false));
        backpacks.put(player, getInventory(tag.getCompoundTag("backpack"), player, false));
    }
    
    private CompoundTag getPlayerCompoundTag(String name) throws SimplyModularFatalException {
        try {
            UUID id = Bukkit.getOfflinePlayer(name).getUniqueId();
            File playerfile = new File(playerFiles, id.toString() + ".nbt");
            if (!playerfile.exists()) {
                Inventory mockInventory = Bukkit.createInventory(null, 54);
                
                CompoundTag inv, ender, backpack;
                inv = new CompoundTag("inventory");
                ender = new CompoundTag("enderchest");
                backpack = new CompoundTag("backpack");
                
                writeToInventoryTag(Bukkit.createInventory(null, 36), inv, name, true);
                inv.setList("armor", new ListTag("armor", CompoundTag.class));
                writeToInventoryTag(mockInventory, ender, name, true);
                writeToInventoryTag(mockInventory, backpack, name, true);
                
                if (getDefaultWorld().equals(this)) {
                    //TODO import old inventories and enderchests to the default world, should admin wish it.
                }
                
                CompoundTag worldstats = new CompoundTag("worldstats");
                ListTag effectsList = new ListTag("potionEffects", CompoundTag.class);
                worldstats.setList("potionEffects", effectsList);
                
                //Hunger, health, saturation, etc.
                float exhaustion, exp, saturation;
                int foodlvl, level, totalxp;
                double health;
                
                exhaustion = 0;
                exp = 0;
                saturation = 5.0f;
                foodlvl = 20;
                level = 0;
                totalxp = 0;
                health = 20;
                
                worldstats.setFloat("exhaustion", exhaustion);
                worldstats.setFloat("exp", exp);
                worldstats.setFloat("saturation", saturation);
                worldstats.setInt("foodlvl", foodlvl);
                worldstats.setInt("health", (int) health);
                worldstats.setInt("level", level);
                worldstats.setInt("totalxp", totalxp);
                worldstats.setDouble("health", health);
                
                CompoundTag tag = new CompoundTag("");
                tag.setTag(inv);
                tag.setTag(ender);
                tag.setTag(backpack);
                tag.setTag(worldstats);
                
                playerfile.createNewFile();
                NBTOutputStream stream = new NBTOutputStream(new FileOutputStream(playerfile));
                stream.writeTag(tag);
                stream.close();
            }
            NBTInputStream in = new NBTInputStream(new FileInputStream(playerfile));
            CompoundTag tag = (CompoundTag) in.readTag();
            in.close();
            return tag;
        } catch (Exception e) {
            throw new SimplyModularFatalException("Unable to create player file for world " + world.getName() + "!", e);
        }
    }
    
    private Inventory getInventory(CompoundTag tag, String playername, boolean playerInventory) {
        Inventory inventory = Bukkit.createInventory(null, tag.getInt("inventorySize"));
        for (Tag itemtag : tag.getList("contents").getValue()) {
            CompoundTag citemtag = (CompoundTag) itemtag;
            int index = citemtag.getInt("index");
            citemtag.removeEntry("index");
            NBTTagCompound nmsTag = (NBTTagCompound) Utils.nmsFROMjnbt(citemtag);
            net.minecraft.server.v1_7_R3.ItemStack is = net.minecraft.server.v1_7_R3.ItemStack.createStack(nmsTag);
            CraftItemStack cis = CraftItemStack.asCraftMirror(is);
            inventory.setItem(index, cis);
        }
        if (playerInventory) {
            ItemStack[] items = new ItemStack[4];
            ListTag armor = tag.getList("armor");
            for (Tag armortag : armor.getValue()) {
                CompoundTag citemtag = (CompoundTag) armortag;
                NBTTagCompound nmsTag = (NBTTagCompound) Utils.nmsFROMjnbt(citemtag);
                net.minecraft.server.v1_7_R3.ItemStack is = net.minecraft.server.v1_7_R3.ItemStack.createStack(nmsTag);
                CraftItemStack cis = CraftItemStack.asCraftMirror(is);
                items[citemtag.getInt("index")] = cis;
            }
            armorContents.put(playername, items);
        }
        
        return inventory;
    }
    
    private CompoundTag getWorldStats(Player player) {
        CompoundTag tag = new CompoundTag("worldstats");
        ListTag effectList = new ListTag("potionEffects", CompoundTag.class);
        ArrayList<PotionEffect> effects = (ArrayList<PotionEffect>) player.getActivePotionEffects();
        for (PotionEffect pe : effects) {
            CompoundTag effectTag = new CompoundTag("effect");
            effectTag.setByte("Amplifier", (byte) pe.getAmplifier());
            effectTag.setByte("Id", (byte) pe.getType().getId());
            effectTag.setInt("Duration", pe.getDuration());
            effectList.addTag(effectTag);
        }
        tag.setList("potionEffects", effectList);
        
        //Hunger, health, saturation, etc.
        float exhaustion, exp, saturation;
        int foodlvl, level, totalxp;
        double health;
        
        exhaustion = player.getExhaustion();
        exp = player.getExp();
        saturation = player.getSaturation();
        foodlvl = player.getFoodLevel();
        health = player.getHealth();
        if (health < 0) health = 0;
        level = player.getLevel();
        totalxp = player.getTotalExperience();
        
        tag.setFloat("exhaustion", exhaustion);
        tag.setFloat("exp", exp);
        tag.setFloat("saturation", saturation);
        tag.setInt("foodlvl", foodlvl);
        tag.setInt("level", level);
        tag.setInt("totalxp", totalxp);
        tag.setDouble("health", health);
        
        return tag;
    }
    
    protected void setWorldStats(Player player) throws SimplyModularFatalException {
        CompoundTag tag = getPlayerCompoundTag(player.getName()).getCompoundTag("worldstats");
        
        //Hunger, health, saturation, etc.
        float exhaustion, exp, saturation;
        int foodlvl, level, totalxp;
        double health;
        exhaustion = tag.getFloat("exhaustion");
        exp = tag.getFloat("exp");
        saturation = tag.getFloat("saturation");
        
        foodlvl = tag.getInt("foodlvl");
        level = tag.getInt("level");
        totalxp = tag.getInt("totalxp");
        health = tag.getDouble("health");
        
        player.setExhaustion(exhaustion);
        player.setExp(exp);
        player.setSaturation(saturation);
        player.setFoodLevel(foodlvl);
        player.setHealth(health);
        player.setLevel(level);
        player.setTotalExperience(totalxp);
    }
}
