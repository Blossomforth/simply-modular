/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.module.core.SimpleWorldManagement;

import me.corriekay.simplymodular.utils.Debug;

import org.bukkit.*;
import org.bukkit.World.Environment;
import org.bukkit.configuration.ConfigurationSection;

public class WorldGeneratorOptions {
    private boolean pvp = false;
    private GameMode gamemode = GameMode.SURVIVAL;
    private Difficulty difficulty = Difficulty.NORMAL;
    private Integer hostilemobs = 70, neutralmobs = 15;
    private String name;
    private Environment environment;
    private boolean backpack = true, enderchest = true;
    private WorldType worldtype = WorldType.NORMAL;
    private boolean structures = true;
    private String seed;
    
    public WorldGeneratorOptions(String name) {
        this.setName(name);
    }
    
    public WorldGeneratorOptions(ConfigurationSection config, String name, Environment environment) {
        setName(name);
        setEnvironment(environment);
        pvp = config.getBoolean("pvp");
        gamemode = GameMode.valueOf(config.getString("gamemode", "SURVIVAL"));
        difficulty = Difficulty.valueOf(config.getString("difficulty", "NORMAL"));
        hostilemobs = config.getInt("hostilemobs");
        neutralmobs = config.getInt("neutralmobs");
        backpack = config.getBoolean("backpack");
        Debug.logDebug("UGO: setting backpack to " + backpack);
        enderchest = config.getBoolean("enderchest");
        Debug.logDebug("UGO: setting enderchest to " + enderchest);
        worldtype = WorldType.valueOf(config.getString("worldtype", worldtype.name()));
        structures = config.getBoolean("structures", structures);
        seed = config.getString("seed", seed);
    }
    
    public boolean getPVP() {
        return pvp;
    }
    
    public GameMode getGameMode() {
        return gamemode;
    }
    
    public Difficulty getDifficulty() {
        return difficulty;
    }
    
    public int gethostileMobSpawnCount() {
        return hostilemobs;
    }
    
    public int getNeutralMobSpawnCount() {
        return neutralmobs;
    }
    
    public boolean getBackpack() {
        return backpack;
    }
    
    public boolean getEnderchest() {
        return enderchest;
    }
    
    public WorldType getWorldType() {
        return worldtype;
    }
    
    public boolean getStructures() {
        return structures;
    }
    
    public String getSeed() {
        return seed;
    }
    
    public Environment getEnvironment() {
        return environment;
    }
    
    public String getName() {
        return name;
    }
    
    public WorldGeneratorOptions setPVP(boolean flag) {
        pvp = flag;
        return this;
    }
    
    public WorldGeneratorOptions setGameMode(GameMode flag) {
        gamemode = flag;
        return this;
    }
    
    public WorldGeneratorOptions setDifficulty(Difficulty flag) {
        difficulty = flag;
        return this;
    }
    
    public WorldGeneratorOptions setHostileMobSpawnLimit(int flag) {
        hostilemobs = flag;
        return this;
    }
    
    public WorldGeneratorOptions setNeutralMobSpawnLimit(int flag) {
        neutralmobs = flag;
        return this;
    }
    
    public WorldGeneratorOptions setBackpacks(boolean flag) {
        backpack = flag;
        return this;
    }
    
    public WorldGeneratorOptions setEnderchest(boolean flag) {
        enderchest = flag;
        return this;
    }
    
    public WorldGeneratorOptions setWorldType(WorldType flag) {
        worldtype = flag;
        return this;
    }
    
    public WorldGeneratorOptions setSpawnStructures(boolean flag) {
        structures = flag;
        return this;
    }
    
    public WorldGeneratorOptions setSeed(String flag) {
        seed = flag;
        return this;
    }
    
    public WorldGeneratorOptions setEnvironment(Environment flag) {
        environment = flag;
        return this;
    }
    
    public WorldGeneratorOptions setName(String flag) {
        name = flag;
        return this;
    }
    
    public static class UniverseGeneratorOptions extends WorldGeneratorOptions {
        private String nether = null, end = null;
        
        public UniverseGeneratorOptions(String name) {
            super(name);
            setEnvironment(Environment.NORMAL);
        }
        
        public String getNetherName() {
            return nether;
        }
        
        public void setNether(String flag) {
            nether = flag;
        }
        
        public String getEndName() {
            return end;
        }
        
        public void setEndName(String flag) {
            end = flag;
        }
    }
}
