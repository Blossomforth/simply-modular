/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.utils;

/**
 * @Class: Utils
 * @Author: CorrieKay
 * @Purpose: Utils class. Non-instantiated, abstract. Contains utility methods, (functions hurr hurr)
 */
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import net.minecraft.server.v1_7_R3.*;

import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.jnbt.*;

import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.regions.Region;

public abstract class Utils {
    
    public static String getDate(long time) {
        Date date = new Date(time);
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
    }
    
    public static String getDatedTimestamp() {
        return getDate(System.currentTimeMillis());
    }
    
    public static String getSystemTime() {
        return getSystemTime(System.currentTimeMillis());
    }
    
    public static String getSystemTime(long time) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date cal = Calendar.getInstance().getTime();
        cal.setTime(time);
        return dateFormat.format(cal.getTime());
    }
    
    public static String getFileDate() {
        return getFileDate(System.currentTimeMillis());
    }
    
    public static String getFileDate(long time) {
        Date date = new Date(time);
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
    
    public static String getTimeStamp() {
        return getTimeStamp(System.currentTimeMillis());
    }
    
    public static String getTimeStamp(long time) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date cal = Calendar.getInstance().getTime();
        cal.setTime(time);
        return dateFormat.format(cal.getTime());
    }
    
    public static HashSet<Block> getBlocks(int length, int height, int width, Location loc) {
        HashSet<Block> blocks = new HashSet<Block>();
        double startX = loc.getX() - length / 2;
        double startY = loc.getY() - height / 2;
        double startZ = loc.getZ() - width / 2;
        double y = startY;
        double z = startZ;
        double x = startX;
        double maxX = loc.getX() + length / 2;
        double maxY = loc.getY() + height / 2;
        double maxZ = loc.getZ() + width / 2;
        boolean Continue = true;
        while (Continue) {
            if (x < maxX) {
                x++;
            } else {
                x = startX;
                if (y < maxY) {
                    y++;
                } else {
                    y = startY;
                    if (z < maxZ) {
                        z++;
                    } else {
                        y = startY;
                        Continue = false;
                    }
                }
            }
            Location loc2 = new Location(loc.getWorld(), x, y, z);
            blocks.add(loc2.getBlock());
        }
        return blocks;
    }
    
    public static ArrayList<String> getArrayLoc(Location l) {
        ArrayList<String> array = new ArrayList<String>();
        if (l != null) {
            array.add(l.getWorld().getName());
            array.add(l.getX() + "");
            array.add(l.getY() + "");
            array.add(l.getZ() + "");
            array.add(l.getPitch() + "");
            array.add(l.getYaw() + "");
        }
        return array;
    }
    
    public static ListTag getListTagLoc(Location l, String name) {
        ListTag tag = new ListTag(name, StringTag.class);
        if (l != null) {
            tag.addTag(new StringTag("world", l.getWorld().getName()));
            tag.addTag(new StringTag("x", l.getBlockX() + ""));
            tag.addTag(new StringTag("y", l.getBlockY() + ""));
            tag.addTag(new StringTag("z", l.getBlockZ() + ""));
            tag.addTag(new StringTag("pitch", l.getPitch() + ""));
            tag.addTag(new StringTag("yaw", l.getYaw() + ""));
            return tag;
        } else return null;
    }
    
    public static Location getLoc(ArrayList<String> array) {
        if (array == null) { return null; }
        try {
            World w;
            double x, y, z;
            float pitch, yaw;
            w = Bukkit.getWorld(array.get(0));
            x = Double.parseDouble(array.get(1));
            y = Double.parseDouble(array.get(2));
            z = Double.parseDouble(array.get(3));
            pitch = Float.parseFloat(array.get(4));
            yaw = Float.parseFloat(array.get(5));
            Location l = new Location(w, x, y, z);
            l.setPitch(pitch);
            l.setYaw(yaw);
            return l;
        } catch (Exception e) {
            return null;
        }
    }
    
    public static Location getLoc(ListTag tag) {
        if (tag == null) { return null; }
        try {
            World w;
            double x, y, z;
            float pitch, yaw;
            w = Bukkit.getWorld(tag.getValue().get(0).getValue().toString());
            x = Double.parseDouble(tag.getValue().get(1).getValue().toString());
            y = Double.parseDouble(tag.getValue().get(2).getValue().toString());
            z = Double.parseDouble(tag.getValue().get(3).getValue().toString());
            pitch = Float.parseFloat(tag.getValue().get(4).getValue().toString());
            yaw = Float.parseFloat(tag.getValue().get(5).getValue().toString());
            Location l = new Location(w, x, y, z);
            l.setPitch(pitch);
            l.setYaw(yaw);
            return l;
        } catch (Exception e) {
            return null;
        }
    }
    
    private static final HashSet<Character> acceptablechars = new HashSet<Character>(Arrays.asList(new Character[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' }));
    
    public static String alphanumbericalize(String oldString) {
        return alphanumericalize(oldString, new Character[] {});
    }
    
    public static String alphanumericalize(String oldString, Character[] otherChars) {
        StringBuilder sb = new StringBuilder();
        char[] charArray = oldString.toCharArray();
        ArrayList<Character> acceptablechars = new ArrayList<Character>(Utils.acceptablechars);
        for (Character character : otherChars) {
            acceptablechars.add(character);
        }
        for (char c : charArray) {
            if (acceptablechars.contains(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    public static boolean intersects(Selection s1, Selection s2) {
        Location s1Min, s1Max, s2Min, s2Max;
        s1Min = s1.getMinimumPoint();
        s1Max = s1.getMaximumPoint();
        s2Min = s2.getMinimumPoint();
        s2Max = s2.getMaximumPoint();
        
        if ((s1Min.getBlockX() <= s2Max.getBlockX()) && (s2Min.getBlockX() <= s1Max.getBlockX())) {
            if ((s1Min.getBlockZ() <= s2Max.getBlockZ()) && (s2Min.getBlockZ() <= s1Max.getBlockZ())) { return true; }
        }
        return false;
    }
    
    public static boolean intersects(Region s1, Region s2) {
        com.sk89q.worldedit.Vector s1Min, s1Max, s2Min, s2Max;
        s1Min = s1.getMinimumPoint();
        s1Max = s1.getMaximumPoint();
        s2Min = s2.getMinimumPoint();
        s2Max = s2.getMaximumPoint();
        
        if ((s1Min.getBlockX() <= s2Max.getBlockX()) && (s2Min.getBlockX() <= s1Max.getBlockX())) {
            if ((s1Min.getBlockZ() <= s2Max.getBlockZ()) && (s2Min.getBlockZ() <= s1Max.getBlockZ())) { return true; }
        }
        return false;
    }
    
    public static String joinStringArray(String[] args) {
        return joinStringArray(args, "");
    }
    
    public static String joinArrayList(ArrayList<String> args) {
        return joinArrayList(args, "");
    }
    
    public static String joinStringSet(Set<String> args) {
        return joinStringSet(args, "");
    }
    
    public static String joinStringArray(String[] args, String delimiter) {
        return joinStringArray(args, delimiter, 0);
    }
    
    public static String joinArrayList(ArrayList<String> args, String delimiter) {
        return joinArrayList(args, delimiter, 0);
    }
    
    public static String joinStringSet(Set<String> args, String delimiter) {
        return joinStringSet(args, delimiter, 0);
    }
    
    public static String joinStringArray(String[] args, String delimiter, int startingIndex) {
        StringBuilder s = new StringBuilder();
        for (int i = startingIndex; i < args.length; i++) {
            s.append(args[i]);
            if (!(i + 1 >= args.length)) {
                s.append(delimiter);
            }
        }
        return s.toString();
    }
    
    public static String joinArrayList(ArrayList<String> args, String delimiter, int startingIndex) {
        StringBuilder s = new StringBuilder();
        for (int i = startingIndex; i < args.size(); i++) {
            s.append(args.get(i));
            if (!(i + 1 >= args.size())) {
                s.append(delimiter);
            }
        }
        return s.toString();
    }
    
    public static String joinStringSet(Set<String> args, String delimiter, int startingIndex) {
        int count = startingIndex;
        StringBuilder s = new StringBuilder();
        for (String string : args) {
            if (count >= startingIndex) {
                s.append(string);
                if (count + 1 < args.size()) {
                    s.append(delimiter);
                }
            }
            count++;
        }
        return s.toString();
    }
    
    public static String getStringFromIndex(int index, String[] args) {
        String s = "";
        for (int i = index; i < args.length; i++) {
            s += args[i] + " ";
        }
        return s.trim();
    }
    
    public static void purgeDirectory(File directory) {
        File[] files = directory.listFiles();
        if (files != null) for (File file : files) {
            if (file.isDirectory()) {
                purgeDirectory(file);
            } else {
                file.delete();
            }
        }
        directory.delete();
    }
    
    public static int highestYAt(Location l) {
        switch (l.getWorld().getEnvironment()) {
            case NETHER: {
                Location loc = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
                int level = l.getWorld().getMaxHeight() - 1;
                boolean foundAir = false;
                boolean foundBedrock = false;
                while (level >= 0) {
                    loc.setY(level);
                    Block b = loc.getBlock();
                    //Debug.logDebug("Checking nether! Level: " + level + " Blocktype: " + b.getType().name());
                    if (!foundBedrock && b.getType() == Material.BEDROCK) {
                        //Debug.logDebug("Bedrock flag tripped, We found bedrock!");
                        foundBedrock = true;
                    }
                    if (!foundAir && foundBedrock && b.getType() == Material.AIR) {
                        //Debug.logDebug("Air flag tripped! We found air!");
                        foundAir = true;
                    }
                    if (foundAir && foundBedrock && b.getType() != Material.AIR) {
                        //Debug.logDebug("Found highest realistic block in nether!");
                        return level;
                    }
                    
                    level--;
                }
                return 0;
            }
            default: {
                Location loc = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
                int level = l.getWorld().getMaxHeight();
                while (level >= 0) {
                    loc.setY(--level);
                    Block b = loc.getBlock();
                    if (b.getType() != Material.AIR) {
                        //Debug.logDebug("materialType: " + b.getType().name());
                        return level;
                    }
                    level--;
                }
                return 0;
            }
        }
    }
    
    public static Location transformHighestYAt(Location l) {
        l.setY(highestYAt(l));
        return l;
    }
    
    public static NBTBase nmsFROMjnbt(Tag tag) {
        if (tag instanceof CompoundTag) {
            CompoundTag jnbtCompound = (CompoundTag) tag;
            NBTTagCompound ctag = new NBTTagCompound();
            for (String key : jnbtCompound.getValue().keySet()) {
                Tag jnbtValue = jnbtCompound.getValue().get(key);
                NBTBase value = nmsFROMjnbt(jnbtValue);
                ctag.set(jnbtValue.getName(), value);
            }
            return ctag;
        } else if (tag instanceof ByteTag) {
            ByteTag jnbtByte = (ByteTag) tag;
            return new NBTTagByte(jnbtByte.getValue());
        } else if (tag instanceof ByteArrayTag) {
            ByteArrayTag jnbtByteArray = (ByteArrayTag) tag;
            return new NBTTagByteArray(jnbtByteArray.getValue());
        } else if (tag instanceof DoubleTag) {
            DoubleTag jnbtDouble = (DoubleTag) tag;
            return new NBTTagDouble(jnbtDouble.getValue());
        } else if (tag instanceof FloatTag) {
            FloatTag jnbtFloat = (FloatTag) tag;
            return new NBTTagFloat(jnbtFloat.getValue());
        } else if (tag instanceof IntTag) {
            IntTag jnbtInt = (IntTag) tag;
            return new NBTTagInt(jnbtInt.getValue());
        } else if (tag instanceof ListTag) {
            ListTag jnbtList = (ListTag) tag;
            NBTTagList nbtlist = new NBTTagList();
            for (int i = 0; i < jnbtList.getValue().size(); i++) {
                nbtlist.add(nmsFROMjnbt(jnbtList.getValue().get(i)));
            }
            return nbtlist;
        } else if (tag instanceof LongTag) {
            LongTag jnbtLong = (LongTag) tag;
            return new NBTTagLong(jnbtLong.getValue());
        } else if (tag instanceof ShortTag) {
            ShortTag jnbtShort = (ShortTag) tag;
            return new NBTTagShort(jnbtShort.getValue());
        } else if (tag instanceof StringTag) {
            StringTag jnbtString = (StringTag) tag;
            return new NBTTagString(jnbtString.getValue());
        } else {
            return null;
        }
    }
    
    public static Tag jnbtFROMnms(String name, NBTBase nbtBase) {
        if (nbtBase instanceof NBTTagCompound) {
            NBTTagCompound nmsCompound = (NBTTagCompound) nbtBase;
            CompoundTag ctag = new CompoundTag(name);
            for (Object key : nmsCompound.c()) {
                ctag.setTag(jnbtFROMnms(key.toString(), nmsCompound.get((String) key)));
            }
            return ctag;
        } else if (nbtBase instanceof NBTTagByte) {
            NBTTagByte byteTag = (NBTTagByte) nbtBase;
            return new ByteTag(name, byteTag.f());
        } else if (nbtBase instanceof NBTTagByteArray) {
            NBTTagByteArray byteArrayTag = (NBTTagByteArray) nbtBase;
            return new ByteArrayTag(name, byteArrayTag.c());
        } else if (nbtBase instanceof NBTTagDouble) {
            NBTTagDouble doubleTag = (NBTTagDouble) nbtBase;
            return new DoubleTag(name, doubleTag.g());
        } else if (nbtBase instanceof NBTTagFloat) {
            NBTTagFloat floatTag = (NBTTagFloat) nbtBase;
            return new FloatTag(name, floatTag.h());
        } else if (nbtBase instanceof NBTTagInt) {
            NBTTagInt intTag = (NBTTagInt) nbtBase;
            return new IntTag(name, intTag.d());
        } else if (nbtBase instanceof NBTTagList) {
            System.out.println("TagList: " + name);
            NBTTagList list = (NBTTagList) nbtBase;
            //int tagType = getTagTypeFromNMS(list.d());
            ListTag list2 = new ListTag(name, getTagTypeFromNMS(list.d()));
            for (int i = 0; i < list.size(); i++) {
                list2.addTag(getJNBTTagFromNMSList(list, i));
            }
            return list2;
        } else if (nbtBase instanceof NBTTagLong) {
            NBTTagLong longTag = (NBTTagLong) nbtBase;
            return new LongTag(name, longTag.c());
        } else if (nbtBase instanceof NBTTagShort) {
            NBTTagShort shortTag = (NBTTagShort) nbtBase;
            return new ShortTag(name, shortTag.e());
        } else if (nbtBase instanceof NBTTagString) {
            NBTTagString stringTag = (NBTTagString) nbtBase;
            return new StringTag(name, stringTag.a_());
        } else {
            return null;
        }
    }
    
    public static Class<? extends Tag> getTagTypeFromNMS(int i) {
        switch (i) {
            case 1:
                return ByteTag.class;
            case 2:
                return ShortTag.class;
            case 3:
                return IntTag.class;
            case 4:
                return LongTag.class;
            case 5:
                return FloatTag.class;
            case 6:
                return DoubleTag.class;
            case 7:
                return ByteArrayTag.class;
            case 8:
                return StringTag.class;
            case 9:
                return ListTag.class;
            case 10:
                return CompoundTag.class;
            default:
                /*
                 * At this point, this means that the NMS NBT tag list is returning a 0 for tag type.
                 * This means that the tag list is empty, and a type has not been chosen. This can
                 * cause some issues, as JNBT requires a tag type for the list. The good news is that
                 * when converting back to NMS, the jnbt list type means jack shit. The nms tag list type
                 * is decided when the first entry is given to it. Since there will be nothing added to
                 * it, returning any tag type is acceptable. IF we are changing it, then that means that
                 * we know what we're changing, and we can set the JNBT tag list to whatever tag type we'll
                 * be having then. If its empty, then it doesnt matter, as minecraft knows how to
                 * handle an empty, essentially uninitialized nms nbt list.
                 */
                return CompoundTag.class;
        }
    }
    
    public static Tag getJNBTTagFromNMSList(NBTTagList listtag, int index) {
        int type = listtag.d();
        System.out.println("Printing list type: " + type);
        switch (type) {
            case 5:
                return new FloatTag("", listtag.e(index));
            case 6:
                return new DoubleTag("", listtag.d(index));
            case 8:
                return new StringTag("", listtag.f(index));
            case 10:
                return jnbtFROMnms("", listtag.get(index));
            default:
                System.out.println("populating jnbt list returning null");
                return null;
        }
    }
}
