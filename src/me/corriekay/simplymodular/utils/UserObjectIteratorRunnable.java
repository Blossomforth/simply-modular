/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.utils;

import java.io.File;
import java.util.UUID;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;

import org.bukkit.Bukkit;

public abstract class UserObjectIteratorRunnable implements Runnable {
    
    @Override
    public final void run() {
        UserManager um = UserManager.getManager();
        
        File dir = new File(SimplyModular.getSM().getDataFolder(), "Players");
        
        for (File file : dir.listFiles()) {
            UUID uuid = UUID.fromString(file.getName().substring(0, file.getName().length() - 4));
            
            try {
                User user = um.getUser(Bukkit.getOfflinePlayer(uuid).getName());
                parseUser(user);
                user.save();
            } catch (SimplyModularFatalModuleException e) {
                e.printStackTrace();
            }
        }
    }
    
    public abstract void parseUser(User user);
    
}
