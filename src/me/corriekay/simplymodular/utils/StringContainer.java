/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.utils;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import me.corriekay.simplymodular.SimplyModular;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class StringContainer {
    
    private static FileConfiguration strings;
    private static File file;
    private static ChatColor defaultColor;
    
    public static void loadStrings() throws SimplyModularFatalException {
        file = new File(SimplyModular.getSM().getDataFolder(), "strings.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new SimplyModularFatalException("StringContainer exception on boot: IO Exception when attempting to create a new file. Please check your file permissions for the server directory!", e);
            }
        }
        strings = YamlConfiguration.loadConfiguration(file);
        repairConfig();
    }
    
    public static String getString(StringType st) {
        String returnme = strings.getString(st.name());
        if (returnme == null) {
            Debug.log("STRING NOT FOUND! Applying defaults to config file...", Level.SEVERE);
            repairConfig();
            returnme = strings.getString(st.name());
        }
        return returnme;
    }
    
    public static void repairConfig() {
        Debug.logHighlyVerbose("Running maintennance on Strings file", false);
        @SuppressWarnings("deprecation")
        FileConfiguration config = YamlConfiguration.loadConfiguration(StringContainer.class.getClassLoader().getResourceAsStream("strings.yml"));
        if (strings != null) {
            for (String s : config.getKeys(false)) {
                if (!strings.contains(s)) {
                    strings.set(s, config.getString(s));
                }
            }
        }
        try {
            defaultColor = ChatColor.valueOf(strings.getString("defaultColor"));
            Debug.logVerbose("Default color found. Setting to " + defaultColor.name(), false);
        } catch (Exception e) {
            Debug.logVerbose("Default color not recognized. Defaulting to Dark Gray", false);
            defaultColor = ChatColor.DARK_GRAY;
            strings.set("defaultColor", "DARK_GRAY");
        }
        try {
            strings.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        strings.set("defaultColor", null);
    }
    
    public static void sendMessage(CommandSender receiver, StringType type, Object... args) {
        if (getString(type).equals("")) { return; }
        receiver.sendMessage(format(getString(type), args));
    }
    
    public static String getMessage(StringType type, Object... args) {
        return format(getString(type), args);
    }
    
    private static String format(String s, Object... args) {
        s = defaultColor + s;
        s = s.replaceAll("<&>", defaultColor.toString());
        s = ChatColor.translateAlternateColorCodes('&', s);
        for (int i = 1; i < args.length + 1; i++) {
            s = s.replaceAll("%" + i, args[i - 1].toString() + defaultColor);
        }
        return s.replaceAll("\\\\n", "\n");
    }
    
    public static enum StringType {
        //Generic
        reenterServer,
        firstJoinServer,
        leaveServer,
        denyNoPermission,
        denyMinArgs,
        metaCommandIncorrectArgs,
        denyNotNumber,
        notConsole,
        notPlayer,
        tooManyMatches,
        playerNotFound,
        worldNotFound,
        cannotTargetOperatorWithThisCommand,
        nicknameSet,
        
        //SimplePermissions
        permissionsReloaded,
        listNumberPlayers,
        permissionNoGroupFound,
        permissionAddGroupPermissionPermissionExists,
        permissionAddGroupPermission,
        permissionRemoveGroupPermissionDoesNotExist,
        permissionRemoveGroupPermission,
        permissionPersonalAddPermissionPermissionExists,
        permissionPersonalAddPermission,
        permissionPersonalRemovePermissionDoesNotExist,
        permissionPersonalRemovePermission,
        groupSetDenyAlreadyInGroup,
        groupSetDenyRequiresConsole,
        groupSetApproveMessage,
        groupSetApprovePlayerMessage,
        
        //SimpleChestProtect
        protectCancel,
        protectSuccess,
        protectActivate,
        protectWrongBlocktype,
        protectIgnoredWorld,
        protectExistingProtection,
        protectGlobalCapViolated,
        protectSpecificCapViolated,
        protectDestroyProtectionDenyNotOwner,
        protectDestroyProtectionNotifyOwner,
        unprotectRemoveProtectionCommand,
        unprotectRemoveProtectionCommandStop,
        unprotectRemoveProtectionNoProtection,
        unprotectRemoveProtectionNoPermission,
        unprotectRemoveProtectionSuccessful,
        buildProtectionConversationStart,
        buildProtectionConversationPromptType,
        buildProtectionConversationPromptAdditionalPermissions,
        buildProtectionConversationFeedbackAdditionalPermissionsAdd,
        buildProtectionConversationFeedbackAdditionalPermissionsAddAlreadyExists,
        buildProtectionConversationFeedbackAdditionalPermissionsRemove,
        buildProtectionConversationFeedbackAdditionalPermissionsRemoveAlreadyRemoving,
        buildProtectionConversationSetup,
        buildProtectionConversationFinish,
        buildProtectionPaintUpdate,
        buildProtectionPaintAdd,
        buildProtectionPaintDenyDoesNotOwnProtection,
        
        //InvisibilityHandler
        pickupEnabled,
        pickupDisabled,
        invisibilityTurnedOn,
        invisibilityTurnedOff,
        invisibilityTurnedOnAlert,
        invisibilityTurnedOffAlert,
        invisibilityLoggedOnAlert,
        invisibilityLoggedOffAlert,
        invisibilitySetInvisLevel,
        invisibilitySetInvisLevelSameLevel,
        invisibilityDefaultInvisLevel,
        invisibilityDefaultInvisLevelAlreadyDefault,
        
        //Chat Manager
        chatManagerJoinChannelChat,
        chatManagerLeaveChannelChat,
        chatManagerAlreadyChatting,
        chatManagerJoinChannelListen,
        chatManagerLeaveChannelListen,
        chatManagerAlreadyListening,
        chatManagerLeaveChannelNoChannel,
        chatManagerLeaveChannelNotInChannel,
        chatManagerLeftAllChannels,
        chatManagerAttemptJoinDeny,
        chatManagerAttemptJoinNoPwOrPwInvalid,
        chatManagerAttemptChatGlobalMute,
        chatManagerAttemptChatNoChattingChannel,
        chatManagerAttemptChatChannelMute,
        chatManagerChannelListCommand,
        chatManagerChannelNullNotification,
        chatManagerNotListeningToChannelNotification,
        chatManagerMatureChannelWarning,
        chatManagerMatureChannelAccept,
        chatManagerMatureChannelDecline,
        chatManagerNotifyMuted,
        chatManagerNotifyUnmuted,
        chatManagerMutedPlayer,
        chatManagerUnmutedPlayer,
        chatManagerSendMessageFail,
        chatManagerRecievePrivateMessage,
        chatManagerSendPrivateMessage,
        chatManagerPmSpyEvent,
        chatManagerSendPrivateMessageFailNull,
        chatManagerIgnoreCannotIgnoreOP,
        chatManagerIgnoreSuccessful,
        chatManagerUnignoreSuccessful,
        chatManagerNoIgnores,
        chatManagerDisplayIgnores,
        chatManagerPmFailTargetIgnored,
        chatManagerPMSpyEnable,
        chatManagerPMSpyDisable,
        chatManagerInvalidColor,
        chatManagerSetChannelColor,
        chatManagerUserChannelCreateAlreadyAdmin,
        chatManagerUserChannelCreateAliasExists,
        chatManagerUserChannelCreateSuccess,
        chatManagerUserChannelNotInUserChannel,
        chatManagerUserChannelNotAdmin,
        chatManagerUserChannelSetQuickCommandExists,
        chatManagerUserChannelSetQuickSuccess,
        chatManagerUserChannelSetPasswordSuccess,
        chatManagerUserChannelRemovePassword,
        chatManagerUserChannelAddMod,
        chatManagerUserChannelModded,
        chatManagerUserChannelRemoveMod,
        chatManagerUserChannelDemodded,
        chatManagerUserChannelBanAlreadyBanned,
        chatManagerUserChannelCannotBanAdmin,
        chatManagerUserChannelBanNotification,
        chatManagerUserChannelUserBannedNotification,
        chatManagerUserChannelUnbanNotBanned,
        chatManagerUserChannelUnbanNotification,
        chatManagerUserChannelUserUnbannedNotification,
        channelManagerUserChannelAdminChangeNotification,
        chatManagerUserChannelAdminChangeConfirmation,
        channelManagerUserChannelAdminPromotionNotification,
        chatManagerUserChannelAdminChangeCannotPromoteAlreadyAdmin,
        channelManagerUserChannelKickNotInChannel,
        channelManagerUserChannelKickNotification,
        channelManagerUserChannelKickStaffNotification,
        channelManagerUserChannelRetireConfirmation,
        channelManagerUserChannelRetiringBroadcast,
        
        //World Manager
        worldManagerNotifyChangeWorld,
        worldManagerNotifyCannotFindWorld,
        worldManagerNotifyWorldSpawnSet,
        
        worldManagerCreateWorldAlreadyExists,
        worldManagerCreateWorldAlreadyCreatingWorld,
        worldManagerCreateWorldExistsNotOfWorldtype,
        worldManagerCreateWorldInvalidWorldname,
        
        worldManagerCreateWorldIntro,
        worldManagerCreateWorldAskWorldname,
        worldManagerCreateWorldAskNether,
        worldManagerCreateWorldAskNetherDecline,
        worldManagerCreateWorldAskEnd,
        worldManagerCreateWorldAskEndDecline,
        worldManagerCreateWorldAskPVP,
        worldManagerCreateWorldAskGameMode,
        worldManagerCreateWorldAskDifficulty,
        worldManagerCreateWorldAskHostileMobs,
        worldManagerCreateWorldAskNeutralMobs,
        worldManagerCreateWorldAskBackpacks,
        worldManagerCreateWorldAskEnderchests,
        worldManagerCreateWorldAskWorldType,
        worldManagerCreateWorldAskStructures,
        worldManagerCreateWorldAskSeed,
        
        worldManagerCreateSettingUpWorld,
        worldManagerCreateWorldCreated,
        worldManagerCreateWorldFailedOrCancelled,
        
        worldManagerRemoveWorldTargetWorldNull,
        worldManagerRemoveWorldTargetWorldDefault,
        worldManagerRemoveWorldTargetWorldMojangWorld,
        worldManagerRemoveWorldAttempting,
        worldManagerRemoveWorldSuccess,
        worldManagerRemoveWorldFailure,
    }
}
