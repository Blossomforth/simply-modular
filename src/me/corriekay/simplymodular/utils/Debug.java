package me.corriekay.simplymodular.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import me.corriekay.simplymodular.SimplyModular;

public abstract class Debug {
    public static Logger log = SimplyModular.getSM().getLogger();
    public static Options options = SimplyModular.getSM().getOptions();
    
    public static void logVerbose(String s, boolean boot) {
        if (boot) {
            if (!options.isVerboseBootup()) { return; }
        } else {
            if (!options.isVerboseShutdown()) { return; }
        }
        displayLog(s, Level.INFO);
    }
    
    public static void logVerbose(String s, boolean boot, Level l) {
        if (boot) {
            if (!options.isVerboseBootup()) { return; }
        } else {
            if (!options.isVerboseShutdown()) { return; }
        }
        displayLog(s, l);
    }
    
    public static void logHighlyVerbose(String s, boolean boot) {
        if (boot) {
            if (!options.isVerboseBootup()) { return; }
            if (!options.isHighlyVerboseBootup()) { return; }
        } else {
            if (!options.isVerboseShutdown()) { return; }
            if (!options.isHighlyVerboseShutdown()) { return; }
        }
        displayLog(s, Level.INFO);
    }
    
    public static void logHighlyVerbose(String s, boolean boot, Level l) {
        if (boot) {
            if (!options.isVerboseBootup()) { return; }
            if (!options.isHighlyVerboseBootup()) { return; }
        } else {
            if (!options.isVerboseShutdown()) { return; }
            if (!options.isHighlyVerboseShutdown()) { return; }
        }
        displayLog(s, l);
    }
    
    public static void log(String s) {
        displayLog(s, Level.INFO);
    }
    
    public static void log(String s, Level l) {
        displayLog(s, l);
    }
    
    public static void logDebug(String s) {
        if (!options.isGlobalDebug()) { return; }
        // TODO filter for module
        displayLog("[debug] " + s, Level.INFO);
    }
    
    public static void logDebug(String s, Level l) {
        if (!options.isGlobalDebug()) { return; }
        displayLog("[debug] " + s, l);
    }
    
    private static void displayLog(String s, Level l) {
        if (!options.isGlobalDebug()) { return; }
        log.log(l, s);
    }
}
