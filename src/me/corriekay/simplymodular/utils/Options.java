/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.utils;

import me.corriekay.simplymodular.SimplyModular;

import org.bukkit.configuration.file.FileConfiguration;

public class Options {
    
    private final FileConfiguration config;
    
    private boolean disableOnFatalError, verboseBootup, highlyVerboseBootup,
            verboseShutdown, highlyVerboseShutdown, globalDebug,
            exceptionLogging, exceptionCleanupOnBoot;
    
    public Options() {
        config = SimplyModular.getSM().getConfig();
        setDisableOnFatalError(config.getBoolean("options.onEnable.disableOnFatalError"));
        setVerboseBootup(config.getBoolean("options.onEnable.verboseBootup"));
        setHighlyVerboseBootup(config.getBoolean("options.onEnable.highlyVerbose"));
        setVerboseShutdown(config.getBoolean("options.onDisable.verboseShutdown"));
        setHighlyVerboseShutdown(config.getBoolean("options.onDisable.highlyVerbose"));
        setGlobalDebug(config.getBoolean("options.debug.globalDebug"));
        setExceptionLogging(config.getBoolean("options.debug.exceptionLogging"));
        setExceptionCleanupOnBoot(config.getBoolean("options.debug.cleanupOnBoot"));
    }
    
    public FileConfiguration getOptionsConfig() {
        return config;
    }
    
    public boolean isDisableOnFatalError() {
        return disableOnFatalError;
    }
    
    public void setDisableOnFatalError(boolean disableOnFatalError) {
        this.disableOnFatalError = disableOnFatalError;
    }
    
    public boolean isVerboseBootup() {
        return verboseBootup;
    }
    
    public void setVerboseBootup(boolean verboseBootup) {
        this.verboseBootup = verboseBootup;
    }
    
    public boolean isHighlyVerboseBootup() {
        return highlyVerboseBootup;
    }
    
    public void setHighlyVerboseBootup(boolean highlyVerboseBootup) {
        this.highlyVerboseBootup = highlyVerboseBootup;
    }
    
    public boolean isVerboseShutdown() {
        return verboseShutdown;
    }
    
    public void setVerboseShutdown(boolean verboseShutdown) {
        this.verboseShutdown = verboseShutdown;
    }
    
    public boolean isHighlyVerboseShutdown() {
        return highlyVerboseShutdown;
    }
    
    public void setHighlyVerboseShutdown(boolean highlyVerboseShutdown) {
        this.highlyVerboseShutdown = highlyVerboseShutdown;
    }
    
    public boolean isExceptionLogging() {
        return exceptionLogging;
    }
    
    public void setExceptionLogging(boolean exceptionLogging) {
        this.exceptionLogging = exceptionLogging;
    }
    
    public boolean isExceptionCleanupOnBoot() {
        return exceptionCleanupOnBoot;
    }
    
    public void setExceptionCleanupOnBoot(boolean exceptionCleanupOnBoot) {
        this.exceptionCleanupOnBoot = exceptionCleanupOnBoot;
    }
    
    public boolean isGlobalDebug() {
        return globalDebug;
    }
    
    public void setGlobalDebug(boolean globalDebug) {
        this.globalDebug = globalDebug;
    }
}
