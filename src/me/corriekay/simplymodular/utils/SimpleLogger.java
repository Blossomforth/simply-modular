/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.utils;

import java.io.*;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.events.*;

import org.bukkit.ChatColor;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class SimpleLogger extends SimpleListener {
    private final File logDirectory;
    
    public SimpleLogger() {
        if (SimplyModular.getSM().getDataFolder() == null) {
            System.out.println("Data folder null!");
        }
        logDirectory = new File(SimplyModular.getSM().getDataFolder() + File.separator + "Logs");
        if (!logDirectory.exists()) {
            logDirectory.mkdirs();
        }
    }
    
    @SimpleEvent
    public void onChat(ChatChannelMessageEvent event) throws Exception {
        File dir = new File(logDirectory.getPath() + File.separator + "ChatLogs" + File.separator + Utils.getFileDate());
        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
        File logfile = new File(dir, event.getChannel().getName() + ".txt");
        boolean newfile = !logfile.exists();
        if (newfile) {
            logfile.createNewFile();
        }
        BufferedWriter out = new BufferedWriter(new FileWriter(logfile, true));
        if (newfile) {
            out.write("##Chat log file for channel \"" + event.getChannel().getName() + "\" for the date of " + Utils.getFileDate() + ".\n##Format: [Timestamp] [name | nickname]: [Message]");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[" + Utils.getTimeStamp() + "]");
        sb.append(" [" + (event.getWhoSent() == null ? "Server | Server" : "user | user") + "]"); //TODO get User nick/name
        sb.append(": " + ChatColor.stripColor(event.getMessage()));
        out.write("\n" + sb.toString());
        out.close();
        
        //Allchat
        logfile = new File(dir, "allchat.txt");
        newfile = !logfile.exists();
        if (newfile) {
            logfile.createNewFile();
        }
        out = new BufferedWriter(new FileWriter(logfile, true));
        if (newfile) {
            out.write("##Chat log for all channels for the date of " + Utils.getFileDate() + ".\n##Format: [Timestamp] [Channel Name] [name | nickname]: [Message]");
        }
        sb = new StringBuilder();
        sb.append("[" + Utils.getTimeStamp() + "]");
        sb.append(" [" + event.getChannel().getName() + "]");
        sb.append(" [" + (event.getWhoSent() == null ? "Server | Server" : "user | user") + "]");
        sb.append(": " + ChatColor.stripColor(event.getMessage()));
        out.write("\n" + sb.toString());
        out.close();
    }
    
    private void logCommand(String who, String command) throws Exception {
        File dir = new File(logDirectory.getPath() + File.separator + "ChatLogs" + File.separator + Utils.getFileDate());
        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
        File logfile = new File(dir, "commandlogs.txt");
        boolean newfile = !logfile.exists();
        if (newfile) {
            logfile.createNewFile();
        }
        BufferedWriter out = new BufferedWriter(new FileWriter(logfile, true));
        if (newfile) {
            out.write("##Command logger for the date of " + Utils.getFileDate() + ".");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[" + Utils.getTimeStamp() + "]");
        sb.append(" " + who + ": " + command);
        out.write("\n" + sb.toString());
        out.close();
    }
    
    @SimpleEvent
    public void onCommand(PlayerCommandPreprocessEvent event) throws Exception {
        logCommand(event.getPlayer().getName(), event.getMessage());
    }
    
    @SimpleEvent
    public void onConsoleCommand(ServerCommandEvent event) throws Exception {
        logCommand("Server", "/" + event.getCommand());
    }
}
