/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.events;

import org.bukkit.event.Event;
import org.bukkit.event.player.*;

/**
 * @author Corrie
 * Class not used at this point.
 */

/*
 * CLASS NOT IN USE. Likely will create something later to create a
 * player event callback system, but for now, will remain unused.
 */
public class PlayerEventListenerManager extends SimpleListener {
    
    static enum PlayerEvents {
        AsyncChat(AsyncPlayerChatEvent.class),
        Animation(PlayerAnimationEvent.class),
        BedEnter(PlayerBedEnterEvent.class),
        BedLeave(PlayerBedLeaveEvent.class),
        Bucket(PlayerBucketEvent.class),
        ChangedWorld(PlayerChangedWorldEvent.class),
        Channel(PlayerChannelEvent.class),
        ChatTabComplete(PlayerChatTabCompleteEvent.class),
        CommandPreprocess(PlayerCommandPreprocessEvent.class),
        DropItem(PlayerDropItemEvent.class),
        EditBook(PlayerEditBookEvent.class),
        EggThrow(PlayerEggThrowEvent.class),
        ExpChange(PlayerExpChangeEvent.class),
        Fish(PlayerFishEvent.class),
        GameModeChange(PlayerGameModeChangeEvent.class),
        InteractEntity(PlayerInteractEntityEvent.class),
        Interact(PlayerInteractEvent.class),
        ItemBreak(PlayerItemBreakEvent.class),
        ItemConsume(PlayerItemConsumeEvent.class),
        ItemHeld(PlayerItemHeldEvent.class),
        Join(PlayerJoinEvent.class),
        Kick(PlayerKickEvent.class),
        LevelChange(PlayerLevelChangeEvent.class),
        Login(PlayerLoginEvent.class),
        Move(PlayerMoveEvent.class),
        PickupItem(PlayerPickupItemEvent.class),
        Quit(PlayerQuitEvent.class),
        Respawn(PlayerRespawnEvent.class),
        ShearEntity(PlayerShearEntityEvent.class),
        ToggleFlight(PlayerToggleFlightEvent.class),
        ToggleSneak(PlayerToggleSneakEvent.class),
        ToggleSprint(PlayerToggleSprintEvent.class),
        Velocity(PlayerVelocityEvent.class);
        private final Class<? extends Event> clazz;
        
        PlayerEvents(Class<? extends Event> clazz) {
            this.clazz = clazz;
        }
        
        public Class<? extends Event> get() {
            return clazz;
        }
    }
}
