/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.events;

import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.usermanager.User;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupChangeEvent extends Event {
    
    private final User user;
    private final Group oldGroup, newGroup;
    private final String playername;
    public static final HandlerList handlers = new HandlerList();
    
    public GroupChangeEvent(User user, Group oldGroup, Group newGroup, String playername) {
        this.user = user;
        this.oldGroup = oldGroup;
        this.newGroup = newGroup;
        this.playername = playername;
    }
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public User getUser() {
        return user;
    }
    
    public Group getOldGroup() {
        return oldGroup;
    }
    
    public Group getNewGroup() {
        return newGroup;
    }
    
    public String getPlayername() {
        return playername;
    }
    
}
