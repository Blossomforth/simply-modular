/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.events;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.logging.Level;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.utils.Debug;

import org.bukkit.Bukkit;
import org.bukkit.event.*;
import org.bukkit.plugin.EventExecutor;

public abstract class SimpleListener implements SMListener, EventExecutor {
    protected final HashMap<Class<? extends Event>, Method> methodMap = new HashMap<Class<? extends Event>, Method>();
    private boolean eventsRegistered = false;
    
    public SimpleListener() {
        registerEvents();
    }
    
    public SimpleListener(boolean register) {
        if (register) {
            registerEvents();
        }
    }
    
    @SuppressWarnings("unchecked")
    public void registerEvents() {
        if (!eventsRegistered) {
            eventsRegistered = true;
            for (Method method : getClass().getMethods()) {
                method.setAccessible(true);
                Annotation a = method.getAnnotation(SimpleEvent.class);
                if (a != null) {
                    Class<?>[] params = method.getParameterTypes();
                    if (params.length == 1) {
                        SimpleEvent sa = (SimpleEvent) a;
                        registerEvent((Class<? extends Event>) params[0], sa.priority(), sa.eventTypes(), method);
                    }
                }
            }
        }
    }
    
    public void registerEvent(Class<? extends Event> event, EventPriority priority, Class<? extends Event>[] eventTypes, Method method) {
        try {
            Bukkit.getPluginManager().registerEvent(event, this, priority, this, SimplyModular.getSM());
            methodMap.put(event, method);
            if (eventTypes != null) {
                for (Class<? extends Event> eventType : eventTypes) {
                    methodMap.put(eventType, method);
                }
            }
        } catch (NullPointerException e) {
            Debug.logDebug("Illegal Event registration!");
            Debug.logHighlyVerbose("Null pointer exception thrown while trying to register an event. Event type: " + event.getCanonicalName() + ".", true);
        } catch (IllegalArgumentException e) {
            Debug.logDebug("Illegal Plugin Access Exception!");
            Debug.logDebug(e.getMessage());
            Debug.logHighlyVerbose("Tried to register illegal event: " + event.getCanonicalName(), true);
        }
    }
    
    public void execute(Listener listener, Event event) throws EventException {
        Method method = methodMap.get(event.getClass());
        if (method == null) { return; }
        try {
            method.invoke(this, event);
        } catch (Exception e) {
            Debug.logDebug("Exception caught in event!", Level.SEVERE);
            e.printStackTrace();
            if (e instanceof InvocationTargetException) {
                InvocationTargetException ite = (InvocationTargetException) e;
                e.setStackTrace(ite.getCause().getStackTrace());
            }
            // String eventName = event.getClass().getCanonicalName();
            // TODO LOG EXCEPTION
        }
    }
    
    public void unregister() {
        if (eventsRegistered) {
            HandlerList.unregisterAll(this);
            eventsRegistered = false;
        }
    }
}
