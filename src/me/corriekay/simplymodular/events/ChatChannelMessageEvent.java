/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.events;

import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChannel;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ChatChannelMessageEvent extends Event {
    public static final HandlerList handlers = new HandlerList();
    
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    private final User user;
    private final String message;
    private final SimpleChannel channel;
    private boolean log;
    
    public ChatChannelMessageEvent(User who, String message, SimpleChannel channel, boolean log) {
        user = who;
        this.message = message;
        this.channel = channel;
        this.log = log;
    }
    
    public User getWhoSent() {
        return user;
    }
    
    public String getMessage() {
        return message;
    }
    
    public SimpleChannel getChannel() {
        return channel;
    }
    
    public boolean log() {
        return log;
    }
    
    public void setLog(boolean flag) {
        log = flag;
    }
}
