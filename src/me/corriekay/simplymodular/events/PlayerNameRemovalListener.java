/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.events;

import java.util.*;

import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;

@SuppressWarnings("rawtypes")
public class PlayerNameRemovalListener extends SimpleListener {
    
    private HashSet<Map> keys = new HashSet<Map>(),
            values = new HashSet<Map>();
    private HashSet<Set> sets = new HashSet<Set>();
    
    public void registerMapKey(Map map) {
        keys.add(map);
    }
    
    public void registerMapValue(Map map) {
        values.add(map);
    }
    
    public void unregisterKeymap(Map map) {
        keys.remove(map);
    }
    
    public void unregisterValuemap(Map map) {
        values.remove(map);
    }
    
    public void registerSet(Set set) {
        sets.add(set);
    }
    
    public void unregisterSet(Set set) {
        sets.remove(set);
    }
    
    @SimpleEvent(priority = EventPriority.MONITOR)
    public void quit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        for (Set set : sets) {
            set.remove(player.getName());
        }
        for (Map map : keys) {
            map.remove(player.getName());
        }
        for (Map map : values) {
            HashSet<Object> removeme = new HashSet<Object>();
            for (Object o : map.keySet()) {
                Object o2 = map.get(o);
                if (o2.toString().equals(player.getName())) {
                    removeme.add(o2);
                }
            }
            for (Object o : removeme) {
                map.remove(o);
            }
        }
    }
}
