/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command;

import java.lang.reflect.Field;
import java.util.*;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.utils.Debug;
import me.corriekay.simplymodular.utils.SimplyModularFatalException;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.craftbukkit.v1_7_R3.CraftServer;
import org.bukkit.event.Listener;

public class ASimpleCommandMap implements Listener {
    private final CommandMap map;
    private final Map<String, Command> knownCommands;
    private final HashMap<String, SimpleCommand> commands = new HashMap<String, SimpleCommand>();
    
    @SuppressWarnings("unchecked")
    public ASimpleCommandMap() throws Exception {
        Debug.logHighlyVerbose("Initializing SimpleCommandMap", true);
        if (Bukkit.getServer() instanceof CraftServer) {
            Debug.logHighlyVerbose("is Craft Server", true);
            Debug.logHighlyVerbose("Hijacking map field", true);
            final Field mapField = CraftServer.class.getDeclaredField("commandMap");
            mapField.setAccessible(true);
            map = (CommandMap) mapField.get(Bukkit.getServer());
            
            Debug.logHighlyVerbose("hijacking known commands", true);
            final Field commandField = SimpleCommandMap.class.getDeclaredField("knownCommands");
            commandField.setAccessible(true);
            knownCommands = (Map<String, Command>) commandField.get(map);
        } else {
            Debug.logVerbose("Command map cannot found!", true);
            throw new NullPointerException("Command map cannot be found!");
        }
        Debug.logHighlyVerbose("Command map successfully hijacked!", true);
        Bukkit.getPluginManager().registerEvents(this, SimplyModular.getSM());
    }
    
    public boolean registerCommand(SimpleCommand command) {
        if (command != null) {
            SimpleCommandMetadata meta = command.meta;
            if (meta != null) {
                ArrayList<String> aliases = meta.getLabels();
                if (aliases == null) {
                    aliases = new ArrayList<String>();
                }
                String perm = meta.getPermission();
                if (perm != null) {
                    command.setPermission(perm);
                }
                commands.put(command.getName(), command);
                return map.register("SimplyModular:", command);
            }
            Debug.logDebug("Command enumerator for command string '" + command.getName() + "' not found!");
            return false;
        }
        Debug.logDebug("Command was null!");
        return false;
    }
    
    public Command getCmd(String alias) {
        return knownCommands.get(alias);
    }
    
    public boolean checkAliasAvailable(String alias) {
        return knownCommands.get(alias) == null;
    }
    
    public boolean setAlias(String cmd, String alias) throws SimplyModularFatalException {
        SimpleCommand c = commands.get(cmd);
        if (c == null) { throw new SimplyModularFatalException("Attempted to set command alias to null command. Command cannot be null! Command string that was found null: " + cmd + ", Alias trying to set: " + alias + ".", new NullPointerException()); }
        knownCommands.put(alias, c);
        return true;
    }
    
    public boolean setAlias(Command cmd, String alias) {
        knownCommands.put(alias, cmd);
        return true;
    }
    
    public void removeAlias(String alias) {
        knownCommands.remove(alias);
    }
}
