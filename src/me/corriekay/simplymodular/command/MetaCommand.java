/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command;

import java.util.HashMap;
import java.util.logging.Level;

import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.validation.arguments.*;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.TabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public abstract class MetaCommand extends SimpleCommand implements
        ISimpleCommand {
    
    private final HashMap<String, MetaSubCommand> subcommands = new HashMap<String, MetaSubCommand>();
    
    public MetaCommand(SimpleModule module, SimpleCommandMetadata meta) {
        super(module, meta);
    }
    
    public void addSubcommand(MetaSubCommand command) {
        subcommands.put(command.subcommand, command);
        this.getMeta().getArgRuleset().addRule(new SimpleArgumentRule(null, false, true) {
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                if (subcommands.containsKey(args[0])) { return true; }
                StringContainer.sendMessage(sender, StringType.metaCommandIncorrectArgs, Utils.joinStringSet(subcommands.keySet(), ", "));
                return false;
            }
        });
    }
    
    @Override
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws Exception {
        if (!validateMeta(sender, args[0], getArgs(args))) { return true; }
        subcommands.get(args[0]).optionalCommand(sender, args[0], getArgs(args));
        return true;
    }
    
    @Override
    public boolean playerCommand(Player sender, String label, String[] args) throws Exception {
        if (!validateMeta(sender, args[0], getArgs(args))) { return true; }
        subcommands.get(args[0]).playerCommand(sender, args[0], getArgs(args));
        return true;
    }
    
    @Override
    public boolean consoleCommand(ConsoleCommandSender sender, String label, String[] args) throws Exception {
        if (!validateMeta(sender, args[0], getArgs(args))) { return true; }
        subcommands.get(args[0]).consoleCommand(sender, args[0], getArgs(args));
        return true;
    }
    
    private boolean validateMeta(CommandSender sender, String label, String[] args) {
        CommandMeta meta = subcommands.get(label).meta;
        if (meta.permission != null && !sender.hasPermission(meta.permission)) {
            StringContainer.sendMessage(sender, StringType.denyNoPermission);
            return false;
        }
        if (args.length < meta.minimumArgs) {
            StringContainer.sendMessage(sender, StringType.denyMinArgs);
            sender.sendMessage(meta.usage);
            return false;
        }
        SimpleArgumentRuleset ruleset = subcommands.get(label).ruleset;
        if (ruleset != null) {
            SimpleArgumentRule valid;
            try {
                valid = ruleset.validateArguments(sender, label, args);
            } catch (SimplyModularFatalException e) {
                Debug.logDebug("Excepti on caught validating arguments. TODO: log this exception", Level.SEVERE);//TODO
                return false;
            }
            if (valid != null) { return false; }
        }
        return true;
    }
    
    private String[] getArgs(String[] args) {
        String[] newArgs = new String[args.length - 1];
        for (int i = 1; i < args.length; i++) {
            newArgs[i - 1] = args[i];
        }
        return newArgs;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
    public static abstract class MetaSubCommand implements ISimpleCommand {
        
        private ArgumentRuleset ruleset = new ArgumentRuleset();
        private TabRuleset tabRules = new TabRuleset();
        
        public final CommandMeta meta;
        
        private final String subcommand;
        protected final MetaCommand supercommand;
        
        public MetaSubCommand(String name, CommandMeta meta, MetaCommand supercommand) {
            subcommand = name;
            this.meta = meta;
            this.supercommand = supercommand;
            setTabComplete(tabRules);
            setArgValidator(ruleset);
        }
        
        @Override
        public boolean optionalCommand(CommandSender sender, String label, String[] args) throws Exception {
            return false;
        }
        
        @Override
        public boolean playerCommand(Player sender, String label, String[] args) throws Exception {
            return false;
        }
        
        @Override
        public boolean consoleCommand(ConsoleCommandSender sender, String label, String[] args) throws Exception {
            return false;
        }
        
        protected abstract void setTabComplete(SimpleTabRuleset ruleset);
        
        protected abstract void setArgValidator(SimpleArgumentRuleset argRuleset);
    }
    
    public static class CommandMeta {
        String permission, description, usage;
        CommandType type;
        int minimumArgs;
        
        /**
         * @param permission
         * @param desc
         * @param use
         * @param type
         * @param minArgs
         */
        public CommandMeta(String permission, String desc, String use, CommandType type, int minArgs) {
            this.permission = permission;
            description = desc;
            usage = use;
            this.type = type;
            minimumArgs = minArgs;
        }
    }
    
}
