/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

public abstract class SimpleCommand extends Command implements ISimpleCommand {
    protected final SimpleCommandMetadata meta;
    protected final SimpleModule module;
    
    public SimpleCommandMetadata getMeta() {
        return meta;
    }
    
    public SimpleCommand(SimpleModule module, SimpleCommandMetadata meta) {
        super(meta.getCommandName(), meta.getDescription(), meta.getUsage(), meta.getLabels());
        this.meta = meta;
        this.module = module;
        this.setArgValidator(meta.getArgRuleset());
        this.setTabComplete(meta.getTabRules());
    }
    
    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        Debug.logHighlyVerbose("Command Executed. Metadata: " + meta.name(), false);
        if (meta.getPermission() != null && !sender.hasPermission(meta.getPermission())) {
            StringContainer.sendMessage(sender, StringType.denyNoPermission);
            StringBuilder builder = new StringBuilder();
            builder.append(Utils.getDatedTimestamp() + "Logged command exectuted by user " + sender.getName() + ": /" + label + " " + Utils.joinStringArray(args, " ") + " (" + meta.getCommandName() + "). Command permission check failed");
            // TODO LOG COMMAND
            return true;
        }
        if (args.length < meta.getMinimumArgs()) {
            StringContainer.sendMessage(sender, StringType.denyMinArgs);
            sender.sendMessage(meta.getUsage());
            return false;
        }
        SimpleArgumentRuleset sar = meta.getArgRuleset();
        if (sar != null) {
            SimpleArgumentRule valid;
            try {
                valid = sar.validateArguments(sender, label, args);
            } catch (SimplyModularFatalException e) {
                Debug.logDebug("Excepti on caught validating arguments. TODO: log this exception", Level.SEVERE);//TODO
                return false;
            }
            if (valid != null) { return valid.returnOnFail; }
        }
        boolean returned = onCommand(sender, label, args);
        if (meta.shouldLog()) {
            StringBuilder builder = new StringBuilder();
            builder.append(Utils.getDatedTimestamp() + " Logged command executed by user: " + sender.getName() + ": /" + label + " " + Utils.joinStringArray(args, " ") + " (" + meta.getCommandName() + "). Command returned " + returned + "\n");
            // TODO LOG
        }
        if (!returned) {
            sender.sendMessage(getUsage());
        }
        return returned;
    }
    
    public final boolean onCommand(CommandSender sender, String label, String[] args) {
        try {
            switch (meta.getCommandType()) {
                case DIFFERENTIATE:
                    if (sender instanceof Player) {
                        return playerCommand((Player) sender, label, args);
                    } else if (sender instanceof ConsoleCommandSender) {
                        return consoleCommand((ConsoleCommandSender) sender, label, args);
                    } else {
                        // TODO implement remote administrative client
                        return false;
                    }
                case REQUIRES_CONSOLE:
                    if (!(sender instanceof ConsoleCommandSender)) {
                        StringContainer.sendMessage(sender, StringType.notConsole);
                        return true;
                    } else {
                        return consoleCommand((ConsoleCommandSender) sender, label, args);
                    }
                case REQUIRES_PLAYER:
                    if (!(sender instanceof Player)) {
                        StringContainer.sendMessage(sender, StringType.notPlayer);
                        return true;
                    } else {
                        return playerCommand((Player) sender, label, args);
                    }
                case OPTIONAL:
                    return optionalCommand(sender, label, args);
                default:
                    return false;
            }
        } catch (Exception e) {
            // String message = "Exception thrown on command: " +
            // cmd.getCommandName() + "\nLabel: " + label;
            // message += "\nArgs: ";
            // for (String arg : args) {
            // message += arg + " ";
            // }
            // TODO log command exception
            // TODO send error messages to player and console.
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws Exception {
        throwUnhandledCommandException(meta);
        return true;
    }
    
    @Override
    public boolean playerCommand(Player sender, String label, String[] args) throws Exception {
        throwUnhandledCommandException(meta);
        return true;
    }
    
    @Override
    public boolean consoleCommand(ConsoleCommandSender sender, String label, String[] args) throws Exception {
        throwUnhandledCommandException(meta);
        return true;
    }
    
    public void throwUnhandledCommandException(SimpleCommandMetadata cmd) throws UnhandledCommandException {
        throw new UnhandledCommandException("Command not handled: " + cmd.getCommandName());
    }
    
    protected User getUser(Player player) throws SimplyModularFatalModuleException {
        return UserManager.getManager().getUser(player);
    }
    
    protected User getUser(String name) throws SimplyModularFatalModuleException {
        return UserManager.getManager().getUser(name);
    }
    
    public Group getGroupByPlayer(Player player) throws SimplyModularFatalModuleException {
        return SimplePermissions.getPermissionsManager().getGroupByPlayer(player);
    }
    
    public Group getGroupByName(String name) throws SimplyModularFatalModuleException {
        return SimplePermissions.getPermissionsManager().getGroupByName(name);
    }
    
    public Group getGroupByUser(User user) throws SimplyModularFatalModuleException {
        return SimplePermissions.getPermissionsManager().getGroupByUser(user);
    }
    
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
        try {
            List<String> strings = new ArrayList<String>();
            for (String s : meta.getTabRules().validateTab(sender, meta, alias, args[args.length - 1], args)) {
                if (s.contains(args[args.length - 1])) {
                    strings.add(s);
                }
            }
            return strings;
        } catch (SimplyModularFatalModuleException e) {
            e.printStackTrace();//TODO LOG ME
            return new ArrayList<String>();
        }
    }
    
    public Player getOnlinePlayer(String pName, CommandSender requester) {
        return getOnlinePlayer(pName, requester, false);
    }
    
    public Player getOnlinePlayer(String pName, CommandSender requester, boolean canNull) {
        return UserManager.getManager().getOnlinePlayer(pName, requester, canNull);
    }
    
    public String getOfflinePlayer(String pName, CommandSender requester, boolean feedback) {
        return UserManager.getManager().getOfflinePlayer(pName, requester, feedback);
    }
    
    public OfflinePlayer getOnlineOfflinePlayer(String pName, CommandSender requester, boolean feedback) {
        return UserManager.getManager().getOnlineOfflinePlayer(pName, requester, feedback);
    }
    
    protected abstract void setTabComplete(SimpleTabRuleset ruleset);
    
    protected abstract void setArgValidator(SimpleArgumentRuleset argRuleset);
}
