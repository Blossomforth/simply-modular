/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.validation.arguments;

import java.util.HashSet;

import org.bukkit.command.CommandSender;

public class SetArgumentRule extends SimpleArgumentRule {
    
    private int index;
    private HashSet<String> valids;
    
    public SetArgumentRule(String failure, boolean showFailure, boolean returnOnFail, int indexToCheck, String... validArgs) {
        super(failure, showFailure, returnOnFail);
        index = indexToCheck;
        valids = new HashSet<String>();
        for (String arg : validArgs) {
            valids.add(arg);
        }
    }
    
    @Override
    public boolean checkRule(CommandSender sender, String label, String[] args) {
        try {
            String checkMe = args[index];
            return valids.contains(checkMe);
        } catch (Exception e) {
            return false;
        }
        
    }
    
}
