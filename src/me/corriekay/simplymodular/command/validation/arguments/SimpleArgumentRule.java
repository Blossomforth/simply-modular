/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.validation.arguments;

import me.corriekay.simplymodular.utils.SimplyModularFatalException;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.command.CommandSender;

public abstract class SimpleArgumentRule {
    private String failureMessage;
    public boolean showFailureMessage, returnOnFail;
    
    /**
     * creates a new rule
     * @param failure the message that should be sent to the command sender should the command return false
     * @param showFailure should the failure message show if validation fails?
     * @param returnOnFail what to return to the command should the validation fail
     */
    public SimpleArgumentRule(String failure, boolean showFailure, boolean returnOnFail) {
        failureMessage = failure;
        showFailureMessage = showFailure;
        this.returnOnFail = returnOnFail;
    }
    
    public abstract boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException;
    
    public String getFailureMessage(CommandSender sender, String[] args) throws SimplyModularFatalModuleException {
        return failureMessage;
    }
}
