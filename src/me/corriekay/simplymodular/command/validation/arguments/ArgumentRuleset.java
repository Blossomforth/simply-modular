/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.validation.arguments;

import java.util.ArrayList;

import me.corriekay.simplymodular.utils.SimplyModularFatalException;

import org.bukkit.command.CommandSender;

public class ArgumentRuleset implements SimpleArgumentRuleset {
    
    private final ArrayList<SimpleArgumentRule> rules = new ArrayList<SimpleArgumentRule>();
    
    public ArgumentRuleset() {}
    
    public ArgumentRuleset(SimpleArgumentRule... rules) {
        for (SimpleArgumentRule rule : rules) {
            addRule(rule);
        }
    }
    
    public void addRule(SimpleArgumentRule rule) {
        rules.add(rule);
    }
    
    @Override
    public SimpleArgumentRule validateArguments(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
        for (SimpleArgumentRule rule : rules) {
            if (!rule.checkRule(sender, label, args)) {
                if (rule.showFailureMessage) {
                    sender.sendMessage(rule.getFailureMessage(sender, args));
                }
                return rule;
            }
        }
        return null;
    }
}
