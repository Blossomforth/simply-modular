/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.validation.arguments;

import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;

public class OperatorInvalidTarget extends SimpleArgumentRule {
    
    private final int index;
    
    public OperatorInvalidTarget(String failure, boolean showFailure, boolean returnOnFail, int indexToCheck) {
        super(failure, showFailure, returnOnFail);
        index = indexToCheck;
    }
    
    @Override
    public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
        String op = SimplePermissions.getPermissionsManager().getOfflinePlayer(args[index], sender, false);
        Group g = SimplePermissions.getPermissionsManager().getGroupByUser(UserManager.getManager().getUser(op));
        if (g.isOpType()) {
            StringContainer.sendMessage(sender, StringType.cannotTargetOperatorWithThisCommand);
            return false;
        }
        return true;
    }
    
}
