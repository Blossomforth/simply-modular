/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.validation.arguments;

import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RequireOnlinePlayernameRule extends SimpleArgumentRule {
    
    private int index;
    boolean displayFailure;
    
    public RequireOnlinePlayernameRule(boolean showFailure, boolean returnOnFail, int indexToCheck) {
        super(null, false, returnOnFail);
        index = indexToCheck;
        displayFailure = showFailure;
    }
    
    private void sendNotFound(CommandSender sender, String fragment) {
        StringContainer.sendMessage(sender, StringType.playerNotFound, fragment);
    }
    
    @Override
    public boolean checkRule(CommandSender sender, String label, String[] args) {
        try {
            String s = args[index];
            for (Player player : Bukkit.getOnlinePlayers()) {
                String name = player.getName().toLowerCase();
                String display = ChatColor.stripColor(player.getDisplayName()).toLowerCase();
                if (name.contains(s) || display.contains(s)) { return true; }
            }
            if (displayFailure) sendNotFound(sender, s);
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    
}
