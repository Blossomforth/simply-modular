/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.validation.questioner;

import java.util.HashMap;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.events.SimpleListener;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerCommandEvent;

public abstract class Questioner extends SimpleListener {
    protected final String prompt;
    protected final CommandSender questionee;
    protected final HashMap<String, SimpleRunnable> answermap = new HashMap<String, SimpleRunnable>();
    
    public Questioner(String prompt, CommandSender questionee) throws Exception {
        super(false);
        this.prompt = prompt;
        this.questionee = questionee;
        registerEvent();
    }
    
    private final void registerEvent() throws Exception {
        if (questionee instanceof Player) {
            this.registerEvent(AsyncPlayerChatEvent.class, EventPriority.LOWEST, null, Questioner.class.getMethod("chatEvent", AsyncPlayerChatEvent.class));
            this.registerEvent(PlayerQuitEvent.class, EventPriority.NORMAL, null, Questioner.class.getMethod("quitEvent", PlayerQuitEvent.class));
        } else {
            this.registerEvent(ServerCommandEvent.class, EventPriority.LOWEST, null, Questioner.class.getMethod("chatEvent", ServerCommandEvent.class));
        }
    }
    
    public void ask() {
        StringBuilder sb = new StringBuilder();
        for (String s : answermap.keySet()) {
            sb.append(s + ", ");
        }
        questionee.sendMessage(prompt + "\n" + sb.substring(0, sb.length() - 2));
    }
    
    public Questioner addAnswer(String answer, SimpleRunnable todo) {
        answermap.put(answer, todo);
        return this;
    }
    
    @SimpleEvent
    public void chatEvent(AsyncPlayerChatEvent event) throws Exception {
        if (event.getPlayer() == questionee) {
            event.setCancelled(true);
        }
        answer(event.getMessage());
    }
    
    @SimpleEvent
    public void chatEvent(ServerCommandEvent event) throws Exception {
        event.setCommand(null);
        answer(event.getCommand());
    }
    
    private void answer(String message) throws Exception {
        SimpleRunnable r = answermap.get(message);
        if (r == null) {
            if (!answerNotFound(message)) {
                unregister();
            } else {
                ask();
            }
        }
        Bukkit.getScheduler().runTask(SimplyModular.getSM(), r);
        unregister();
    }
    
    /**
     * 
     * @param message message that player/console sent to questioner
     * @return false to stop questioning, true to re-prompt.
     */
    protected abstract boolean answerNotFound(String message);
    
    @SimpleEvent
    public void quitEvent(PlayerQuitEvent event) {
        unregister();
    }
    
    public void unregister() {
        HandlerList.unregisterAll(this);
    }
}
