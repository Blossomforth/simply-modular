package me.corriekay.simplymodular.command.validation.tabbing;

import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.command.CommandSender;

public interface SimpleTabRuleset {
    public List<String> validateTab(CommandSender sender, SimpleCommandMetadata cmd, String label, String arg, String[] args) throws SimplyModularFatalModuleException;
    
    public void addTabRule(int index, SimpleTabRule rule);
}
