package me.corriekay.simplymodular.command.validation.tabbing;

import java.util.*;

import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.command.CommandSender;

public class TabRuleset implements SimpleTabRuleset {
    
    private final HashMap<Integer, SimpleTabRule> rules = new HashMap<Integer, SimpleTabRule>();
    
    public List<String> validateTab(CommandSender sender, SimpleCommandMetadata cmd, String label, String arg, String[] args) throws SimplyModularFatalModuleException {
        if (cmd.getPermission() != null && !(sender.hasPermission(cmd.getPermission()))) { return new ArrayList<String>(); }
        SimpleTabRule rule = rules.get(args.length - 1);
        List<String> tabs = new ArrayList<String>();
        tabs.add(arg);
        if (rule != null) {
            tabs = rule.tab(sender, cmd, arg, args);
        }
        return tabs;
    }
    
    public void addTabRule(int index, SimpleTabRule rule) {
        rules.put(index, rule);
    }
    
}
