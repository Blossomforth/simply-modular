package me.corriekay.simplymodular.command.validation.tabbing;

import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.command.CommandSender;

public interface SimpleTabRule {
    
    public abstract List<String> tab(CommandSender sender, SimpleCommandMetadata cmd, String argBit, String[] args) throws SimplyModularFatalModuleException;
}
