package me.corriekay.simplymodular.command.validation.tabbing;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerTab implements SimpleTabRule {
    
    @SuppressWarnings("unused")
    public List<String> tab(CommandSender sender, SimpleCommandMetadata cmd, String argBit, String[] args) throws SimplyModularFatalModuleException {
        Player senderplayer = (Player) sender;
        User user = UserManager.getManager().getUser(senderplayer);
        List<String> returnMe = new ArrayList<String>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            User user2 = UserManager.getManager().getUser(player);
            //TODO check for invisibility if invis node is active
            //TODO add username to returnMe
        }
        return returnMe;
    }
}
