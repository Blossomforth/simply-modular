/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.validation.arguments.ArgumentRuleset;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.TabRuleset;

import org.bukkit.configuration.file.FileConfiguration;

public enum SimpleCommandMetadata {
    
    //Generic
    motd("motd", null, "displays the MOTD", "/motd", null, CommandType.OPTIONAL, 0, false),
    rules("rules", null, "Displays the rules", "/rules", null, CommandType.OPTIONAL, 0, false),
    nick("nick", "simplymodular.generic.nickname", "Allows a user to change their nickname", "/nick <nickname>", new String[] { "nickname" }, CommandType.REQUIRES_PLAYER, 1, false),
    
    //SimplePermissions
    list("list", "simplymodular.simplepermissions.list", "shows a list of players who are online", "/list", new String[] { "who", "online", "players" }, CommandType.OPTIONAL, 0, false),
    groupaddperm("groupaddperm", "simplymodular.simplepermissions.groupmanagement", "adds a permission to a group", "/groupaddperm <group> [world] <permission>", new String[] { "gaddperm" }, CommandType.OPTIONAL, 2, true),
    groupdelperm("groupdelperm", "simplymodular.simplepermissions.groupmanagement", "deletes a permission from a group", "/groupdelperm <group> [world] <permission>", new String[] { "gdelperm" }, CommandType.OPTIONAL, 2, true),
    useraddperm("useraddperm", "simplymodular.simplepermissions.groupmanagement", "adds a permission to a specific player", "/useraddperm <player> <permission>", new String[] { "uaddperm" }, CommandType.OPTIONAL, 2, true),
    userdelperm("userdelperm", "simplymodular.simplepermissions.groupmanagement", "removes a permission to a specific player", "/userdelperm <player> <permission>", new String[] { "udelperm" }, CommandType.OPTIONAL, 2, true),
    setgroup("setgroup", "simplymodular.simplepermissions.groupmanagement", "sets a players group", "/setgroup <target> <group>", new String[] { "groupset" }, CommandType.OPTIONAL, 2, true),
    reloadpermissions("reloadpermissions", "simplymodular.simplepermissions.reload", "reloads permissions from file", "/reloadpermissions", new String[] { "reloadperms" }, CommandType.OPTIONAL, 0, false),
    hasperm("hasperm", "simplymodular.simplepermissions.hasperm", "checks if a player has a permission", "/hasperm [player] <permission>", null, CommandType.OPTIONAL, 2, false),
    
    //SimpleChestProtect
    protect("protect", "simplymodular.simplechestprotect.protect", "Registers a chest as protected", "/protect <type> {players added to protection}", null, CommandType.REQUIRES_PLAYER, 0, false),
    unprotect("unprotect", "simplymodular.simplechestprotect.protect", "Unregisters a chest as protected", "/unprotect", null, CommandType.REQUIRES_PLAYER, 0, false),
    buildprotection("buildprotection", "simplymodular.simplechestprotect.protect", "Allows a player to build a more in depth block protection with a single command", "/buildprotection", null, CommandType.REQUIRES_PLAYER, 0, false),
    deleteplayerprotections("deleteplayerprotections", "simplymodular.simplechestprotect.deleteall", "Allows a (presumably) moderator or admin to wipe out all of a players protections", "/deleteplayerptotections <playername>", null, CommandType.OPTIONAL, 1, true),
    
    //SimpleInvisibility
    hide("hide", "simplymodular.simpleinvisibility.hide", "Hides you from the rest of the server", "/hide", null, CommandType.REQUIRES_PLAYER, 0, false),
    setinvislevel("setinvislevel", "simplymodular.simpleinvisibility.setinvislevel", "Manually sets the invisibility level of a target player", "/setinvislevel <player> <level>", null, CommandType.OPTIONAL, 1, true),
    
    //SimpleChatChannelManager
    channel("channel", "simplymodular.simplechatchannelmanager.channel", "utilized in two fashions, one for listing the channels on the server, and two as aliases for channel quick commands", "/channel OR /<channel name or quick> <message>", null, CommandType.OPTIONAL, 0, false),
    join("join", "simplymodular.simplechatchannelmanager.channel", "Joins a chat channel", "/join <channel> [password]", new String[] { "ch", "focus" }, CommandType.REQUIRES_PLAYER, 1, false),
    leave("leave", "simplymodular.simplechatchannelmanager.channel", "leaves a chat channel", "/leave <channel>", null, CommandType.REQUIRES_PLAYER, 0, false),
    me("me", "simplymodular.simplechatchannelmanager.emote", "sends an emaote to your current channel", "/me <emote>", new String[] { "me's" }, CommandType.DIFFERENTIATE, 1, false),
    mute("mute", "simplymodular.simplechatchannelmanager.mute", "toggles a players global mute flag", "/mute <playername>", null, CommandType.OPTIONAL, 1, true),
    pm("pm", "simplymodular.simplechatchannelmanager.pm", "sends a private message to another player", "/pm <target> <message>", new String[] { "msg", "tell", "whisper", "w" }, CommandType.REQUIRES_PLAYER, 2, false),
    r("r", "simplymodular.simplechatchannelmanager.pm", "replies to the last private message you've received", "/r <message", null, CommandType.REQUIRES_PLAYER, 1, false),
    ignore("ignore", "simplymodular.simplechatchannelmanager.ignore", "ignores a player, making it impossible to see their messages", "/ignore <target>", null, CommandType.REQUIRES_PLAYER, 1, false),
    ignored("ignored", "simplymodular.simplechatchannelmanager.ignore", "lists ignored players", "/ignored", null, CommandType.REQUIRES_PLAYER, 0, false),
    pmspy("pmspy", "simplymodular.simplechatchannelmanager.pmspy", "allows an operator to intercept private messages on the server", "/pmspy", null, CommandType.REQUIRES_PLAYER, 0, false),
    channelcolor("channelcolor", "simplymodular.simplechatchannelmanager.channelcolor", "allows a player to set their personal color for a chat channel", "/channelColor [channel] <color>", new String[] { "cc" }, CommandType.REQUIRES_PLAYER, 1, false),
    uc("uc", null, "Base command for creating and managing user-created channels", "/uc <command> [additional args]", null, CommandType.DIFFERENTIATE, 1, true),
    
    //SimpleWorldManager
    world("world", "simplymodular.simpleworldmanager.world", "debug command that teleports the user to a specific world. Should not be used normally", "/world <worldname>", null, CommandType.REQUIRES_PLAYER, 0, false),
    setworldspawn("setworldspawn", "simplymodular.simpleworldmanager.setworldspawn", "sets the spawn location for the specific world you're in.", "/setworldspawn", null, CommandType.REQUIRES_PLAYER, 0, true),
    createworld("createworld", "simplymodular.simpleworldmanager.worldmanagement", "allows a user to create a world, complete with setting up all of the settings. Starts a dialogue.", "/createworld <worldname>", null, CommandType.OPTIONAL, 0, true),
    removeworld("removeworld", "simplymodular.simpleworldmanager.worldmanagement", "allows the user to remove a world. If used to remove an overworld, any attached nether and end worlds will also be unloaded", "/removeworld <worldname>", null, CommandType.OPTIONAL, 1, true),
    
    ;
    
    private String commandName, permission, description, usage;
    private ArrayList<String> aliases;
    private CommandType type;
    private int minimumArgs;
    private boolean logCommand;
    private SimpleArgumentRuleset argRules = new ArgumentRuleset();
    private SimpleTabRuleset tabRules = new TabRuleset();
    
    private SimpleCommandMetadata(String name, String perm, String desc, String use, String[] labelArray, CommandType type, int minArgs, boolean log) {
        FileConfiguration c = SimplyModular.getSM().getConfig();
        String path = "commandOptions." + name + ".";
        commandName = c.getString(path + "name", name).toLowerCase();
        permission = c.getString(path + "permission", perm);
        description = c.getString(path + "desc", desc);
        usage = c.getString(path + "use", use);
        
        if (labelArray != null) {
            ArrayList<String> labels = new ArrayList<String>();
            for (String s : labelArray) {
                labels.add(s);
            }
            aliases = labels;
        } else {
            aliases = new ArrayList<String>();
        }
        {//Aliases
            List<String> aliases = c.getStringList(path + ".aliases");
            for (String alias : aliases) {
                if (alias.startsWith("-")) {
                    alias = alias.substring(1, alias.length());
                    this.aliases.remove(alias);
                } else {
                    this.aliases.add(alias);
                }
            }
        }
        this.type = type;
        minimumArgs = minArgs;
        logCommand = c.getBoolean(path + "logUsage", log);
    }
    
    public String getCommandName() {
        return commandName;
    }
    
    public String getPermission() {
        return permission;
    }
    
    public String getDescription() {
        return description;
    }
    
    public String getUsage() {
        return usage;
    }
    
    public ArrayList<String> getLabels() {
        return aliases;
    }
    
    public SimpleTabRuleset getTabRules() {
        return tabRules;
    }
    
    public SimpleArgumentRuleset getArgRuleset() {
        return argRules;
    }
    
    /**
     * Sets the labels. Warning: labels must be set before registering the
     * commands, otherwise this will do nothing.
     * 
     * @param labels
     */
    public void setLabels(ArrayList<String> labels) {
        aliases = labels;
    }
    
    public CommandType getCommandType() {
        return type;
    }
    
    public int getMinimumArgs() {
        return minimumArgs;
    }
    
    public boolean shouldLog() {
        return logCommand;
    }
    
    public static SimpleCommandMetadata getEnum(String arg) {
        arg = arg.toLowerCase();
        for (SimpleCommandMetadata cmd : SimpleCommandMetadata.values()) {
            String name = cmd.name().toLowerCase();
            if (name.equals(arg)) { return cmd; }
        }
        return null;
    }
    
    public enum CommandType {
        REQUIRES_PLAYER, REQUIRES_CONSOLE, DIFFERENTIATE, OPTIONAL;
    }
}
