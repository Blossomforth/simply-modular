/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChannel;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jnbt.CompoundTag;

public class ChannelColorCommand extends SimpleCommand {
    
    public ChannelColorCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.channelcolor);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        SimpleChannel channel;
        ChatColor color;
        String colorstring;
        if (args.length > 1) {
            channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
            colorstring = args[1];
        } else {
            channel = SimpleChatChannelManager.getModule().getChattingChannel(player);
            colorstring = args[0];
        }
        color = colorstring.equalsIgnoreCase("default") ? channel.getChannelColor() : ChatColor.valueOf(colorstring.toUpperCase());
        
        User user = getUser(player);
        CompoundTag tag = user.getCompoundTag(module);
        tag.getCompoundTag("channelColors").setString(channel.getName(), color.name());
        if (colorstring.equalsIgnoreCase("default")) {
            //Player is specifying default. This means that they want the channel to be defaulted. Previously, at any time after setting a channel, it would be impossible to go back to what the channel color is dynamically.
            tag.getCompoundTag("channelColors").removeEntry(channel.getName());
        }
        user.save();
        ((SimpleChatChannelManager) module).updateChannelColors(user, player.getName());
        
        StringContainer.sendMessage(player, StringType.chatManagerSetChannelColor, channel.getFormattedName(), color + "" + color.name());
        
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                SimpleChannel channel;
                if (args.length > 1) { //player is attempting to specify a channel
                    channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
                    if (channel == null || !channel.canJoin((Player) sender)) {
                        channel = null;
                    }
                } else {
                    channel = SimpleChatChannelManager.getModule().getChattingChannel(sender);
                }
                if (channel == null) {
                    StringContainer.sendMessage(sender, StringType.chatManagerChannelNullNotification);
                    return false;
                }
                return true;
            }
        });
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                try {
                    String colorstring;
                    if (args.length > 1) {
                        colorstring = args[1];
                    } else {
                        colorstring = args[0];
                    }
                    if (colorstring.equalsIgnoreCase("default")) { return true; }
                    ChatColor.valueOf(colorstring.toUpperCase());
                    return true;
                } catch (IllegalArgumentException e) {
                    StringContainer.sendMessage(sender, StringType.chatManagerInvalidColor);
                    return false;
                }
            }
            
        });
    }
    
}
