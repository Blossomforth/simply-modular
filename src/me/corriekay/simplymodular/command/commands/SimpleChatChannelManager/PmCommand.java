/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.ListenerCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.RequireOnlinePlayernameRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.PrivateMessageEvent;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;

public class PmCommand extends ListenerCommand {
    
    public PmCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.pm, true);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        Player target = getOnlinePlayer(args[0], player);
        PrivateMessageEvent pme = new PrivateMessageEvent(player, target, Utils.joinStringArray(args, " ", 1));
        Bukkit.getPluginManager().callEvent(pme);
        if (!pme.isCancelled()) {
            StringContainer.sendMessage(player, StringType.chatManagerSendPrivateMessage, target.getDisplayName(), pme.getMsg());
            StringContainer.sendMessage(target, StringType.chatManagerRecievePrivateMessage, player.getDisplayName(), pme.getMsg());
            SimpleChatChannelManager.getModule().updatePMs(player.getName(), target.getName());
        }
        return true;
    }
    
    @SimpleEvent(priority = EventPriority.LOW)
    public void pmEvent(PrivateMessageEvent event) {
        if (SimpleChatChannelManager.getModule().getData().getIgnores().get(event.getSender().getName()).contains(event.getReciever().getName())) {
            event.setCancelled(true);
            StringContainer.sendMessage(event.getSender(), StringType.chatManagerPmFailTargetIgnored);
        }
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new MutedValidator());
        argRuleset.addRule(new RequireOnlinePlayernameRule(true, true, 0));
        argRuleset.addRule(new IgnoredValidator(0));
    }
    
}
