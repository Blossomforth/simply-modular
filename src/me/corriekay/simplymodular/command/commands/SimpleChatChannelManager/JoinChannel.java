/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.questioner.Questioner;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.*;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jnbt.CompoundTag;

public class JoinChannel extends SimpleCommand {
    
    public JoinChannel(SimpleModule module) {
        super(module, SimpleCommandMetadata.join);
    }
    
    public boolean playerCommand(final Player player, String label, String[] args) throws Exception {
        final SimpleChannel channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
        final User user = getUser(player);
        if (channel.isPrivateChannel()) {//private channel banned/pw logic
            UserChannel uc = (UserChannel) channel;
            boolean isOp = SimplePermissions.getPermissionsManager().getGroupByPlayer(player).isOpType();
            if (uc.getBanned().contains(player.getName()) && !isOp) {
                StringContainer.sendMessage(player, StringType.chatManagerAttemptJoinDeny);
                return true;
            }
            System.out.println("Password is: " + uc.getPassword());
            if ((uc.getPassword() != null && uc.getPassword() != 0) && !(uc.getAdmin().equals(player.getName()) || uc.getMods().contains(player.getName()) || isOp)) {
                if (args.length > 1) {
                    Integer pwcheck = args[1].hashCode();
                    if (!uc.getPassword().equals(pwcheck)) {
                        StringContainer.sendMessage(player, StringType.chatManagerAttemptJoinNoPwOrPwInvalid);
                        return true;
                    }
                } else {
                    StringContainer.sendMessage(player, StringType.chatManagerAttemptJoinNoPwOrPwInvalid);
                    return true;
                }
            }
        }
        
        if (channel.isMatureFlagged()) {
            final CompoundTag tag = user.getCompoundTag(module);
            if (tag.getBoolean("matureBlocked")) {
                StringContainer.sendMessage(player, StringType.chatManagerAttemptJoinDeny);
                return true;
            }
            if (!tag.getBoolean("matureAccepted")) {
                Questioner questioner = new Questioner(StringContainer.getString(StringType.chatManagerMatureChannelWarning), player) {
                    @Override
                    protected boolean answerNotFound(String message) {
                        return true;
                    }
                };
                questioner.addAnswer("yes", new me.corriekay.simplymodular.command.validation.questioner.SimpleRunnable() {
                    public void run() {
                        tag.setBoolean("matureAccepted", true);
                        user.save();
                        StringContainer.sendMessage(player, StringType.chatManagerMatureChannelAccept);
                        try {
                            joinChannel(player, channel);
                        } catch (SimplyModularFatalModuleException e) {
                            
                        }
                    }
                });
                questioner.addAnswer("no", new me.corriekay.simplymodular.command.validation.questioner.SimpleRunnable() {
                    public void run() {
                        StringContainer.sendMessage(player, StringType.chatManagerMatureChannelDecline);
                    }
                });
                questioner.ask();
                return true;
            }
        }
        joinChannel(player, channel);
        return true;
    }
    
    private void joinChannel(Player player, SimpleChannel channel) throws SimplyModularFatalModuleException {
        if (channel.isChatting(player)) {
            StringContainer.sendMessage(player, StringType.chatManagerAlreadyChatting);
            return;
        }
        if (channel.canJoin(player)) {
            try {
                SimpleChatChannelManager.getModule().getChattingChannel(player).leaveChat(player, false, false);
            } catch (NullPointerException e) {}
            channel.joinChat(player, true);
            SimpleChatChannelManager.getModule().updateChannels(player);
        } else {
            StringContainer.sendMessage(player, StringType.chatManagerAttemptJoinDeny);
        }
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(StringContainer.getString(StringType.chatManagerChannelNullNotification), true, true) {
            public boolean checkRule(CommandSender sender, String label, String[] args) {
                SimpleChannel channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
                return (channel != null);
            }
        });
    }
    
}
