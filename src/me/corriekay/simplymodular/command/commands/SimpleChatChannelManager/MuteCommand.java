/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.RequireExistingPlayernameRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.ChannelDataManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jnbt.CompoundTag;

public class MuteCommand extends SimpleCommand {
    
    public MuteCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.mute);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
        OfflinePlayer op = UserManager.getManager().getOnlineOfflinePlayer(args[0], sender, false);
        User user = getUser(op.getName());
        CompoundTag tag = user.getCompoundTag(SimpleChatChannelManager.getModule());
        boolean currentFlag = tag.getBoolean("muted");
        currentFlag = !currentFlag;
        ChannelDataManager data = SimpleChatChannelManager.getModule().getData();
        data.setMuted(op.getName(), currentFlag);
        tag.setBoolean("muted", currentFlag);
        user.save();
        StringType targetmessage, opmessage;
        if (currentFlag) {
            targetmessage = StringType.chatManagerNotifyMuted;
            opmessage = StringType.chatManagerMutedPlayer;
        } else {
            targetmessage = StringType.chatManagerNotifyUnmuted;
            opmessage = StringType.chatManagerUnmutedPlayer;
        }
        if (op.isOnline()) {
            Player player = (Player) op;
            StringContainer.sendMessage(player, targetmessage);
        }
        StringContainer.sendMessage(sender, opmessage, op.isOnline() ? ((Player) op).getDisplayName() : op.getName());
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new RequireExistingPlayernameRule(true, true, 0));
    }
    
}
