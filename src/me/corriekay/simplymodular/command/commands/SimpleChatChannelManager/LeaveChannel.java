/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import java.util.ArrayList;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChannel;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveChannel extends SimpleCommand {
    
    public LeaveChannel(SimpleModule module) {
        super(module, SimpleCommandMetadata.leave);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        SimpleChannel channel;
        if (args.length > 0) {
            channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
        } else {
            channel = SimpleChatChannelManager.getModule().getChattingChannel(player);
        }
        boolean chatting = channel.isChatting(player);
        if (chatting) {
            channel.leaveChat(player, true, true);
            ArrayList<SimpleChannel> chans = SimpleChatChannelManager.getModule().getListeningChannels(player);
            if (chans.size() > 0) {
                SimpleChannel chan = chans.get(0);
                chan.joinChat(player, true);
            } else {
                StringContainer.sendMessage(player, StringType.chatManagerLeftAllChannels);
            }
        } else {
            channel.leaveListen(player, true);
        }
        SimpleChatChannelManager.getModule().updateChannels(player);
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(StringContainer.getString(StringType.chatManagerChannelNullNotification), true, false) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) {
                if (args.length > 0) {
                    SimpleChannel channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
                    if (channel == null) { return false; }
                }
                return true;
            }
        });
        argRuleset.addRule(new SimpleArgumentRule(null, false, false) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) {
                if (args.length == 0) {
                    SimpleChannel channel = SimpleChatChannelManager.getModule().getChattingChannel(((Player) sender));
                    if (channel == null) {
                        StringContainer.sendMessage(sender, StringType.chatManagerLeaveChannelNoChannel);
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    SimpleChannel channel = SimpleChatChannelManager.getModule().getChannel(args[0]);
                    if (!channel.isListening((Player) sender)) {
                        StringContainer.sendMessage(sender, StringType.chatManagerLeaveChannelNotInChannel);
                        return false;
                    } else {
                        return true;
                    }
                }
            }
            
        });
    }
    
}
