/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import java.util.ArrayList;

import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand.ChannelAdminCommandValidator;
import me.corriekay.simplymodular.command.validation.arguments.*;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.*;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ChannelKickCommand extends MetaSubCommand {
    
    public ChannelKickCommand(MetaCommand supercommand) {
        super("kick", new CommandMeta(null, "kicks a user from a channel", "/uc kick <player>", CommandType.REQUIRES_PLAYER, 1), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        UserChannel uc = (UserChannel) SimpleChatChannelManager.getModule().getChattingChannel(player);
        String op = supercommand.getOfflinePlayer(args[0], player, false);
        @SuppressWarnings("deprecation")
        Player player2 = Bukkit.getPlayer(op);
        
        if (!(uc.isListening(player2) || uc.isChatting(player2))) {
            StringContainer.sendMessage(player, StringType.channelManagerUserChannelKickNotInChannel);
            return true;
        }
        
        if (uc.getMods().contains(player.getName()) && uc.getAdmin().equals(op) || uc.getMods().contains(op)) {
            StringContainer.sendMessage(player, StringType.chatManagerUserChannelCannotBanAdmin);
            return true;
        }
        
        SimpleChatChannelManager module = SimpleChatChannelManager.getModule();
        uc.leaveChat(player2, true, true);
        ArrayList<SimpleChannel> chans = module.getListeningChannels(player2);
        if (chans.size() > 0) {
            chans.get(0).joinChat(player2, true);
        }
        StringContainer.sendMessage(player2, StringType.channelManagerUserChannelKickNotification, uc.getName());
        uc.messageStaff(StringType.channelManagerUserChannelKickStaffNotification, player2.getName());
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new ChannelAdminCommandValidator(true));
        argRuleset.addRule(new RequireOnlinePlayernameRule(true, true, 0));
        argRuleset.addRule(new OperatorInvalidTarget(null, false, true, 0));
    }
    
}
