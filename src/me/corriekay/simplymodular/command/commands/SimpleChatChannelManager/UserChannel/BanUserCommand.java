/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import java.util.ArrayList;

import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand.ChannelAdminCommandValidator;
import me.corriekay.simplymodular.command.validation.arguments.*;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.*;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jnbt.*;

public class BanUserCommand extends MetaSubCommand {
    
    public BanUserCommand(MetaCommand supercommand) {
        super("ban", new CommandMeta(null, "Bans a user from the channel", "/uc ban <player>", CommandType.REQUIRES_PLAYER, 1), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        UserChannel uc = (UserChannel) SimpleChatChannelManager.getModule().getChattingChannel(player);
        String op = supercommand.getOfflinePlayer(args[0], player, false);
        @SuppressWarnings("deprecation")
        Player player2 = Bukkit.getPlayer(op);
        if (uc.getBanned().contains(op)) {
            StringContainer.sendMessage(player, StringType.chatManagerUserChannelBanAlreadyBanned);
            return true;
        }
        if (uc.getAdmin().equals(op) || uc.getMods().contains(op)) {
            StringContainer.sendMessage(player, StringType.chatManagerUserChannelCannotBanAdmin);
            return true;
        }
        uc.getBanned().add(op);
        if (player2 != null) {
            StringContainer.sendMessage(player2, StringType.chatManagerUserChannelBanNotification, uc.getName());
            uc.leaveChat(player2, false, true);
            SimpleChannel channel = SimpleChatChannelManager.getModule().getListeningChannels(player2).size() > 0 ? SimpleChatChannelManager.getModule().getListeningChannels(player2).get(0) : SimpleChatChannelManager.getModule().getData().getDefaultChannel();
            channel.joinChat(player2, true);
        } else {
            User u = UserManager.getManager().getUser(op);
            CompoundTag tag = u.getCompoundTag(SimpleChatChannelManager.getModule());
            ListTag listening = tag.getList("listeningChannels");
            
            {
                ArrayList<String> listeningChans = new ArrayList<String>();
                for (Tag t : listening.getValue()) {
                    listeningChans.add(((StringTag) t).getValue());
                }
                listeningChans.remove(uc.getName());
                if (listeningChans.size() < 1) {
                    listeningChans.add(SimpleChatChannelManager.getModule().getData().getDefaultChannel().getName());
                }
                listening.getValue().clear();
                for (String s : listeningChans) {
                    listening.addTag(new StringTag("", s));
                }
                if (tag.getString("chattingChannel").equals(uc.getName())) {
                    tag.setString("chattingChannel", listeningChans.get(0));
                }
            }
            u.save();
        }
        SimpleChatChannelManager.getModule().saveChannel(uc);
        uc.messageStaff(StringType.chatManagerUserChannelUserBannedNotification, op);
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new ChannelAdminCommandValidator(true));
        argRuleset.addRule(new RequireExistingPlayernameRule(true, true, 0));
        argRuleset.addRule(new OperatorInvalidTarget(null, false, true, 0));
    }
    
}
