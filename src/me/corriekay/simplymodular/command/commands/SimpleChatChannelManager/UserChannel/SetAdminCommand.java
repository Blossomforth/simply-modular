/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand.ChannelAdminCommandValidator;
import me.corriekay.simplymodular.command.validation.arguments.RequireExistingPlayernameRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.questioner.Questioner;
import me.corriekay.simplymodular.command.validation.questioner.SimpleRunnable;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.UserChannel;
import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SetAdminCommand extends MetaSubCommand {
    
    public SetAdminCommand(MetaCommand supercommand) {
        super("setadmin", new CommandMeta(null, "sets the admin for this channel", "/uc setadmin <player>", CommandType.REQUIRES_PLAYER, 1), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws Exception {
        
        final UserChannel uc = (UserChannel) SimpleChatChannelManager.getModule().getChattingChannel(player);
        final String op = supercommand.getOfflinePlayer(args[0], player, false);
        if (UserChannel.isAdminOfAnyChannel(op)) {
            StringContainer.sendMessage(player, StringType.chatManagerUserChannelAdminChangeCannotPromoteAlreadyAdmin, op);
            return true;
        }
        
        Questioner question = new Questioner(StringContainer.getMessage(StringType.chatManagerUserChannelAdminChangeConfirmation, op, uc.getName()), player) {
            
            @Override
            protected boolean answerNotFound(String message) {
                return false;
            }
        };
        question.addAnswer("no", new SimpleRunnable() {
            public void run() {}
        });
        question.addAnswer("yes", new SimpleRunnable() {
            public void run() {
                setAdmin(op, uc);
            }
        });
        question.ask();
        return true;
    }
    
    private void setAdmin(String player, UserChannel channel) {
        String oldAdmin = channel.getAdmin();
        if (!channel.getMods().contains(oldAdmin)) {
            channel.getMods().add(oldAdmin);
        }
        channel.setAdmin(player);
        if (channel.getMods().contains(player)) {
            channel.getMods().remove(player);
        }
        @SuppressWarnings("deprecation")
        Player newAdmin = Bukkit.getPlayer(player);
        if (newAdmin != null) {
            StringContainer.sendMessage(newAdmin, StringType.channelManagerUserChannelAdminPromotionNotification, channel.getName());
        }
        channel.messageStaff(StringType.channelManagerUserChannelAdminChangeNotification, player);
        SimpleChatChannelManager.getModule().saveChannel(channel);
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new ChannelAdminCommandValidator(false));
        argRuleset.addRule(new RequireExistingPlayernameRule(true, true, 0));
    }
    
}
