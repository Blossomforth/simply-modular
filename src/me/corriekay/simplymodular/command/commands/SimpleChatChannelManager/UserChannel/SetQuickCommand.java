/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand.ChannelAdminCommandValidator;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.UserChannel;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetQuickCommand extends MetaSubCommand {
    
    public SetQuickCommand(MetaCommand supercommand) {
        super("setquick", new CommandMeta(null, "Allows a channel admin to set the quick for the channel", "/uc setquick <quick>", CommandType.REQUIRES_PLAYER, 1), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalException {
        UserChannel channel = (UserChannel) SimpleChatChannelManager.getModule().getChattingChannel(player);
        channel.setQuick(args[0].toLowerCase());
        SimpleChatChannelManager.getModule().saveChannel(channel);
        StringContainer.sendMessage(player, StringType.chatManagerUserChannelSetQuickSuccess, channel.getQuick());
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new ChannelAdminCommandValidator(false));
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                if (!SimplyModular.getCommandMap().checkAliasAvailable(args[0].toLowerCase())) {
                    StringContainer.sendMessage(sender, StringType.chatManagerUserChannelSetQuickCommandExists);
                    return false;
                }
                return true;
            }
        });
    }
    
}
