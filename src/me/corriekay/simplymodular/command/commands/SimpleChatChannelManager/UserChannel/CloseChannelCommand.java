/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand.ChannelAdminCommandValidator;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.questioner.Questioner;
import me.corriekay.simplymodular.command.validation.questioner.SimpleRunnable;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.UserChannel;
import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.entity.Player;

public class CloseChannelCommand extends MetaSubCommand {
    
    public CloseChannelCommand(MetaCommand supercommand) {
        super("closechannel", new CommandMeta(null, "retires the current channel, deleting it from the server.", "/uc closechannel", CommandType.REQUIRES_PLAYER, 0), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws Exception {
        final UserChannel uc = (UserChannel) SimpleChatChannelManager.getModule().getChattingChannel(player);
        
        Questioner question = new Questioner(StringContainer.getMessage(StringType.channelManagerUserChannelRetireConfirmation), player) {
            
            @Override
            protected boolean answerNotFound(String message) {
                return false;
            }
        };
        question.addAnswer("yes", new SimpleRunnable() {
            public void run() {
                SimpleChatChannelManager.getModule().retireChannel(uc);
            }
        });
        question.addAnswer("no", new SimpleRunnable() {});
        question.ask();
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new ChannelAdminCommandValidator(false));
    }
    
}
