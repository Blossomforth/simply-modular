/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import me.corriekay.simplymodular.command.MetaCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.*;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UCCommand extends MetaCommand {
    
    public UCCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.uc);
        addSubcommand(new CreateChannelCommand(this));
        addSubcommand(new SetQuickCommand(this));
        addSubcommand(new SetUCColorCommand(this));
        addSubcommand(new SetPasswordCommand(this));
        addSubcommand(new RemovePasswordCommand(this));
        addSubcommand(new SetModeratorCommand(this));
        addSubcommand(new BanUserCommand(this));
        addSubcommand(new UnbanCommand(this));
        addSubcommand(new SetAdminCommand(this));
        addSubcommand(new ChannelKickCommand(this));
        addSubcommand(new CloseChannelCommand(this));
        //TODO closechannel 
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
    /**
     * 
     * @author Corrie
     * Validates that the user is in a user channel and is either an OP on the server, or is the admin of the channel. Optionally, a moderator by boolean.
     */
    public static class ChannelAdminCommandValidator extends SimpleArgumentRule {
        
        private final boolean modOk;
        
        public ChannelAdminCommandValidator(boolean modOkay) {
            super(null, false, true);
            modOk = modOkay;
        }
        
        @Override
        public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
            SimpleChannel channel = SimpleChatChannelManager.getModule().getChattingChannel(sender);
            if (channel == null || !(channel instanceof UserChannel)) {
                StringContainer.sendMessage(sender, StringType.chatManagerUserChannelNotInUserChannel);
                return false;
            }
            UserChannel uc = (UserChannel) channel;
            if (!(uc.getAdmin().equals(sender.getName()) || (modOk && uc.getMods().contains(sender.getName())) || SimplePermissions.getPermissionsManager().getGroupByPlayer((Player) sender).isOpType())) {
                StringContainer.sendMessage(sender, StringType.chatManagerUserChannelNotAdmin);
                return false;
            }
            return true;
        }
        
    }
}
