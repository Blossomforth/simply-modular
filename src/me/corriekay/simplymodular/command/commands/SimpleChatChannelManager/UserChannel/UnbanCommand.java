/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel.UCCommand.ChannelAdminCommandValidator;
import me.corriekay.simplymodular.command.validation.arguments.RequireExistingPlayernameRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.UserChannel;
import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class UnbanCommand extends MetaSubCommand {
    
    public UnbanCommand(MetaCommand supercommand) {
        super("unban", new CommandMeta(null, "Unbans a player from this channel", "/uc unban <player>", CommandType.REQUIRES_PLAYER, 1), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        UserChannel uc = (UserChannel) SimpleChatChannelManager.getModule().getChattingChannel(player);
        String op = supercommand.getOfflinePlayer(args[0], player, false);
        @SuppressWarnings("deprecation")
        Player player2 = Bukkit.getPlayer(op);
        if (!uc.getBanned().contains(op)) {
            StringContainer.sendMessage(player, StringType.chatManagerUserChannelUnbanNotBanned);
            return true;
        }
        uc.getBanned().remove(op);
        if (player2 != null) {
            StringContainer.sendMessage(player2, StringType.chatManagerUserChannelUnbanNotification, uc.getName());
        }
        SimpleChatChannelManager.getModule().saveChannel(uc);
        uc.messageStaff(StringType.chatManagerUserChannelUserUnbannedNotification, op);
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new ChannelAdminCommandValidator(true));
        argRuleset.addRule(new RequireExistingPlayernameRule(true, true, 0));
    }
    
}
