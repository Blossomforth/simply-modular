/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager.UserChannel;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.*;
import me.corriekay.simplymodular.command.MetaCommand.CommandMeta;
import me.corriekay.simplymodular.command.MetaCommand.MetaSubCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata.CommandType;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.UserChannel;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateChannelCommand extends MetaSubCommand {
    
    public CreateChannelCommand(MetaCommand supercommand) {
        super("createchannel", new CommandMeta("simplymodular.simplechatchannelmanager.channeladmin", "Creates a user channel", "/uc createchannel <channelname>", CommandType.REQUIRES_PLAYER, 1), supercommand);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalException {
        String name, quick, icon;
        name = args[0];
        quick = name;
        icon = "[" + name + "]";
        ChatColor color = ChatColor.WHITE;
        boolean mature = false;
        String admin = player.getName();
        long lastActivity = System.currentTimeMillis();
        List<String> moderators, banned;
        moderators = new ArrayList<String>();
        banned = new ArrayList<String>();
        
        UserChannel uc = new UserChannel(name, quick, icon, null, color, mature, admin, moderators, banned, lastActivity, null, SimpleChatChannelManager.getModule());
        SimpleChatChannelManager.getModule().getData().addChannel(uc);
        SimplyModular.getCommandMap().setAlias("channel", uc.getName());
        SimpleChatChannelManager.getModule().saveChannel(uc);
        StringContainer.sendMessage(player, StringType.chatManagerUserChannelCreateSuccess, "/" + uc.getName());
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                if (UserChannel.isAdminOfAnyChannel(sender.getName())) {
                    StringContainer.sendMessage(sender, StringType.chatManagerUserChannelCreateAlreadyAdmin);
                    return false;
                }
                return true;
            }
        });
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                if (!SimplyModular.getCommandMap().checkAliasAvailable(args[0].toLowerCase())) {
                    StringContainer.sendMessage(sender, StringType.chatManagerUserChannelCreateAliasExists);
                    return false;
                }
                return true;
            }
        });
    }
    
}
