/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChannel;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MutedValidator extends SimpleArgumentRule {
    
    public MutedValidator() {
        super(StringContainer.getString(StringType.chatManagerAttemptChatChannelMute), true, true);
    }
    
    @Override
    public boolean checkRule(CommandSender sender, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            SimpleChannel channel = SimpleChatChannelManager.getModule().getChattingChannel(player);
            if (channel != null) {
                boolean global, chan;
                global = SimpleChatChannelManager.getModule().getData().getMuted().contains(player.getName());
                chan = channel.isMuted(player.getName());
                return !(global || chan);
            }
        }
        return true;
    }
    
}
