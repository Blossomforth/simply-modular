/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChannel;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.Utils;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MeCommand extends SimpleCommand {
    public MeCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.me);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        SimpleChannel channel = SimpleChatChannelManager.getModule().getChattingChannel(player);
        String emote = Utils.joinStringArray(args, " ");
        if (label.equals("me's")) {
            emote = "'s " + emote;
        }
        channel.broadcastEmote(emote, player.getDisplayName(), true);
        return true;
    }
    
    public boolean consoleCommand(ConsoleCommandSender console, String label, String[] args) {
        SimpleChannel channel = SimpleChatChannelManager.getModule().getChattingChannel(console);
        String emote = Utils.joinStringArray(args, " ");
        if (label.equals("me's")) {
            emote = "'s " + emote;
        }
        channel.broadcastEmote(emote, "Server", true);
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new NoChannelValidation());
        argRuleset.addRule(new MutedValidator());
    }
    
}
