/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.*;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ChannelCommand extends SimpleCommand {
    public ChannelCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.channel);
    }
    
    public boolean optionalCommand(CommandSender sender, String alias, String[] args) throws Exception {
        if (alias.equals(SimpleCommandMetadata.channel.getCommandName())) {
            ChannelDataManager data = SimpleChatChannelManager.getModule().getData();
            ArrayList<String> publicChannels = new ArrayList<String>();
            ArrayList<String> privateChannels = new ArrayList<String>();
            for (SimpleChannel channel : data.getChannels().values()) {
                if (channel.getPermission() == null || sender.hasPermission(channel.getPermission())) {
                    if (channel.isPrivateChannel()) {
                        privateChannels.add(channel.getName());
                    } else {
                        publicChannels.add(channel.getName());
                    }
                }
            }
            Collections.sort(publicChannels);
            Collections.sort(privateChannels);
            StringContainer.sendMessage(sender, StringType.chatManagerChannelListCommand, getChannelListFormatted(publicChannels, data), getChannelListFormatted(privateChannels, data));
            return true;
        } else {
            if (sender instanceof Player) {
                return playerCommand((Player) sender, alias, args);
            } else if (sender instanceof ConsoleCommandSender) {
                return consoleCommand((ConsoleCommandSender) sender, alias, args);
            } else {
                return false;
            }
        }
    }
    
    public boolean playerCommand(Player player, String alias, String[] args) throws SimplyModularFatalModuleException {
        SimpleChannel channel = SimpleChatChannelManager.getModule().getChannel(alias);
        if (channel.isListening(player)) {
            SimpleChatChannelManager.getModule().chat(channel, player, Utils.joinStringArray(args, " "), true);
        } else {
            StringContainer.sendMessage(player, StringType.chatManagerNotListeningToChannelNotification);
        }
        return true;
    }
    
    public boolean consoleCommand(ConsoleCommandSender sender, String alias, String[] args) throws SimplyModularFatalModuleException {
        SimpleChannel channel = SimpleChatChannelManager.getModule().getChannel(alias);
        channel.broadcastToChannel("Server", null, Utils.joinStringArray(args, " "), true);
        return true;
    }
    
    private String getChannelListFormatted(ArrayList<String> chans, ChannelDataManager data) {
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (String chanName : chans) {
            SimpleChannel chan = data.getChannels().get(chanName);
            if (chan == null) {
                Debug.logHighlyVerbose("Null channel in Channel command! Attempted to find channel named " + chanName + " however, not found!", false, Level.SEVERE);
                continue;
            }
            builder.append(chan.getFormattedName() + ChatColor.WHITE + ", ");
            i++;
        }
        if (i == 0) {
            return "No Channels";
        } else {
            return builder.substring(0, builder.length() - 4).toString();
        }
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(StringContainer.getMessage(StringType.chatManagerChannelNullNotification), true, true) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) {
                if (label.equals(getMeta().getCommandName())) { return true; }
                SimpleChannel chan = SimpleChatChannelManager.getModule().getChannel(label);
                if (chan == null) { return false; }
                return true;
            }
            
        });
    }
    
}
