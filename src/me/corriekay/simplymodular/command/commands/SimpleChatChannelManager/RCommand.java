/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.PrivateMessageEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class RCommand extends SimpleCommand {
    
    public RCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.r);
    }
    
    @SuppressWarnings("deprecation")
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        SimpleChatChannelManager sccm = SimpleChatChannelManager.getModule();
        String starget = sccm.getData().getPmTargets().get(player.getName());
        if (starget == null) {
            StringContainer.sendMessage(player, StringType.chatManagerSendPrivateMessageFailNull);
            return true;
        }
        Player target = Bukkit.getPlayerExact(starget);
        if (target == null) {
            StringContainer.sendMessage(player, StringType.playerNotFound, starget);
            return true;
        }
        if (!sccm.doSendMessage(player, target)) {
            StringContainer.sendMessage(player, StringType.chatManagerSendMessageFail, target.getName());
            return true;
        }
        PrivateMessageEvent pme = new PrivateMessageEvent(player, target, Utils.joinStringArray(args, " "));
        Bukkit.getPluginManager().callEvent(pme);
        if (!pme.isCancelled()) {
            StringContainer.sendMessage(player, StringType.chatManagerSendPrivateMessage, target.getDisplayName(), pme.getMsg());
            StringContainer.sendMessage(target, StringType.chatManagerRecievePrivateMessage, player.getDisplayName(), pme.getMsg());
            SimpleChatChannelManager.getModule().updatePMs(player.getName(), target.getName());
            return true;
        }
        return false;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new MutedValidator());
    }
    
}
