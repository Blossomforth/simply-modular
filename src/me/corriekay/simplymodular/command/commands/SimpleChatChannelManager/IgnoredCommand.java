/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import java.util.HashSet;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.StringContainer;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.entity.Player;

public class IgnoredCommand extends SimpleCommand {
    
    public IgnoredCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.ignored);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        HashSet<String> ignored = SimpleChatChannelManager.getModule().getData().getIgnores().get(player.getName());
        if (ignored.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : ignored) {
                sb.append(s + ", ");
            }
            StringContainer.sendMessage(player, StringType.chatManagerDisplayIgnores, sb.substring(0, sb.length() - 2));
        } else {
            StringContainer.sendMessage(player, StringType.chatManagerNoIgnores);
        }
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
