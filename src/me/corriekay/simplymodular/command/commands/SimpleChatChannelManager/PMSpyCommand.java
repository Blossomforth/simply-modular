/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import java.util.HashSet;

import me.corriekay.simplymodular.command.ListenerCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.*;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PMSpyCommand extends ListenerCommand {
    
    private final HashSet<String> spies = new HashSet<String>();
    @SuppressWarnings("unused")
    private final SimpleListener joinquitListener = new SimpleListener() {
        @SimpleEvent
        public void join(PlayerJoinEvent event) throws SimplyModularFatalModuleException {
            updatePlayer(event.getPlayer());
            updateListener();
        }
        
        @SimpleEvent
        public void quit(PlayerQuitEvent event) {
            spies.remove(event.getPlayer().getName());
        }
    };
    
    public PMSpyCommand(SimpleModule module) throws SimplyModularFatalModuleException {
        super(module, SimpleCommandMetadata.pmspy, false);
        for (Player player : Bukkit.getOnlinePlayers()) {
            updatePlayer(player);
        }
        updateListener();
    }
    
    private void updatePlayer(Player player) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        if (user.getCompoundTag(module).getBoolean("pmspy")) {
            spies.add(player.getName());
        }
    }
    
    @SimpleEvent
    public void pmSpy(PrivateMessageEvent event) {
        for (String spyString : spies) {
            @SuppressWarnings("deprecation")
            Player spy = Bukkit.getPlayerExact(spyString);
            if (spy != null) {
                if (spy.getName().equals(event.getSender().getName()) || spy.getName().equals(event.getReciever().getName())) {
                    continue;
                }
                StringContainer.sendMessage(spy, StringType.chatManagerPmSpyEvent, event.getSender().getDisplayName(), event.getReciever().getDisplayName(), event.getMsg());
            }
        }
    }
    
    private void updateListener() {
        if (spies.size() > 0) {
            registerEvents();
        } else {
            unregister();
        }
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        User user = getUser(player);
        boolean currentFlag = spies.contains(player.getName());
        if (currentFlag) { //Theyre spying, so we're turning it off;
            spies.remove(player.getName());
            StringContainer.sendMessage(player, StringType.chatManagerPMSpyDisable);
        } else { //Not spying, turning on
            spies.add(player.getName());
            StringContainer.sendMessage(player, StringType.chatManagerPMSpyEnable);
        }
        updateListener();
        user.getCompoundTag(module).setBoolean("pmspy", !currentFlag);
        user.save();
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
