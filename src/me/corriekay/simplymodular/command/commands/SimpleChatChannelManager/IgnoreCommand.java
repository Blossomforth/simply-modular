/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChatChannelManager;

import java.util.HashSet;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.RequireExistingPlayernameRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.optional.SimpleChatChannelManager.SimpleChatChannelManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jnbt.*;

public class IgnoreCommand extends SimpleCommand {
    
    public IgnoreCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.ignore);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        SimpleChatChannelManager sccm = (SimpleChatChannelManager) module;
        OfflinePlayer target = module.getOnlineOfflinePlayer(args[0], player, false);
        User user = getUser(player);
        User tuser = getUser(target.getName());
        CompoundTag ctag = user.getCompoundTag(sccm);
        
        Group tgroup = getGroupByUser(tuser);
        
        HashSet<String> ignored = sccm.getIgnoredPlayers(player);
        
        if (ignored.contains(target.getName())) { //unignore player
            ignored.remove(target.getName());
            StringContainer.sendMessage(player, StringType.chatManagerUnignoreSuccessful, target.getName());//TODO refactor system to allow to get display name, basically (op.isOnline() ? ((Player)op).getDisplayName() : op.getName())
            
        } else {
            if (tgroup.isOpType()) { //ignore player
                StringContainer.sendMessage(player, StringType.chatManagerIgnoreCannotIgnoreOP);
                return true;
            }
            ignored.add(target.getName());
            StringContainer.sendMessage(player, StringType.chatManagerIgnoreSuccessful, target.getName());
        }
        
        ListTag list = new ListTag("ignored", StringTag.class);
        for (String ignoreme : ignored) {
            list.addTag(new StringTag("", ignoreme));
        }
        ctag.setList("ignored", list);
        user.save();
        
        sccm.updateIgnored(player);
        
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new RequireExistingPlayernameRule(true, true, 0));
    }
    
}
