/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChestProtect;

import java.util.ArrayList;
import java.util.HashMap;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.ListenerCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.*;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.Protection.LockType;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Protect extends ListenerCommand {
    
    private HashMap<String, LockType> players = new HashMap<String, LockType>();
    private ChestProtectOptions cpo;
    
    public Protect(SimpleModule module) {
        super(module, SimpleCommandMetadata.protect, false);
        SimplyModular.getNameListener().registerMapKey(players);
        cpo = SimpleChestProtect.getOptions();
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        if (players.containsKey(player.getName())) {
            unregisterPlayer(player.getName());
            StringContainer.sendMessage(player, StringType.protectCancel);
        } else {
            if (players.size() == 0) {
                registerEvents();
            }
            LockType type = LockType.PRIVATE;
            if (args.length > 0) {
                type = LockType.valueOf(args[0]);
            }
            players.put(player.getName(), type);
            StringContainer.sendMessage(player, StringType.protectActivate);
        }
        return true;
    }
    
    private void unregisterPlayer(String name) {
        players.remove(name);
        if (players.size() == 0) {
            unregister();
        }
    }
    
    @SimpleEvent(priority = EventPriority.LOWEST)
    public void onBlockPunch(PlayerInteractEvent event) throws SimplyModularFatalException {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) { return; }
        Player player = event.getPlayer();
        Material type = event.getClickedBlock().getType();
        if (players.containsKey(player.getName())) {
            event.setCancelled(true);
            LockType locktype = players.get(player.getName());
            players.remove(player.getName());
            if (players.size() == 0) {
                unregister();
            }
            if ((event.getClickedBlock().getType() != Material.AIR) && !cpo.isIgnoredWorld(player.getWorld().getName())) {
                //Player is registered to protect,  the block type is not air, and theyre not in an ignored world.
                if (type == Material.WATER || type == Material.LAVA) { return;//Player is punching a liquid, ignore.
                }
                if (cpo.isAllowedBlockType(type)) {
                    //player is attempting to punch an allowed blocktype
                    if (!SimpleChestProtect.getSCP().canProtectAdditionalBlock(player, player.getWorld(), event.getClickedBlock(), true)) { return; }
                    if (SimpleChestProtect.getSCP().getBlockProtection(event.getClickedBlock(), false) != null) {
                        StringContainer.sendMessage(player, StringType.protectExistingProtection);
                        return;
                    }
                    SimpleChestProtect.getSCP().addBlockProtection(player.getName(), event.getClickedBlock(), true, locktype, new ArrayList<String>());
                    return;
                } else {
                    StringContainer.sendMessage(player, StringType.protectWrongBlocktype, type.name());
                }
            }
            if (cpo.isIgnoredWorld(player.getWorld().getName())) {
                StringContainer.sendMessage(player, StringType.protectIgnoredWorld, player.getWorld().getName());
            }
        }
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
}
