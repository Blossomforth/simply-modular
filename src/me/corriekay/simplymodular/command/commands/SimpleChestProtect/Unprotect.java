/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChestProtect;

import java.util.HashSet;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.ListenerCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.Protection;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.SimpleChestProtect;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Unprotect extends ListenerCommand {
    
    private final HashSet<String> removingPlayers = new HashSet<String>();
    
    public Unprotect(SimpleModule module) {
        super(module, SimpleCommandMetadata.unprotect, false);
        SimplyModular.getNameListener().registerSet(removingPlayers);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        if (removingPlayers.contains(player.getName())) {
            StringContainer.sendMessage(player, StringType.unprotectRemoveProtectionCommandStop);
            removingPlayers.remove(player.getName());
            if (removingPlayers.size() == 0) {
                unregister();
            }
        } else {
            StringContainer.sendMessage(player, StringType.unprotectRemoveProtectionCommand);
            if (removingPlayers.size() == 0) {
                registerEvents();
            }
            removingPlayers.add(player.getName());
        }
        return true;
    }
    
    @SimpleEvent
    public void interact(PlayerInteractEvent event) throws SimplyModularFatalModuleException {
        Player player = event.getPlayer();
        if (!removingPlayers.contains(player.getName()) || event.getAction() != Action.LEFT_CLICK_BLOCK) { return; }
        event.setCancelled(true);
        Block block = event.getClickedBlock();
        Protection protection = SimpleChestProtect.getSCP().getBlockProtection(block, false);
        
        if (protection == null) {
            StringContainer.sendMessage(player, StringType.unprotectRemoveProtectionNoProtection);
        } else {
            if (protection.getOwner().equals(player.getName()) || player.hasPermission("simplechestprotect.admin")) {
                StringContainer.sendMessage(player, StringType.unprotectRemoveProtectionSuccessful, SimpleChestProtect.locationToString(event.getClickedBlock()), event.getClickedBlock().getType().name());
                SimpleChestProtect.getSCP().deleteBlockProtection(protection, protection.getOwner().equals(player.getName()), true);
                removingPlayers.remove(player.getName());
                if (removingPlayers.size() == 0) {
                    unregister();
                }
            } else {
                StringContainer.sendMessage(player, StringType.unprotectRemoveProtectionNoPermission);
            }
        }
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
