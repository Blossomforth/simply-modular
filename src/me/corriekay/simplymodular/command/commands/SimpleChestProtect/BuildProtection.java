/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleChestProtect;

import java.util.*;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.ListenerCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.SimpleEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.*;
import me.corriekay.simplymodular.module.optional.SimpleChestProtect.Protection.LockType;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Material;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class BuildProtection extends ListenerCommand implements
        ConversationAbandonedListener {
    private HashMap<String, OptionsContainer> players = new HashMap<String, OptionsContainer>();
    private ChestProtectOptions cpo;
    
    public BuildProtection(SimpleModule module) {
        super(module, SimpleCommandMetadata.buildprotection, false);
        SimplyModular.getNameListener().registerMapKey(players);
        cpo = SimpleChestProtect.getOptions();
    }
    
    @SimpleEvent
    public void onInteract(PlayerInteractEvent event) throws SimplyModularFatalModuleException {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) { return; }
        Player player = event.getPlayer();
        Material type = event.getClickedBlock().getType();
        if (players.containsKey(player.getName())) {
            event.setCancelled(true);
            if ((event.getClickedBlock().getType() != Material.AIR) && !cpo.isIgnoredWorld(player.getWorld().getName())) {
                if (type == Material.LAVA || type == Material.WATER) { return; }
                if (cpo.isAllowedBlockType(type)) {
                    if (!SimpleChestProtect.getSCP().canProtectAdditionalBlock(player, player.getWorld(), event.getClickedBlock(), true)) { return; }
                    OptionsContainer options = players.get(player.getName());
                    Protection p = SimpleChestProtect.getSCP().getBlockProtection(event.getClickedBlock(), false);
                    if (p != null) {
                        if (p.getOwner().equals(player.getName())) {
                            Debug.logDebug("Updating block protection because owners are equals! protection owner? " + p.getOwner() + " puncher? " + player.getName());
                            for (String s : options.hasPermission) {
                                if (s.startsWith("-")) {
                                    p.getHasPermission().remove(s.substring(1, s.length()));
                                } else {
                                    p.getHasPermission().add(s);
                                }
                            }
                            p.setLockType(options.type);
                            SimpleChestProtect.getSCP().deleteBlockProtection(p, false, true);
                            SimpleChestProtect.getSCP().addBlockProtection(player.getName(), event.getClickedBlock(), false, p.getType(), p.getHasPermission());
                            StringContainer.sendMessage(player, StringType.buildProtectionPaintUpdate);
                        } else {
                            StringContainer.sendMessage(player, StringType.buildProtectionPaintDenyDoesNotOwnProtection);
                        }
                        return;
                    } else {
                        ArrayList<String> hasProtection = new ArrayList<String>();
                        for (String s : options.hasPermission) {
                            if (!s.startsWith("-")) {
                                hasProtection.add(s);
                            }
                        }
                        SimpleChestProtect.getSCP().addBlockProtection(player.getName(), event.getClickedBlock(), true, options.type, hasProtection);
                        StringContainer.sendMessage(player, StringType.buildProtectionPaintAdd);
                    }
                }
            }
        }
    }
    
    public boolean playerCommand(Player player, String label, String[] args) {
        initializeCommand(player);
        return true;
    }
    
    private void unregisterPlayer(String name) {
        players.remove(name);
        if (players.size() == 0) {
            unregister();
        }
    }
    
    class OptionsContainer {
        public boolean building = true;
        LockType type = LockType.PRIVATE;
        Set<String> hasPermission = new HashSet<String>();
    }
    
    // @formatter:off
	private void initializeCommand(Player player) {
		Debug.logDebug("starting protection conversation");
		ConversationFactory buildConv = new ConversationFactory(SimplyModular.getSM());
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("options", new OptionsContainer());
		buildConv.withModality(false)
		.withLocalEcho(false)
		.withConversationCanceller(new ExactMatchConversationCanceller("exit"))
		.withInitialSessionData(map)
		.addConversationAbandonedListener(this)
		.withFirstPrompt(new ProtectionBuildConvoStart())
		.buildConversation(player).begin();
	}
	// @formatter:on
    
    private Player getConversationPlayer(ConversationContext context) {
        return (Player) context.getForWhom();
    }
    
    public void conversationAbandoned(ConversationAbandonedEvent arg0) {
        unregisterPlayer(((Player) arg0.getContext().getForWhom()).getName());
    }
    
    class ProtectionBuildConvoStart extends MessagePrompt {//Start the conversation
        public String getPromptText(ConversationContext arg0) {
            return StringContainer.getMessage(StringType.buildProtectionConversationStart);
        }
        
        protected Prompt getNextPrompt(ConversationContext arg0) {
            return new ProtectionBuildConvoGetProtectionType();
        }
    }
    
    class ProtectionBuildConvoGetProtectionType extends ValidatingPrompt {//protection type?
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.buildProtectionConversationPromptType);
        }
        
        protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
            LockType type = LockType.valueOf(arg1.toUpperCase());
            ((OptionsContainer) arg0.getSessionData("options")).type = type;
            return new ProtectionBuildConvoGetAdditionalPermissions();
        }
        
        protected boolean isInputValid(ConversationContext arg0, String arg1) {
            return (arg1.equalsIgnoreCase("private") || arg1.equalsIgnoreCase("public") || arg1.equalsIgnoreCase("donation"));
        }
    }
    
    class ProtectionBuildConvoGetAdditionalPermissions extends ValidatingPrompt {
        boolean textPrompted = false;
        
        public String getPromptText(ConversationContext context) {
            if (textPrompted) { return ""; }
            textPrompted = true;
            return StringContainer.getMessage(StringType.buildProtectionConversationPromptAdditionalPermissions);
        }
        
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            return new ProtectionBuildConvoApplyProtections();
        }
        
        protected boolean isInputValid(ConversationContext context, String input) {
            if (input.equalsIgnoreCase("done")) { return true; }
            OptionsContainer options = ((OptionsContainer) context.getSessionData("options"));
            input = input.replaceAll(",", "");
            for (String name : input.split(" ")) {//TODO change to grab player name from name fragment after system is implemented
                if (name.startsWith("-")) {
                    if (!options.hasPermission.contains(name)) {
                        name = name.substring(1, name.length());
                        options.hasPermission.remove(name);
                        options.hasPermission.add("-" + name);
                        StringContainer.sendMessage(getConversationPlayer(context), StringType.buildProtectionConversationFeedbackAdditionalPermissionsRemove, name);
                    } else {
                        StringContainer.sendMessage(getConversationPlayer(context), StringType.buildProtectionConversationFeedbackAdditionalPermissionsRemoveAlreadyRemoving, name);
                    }
                } else {
                    if (!options.hasPermission.contains(name)) {
                        options.hasPermission.remove("-" + name);
                        options.hasPermission.add(name);
                        StringContainer.sendMessage(getConversationPlayer(context), StringType.buildProtectionConversationFeedbackAdditionalPermissionsAdd, name);
                    } else {
                        StringContainer.sendMessage(getConversationPlayer(context), StringType.buildProtectionConversationFeedbackAdditionalPermissionsAddAlreadyExists, name);
                    }
                }
            }
            return false;
        }
    }
    
    class ProtectionBuildConvoApplyProtections extends ValidatingPrompt {
        
        public String getPromptText(ConversationContext context) {
            if (players.size() == 0) {
                registerEvents();
            }
            players.put(((Player) context.getForWhom()).getName(), (OptionsContainer) context.getSessionData("options"));
            return StringContainer.getMessage(StringType.buildProtectionConversationSetup);
        }
        
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            return new ProtectionBuildConvoFinish();
        }
        
        protected boolean isInputValid(ConversationContext context, String input) {
            return input.equals("/exit");
        }
    }
    
    class ProtectionBuildConvoFinish extends MessagePrompt {
        
        @Override
        public String getPromptText(ConversationContext arg0) {
            players.remove(((Player) arg0.getForWhom()).getName());
            if (players.size() == 0) {
                unregister();
            }
            return StringContainer.getMessage(StringType.buildProtectionConversationFinish);
        }
        
        @Override
        protected Prompt getNextPrompt(ConversationContext arg0) {
            return null;
        }
        
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
}
