/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleWorldManager;

import me.corriekay.simplymodular.command.ListenerCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimpleWorldManagement.SimpleWorld;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;
import net.minecraft.server.v1_7_R3.MinecraftServer;
import net.minecraft.server.v1_7_R3.WorldServer;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R3.CraftServer;
import org.bukkit.craftbukkit.v1_7_R3.CraftWorld;
import org.bukkit.event.world.WorldUnloadEvent;

public class RemoveWorldCommand extends ListenerCommand {
    
    public RemoveWorldCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.removeworld, true);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) {
        SimpleWorld world = SimpleWorld.getWorld(args[0]);
        int trying = 1;
        String worldname = world.getName();
        while (trying <= 3) {
            StringContainer.sendMessage(sender, StringType.worldManagerRemoveWorldAttempting, "\"" + worldname + "\"", trying);
            if (SimpleWorld.unloadWorld(world)) {
                StringContainer.sendMessage(sender, StringType.worldManagerRemoveWorldSuccess, "\"" + worldname + "\"");
                return true;
            }
            trying++;
        }
        StringContainer.sendMessage(sender, StringType.worldManagerRemoveWorldFailure);
        
        return true;
    }
    
    //@SimpleEvent(ignoreCancelled = true)
    public void worldUnloadEvent(WorldUnloadEvent event) throws Exception {
        System.out.println("Event cancelled: " + event.isCancelled());
        
        {
            World world = event.getWorld();
            
            WorldServer handle = ((CraftWorld) world).getHandle();
            
            boolean contains, dimension, players;
            
            {
                MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
                contains = !(server.worlds.contains(handle));
            }
            
            {
                dimension = !(handle.dimension > 1);
            }
            
            {
                players = handle.players.size() > 0;
            }
            
            System.out.println("Attempting to unload world. Returns - contains: " + contains + ", dimension: " + dimension + ". players: " + players + ".");
        }
        
        event.setCancelled(false);
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            
            @Override
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalException {
                SimpleWorld world = SimpleWorld.getWorld(args[0]);
                if (world == null) {
                    StringContainer.sendMessage(sender, StringType.worldManagerRemoveWorldTargetWorldNull);
                    return false;
                }
                if (SimpleWorld.getDefaultWorld().equals(world)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerRemoveWorldTargetWorldDefault);
                    return false;
                }
                if (!SimpleWorld.verifyCanRemoveWorld(world)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerRemoveWorldTargetWorldMojangWorld);
                    return false;
                }
                return true;
            }
        });
    }
    
}
