/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleWorldManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimpleWorldManagement.SimpleWorldManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class SetWorldSpawnCommand extends SimpleCommand {
    
    public SetWorldSpawnCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.setworldspawn);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalException {
        World world = player.getWorld();
        Location l = player.getLocation();
        world.setSpawnLocation(l.getBlockX(), l.getBlockY(), l.getBlockZ());
        SimpleWorldManager.writeMeta(world);
        StringContainer.sendMessage(player, StringType.worldManagerNotifyWorldSpawnSet, world.getName(), l.getBlockX() + ", " + l.getBlockY() + ", " + l.getBlockZ());
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
