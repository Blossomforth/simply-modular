/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleWorldManager;

import java.util.HashMap;

import me.corriekay.simplymodular.SimplyModular;
import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimpleWorldManagement.*;
import me.corriekay.simplymodular.module.core.SimpleWorldManagement.WorldGeneratorOptions.UniverseGeneratorOptions;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.*;
import org.bukkit.World.Environment;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.conversations.*;
import org.jnbt.CompoundTag;

public class CreateWorldCommand extends SimpleCommand implements
        ConversationAbandonedListener {
    
    boolean creatingWorld = false;
    
    public CreateWorldCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.createworld);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws Exception {
        if (creatingWorld) {
            StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldAlreadyCreatingWorld);
            return true;
        }
        creatingWorld = true;
        startConversation(sender);
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
    private UniverseGeneratorOptions getUGO(ConversationContext context) {
        return (UniverseGeneratorOptions) context.getSessionData("Options");
    }
    
    private boolean exists(ConversationContext context) {
        return context.getSessionData("metadataExists") != null;
    }
    
    private CommandSender getSender(ConversationContext context) {
        return (CommandSender) context.getForWhom();
    }
    
    private void startConversation(CommandSender sender) throws SimplyModularFatalException {
        ConversationFactory factory = new ConversationFactory(SimplyModular.getSM());
        HashMap<Object, Object> map = new HashMap<Object, Object>();
        map.put("completed", false);
        factory.withInitialSessionData(map);
        factory.withModality(true).withEscapeSequence("exit").withLocalEcho(true);
        factory.withFirstPrompt(new ExplanationPrompt());
        factory.addConversationAbandonedListener(this);
        factory.buildConversation((Conversable) sender).begin();
    }
    
    public class ExplanationPrompt extends StringPrompt {
        
        @Override
        public Prompt acceptInput(ConversationContext context, String input) {
            return new NamePrompt();
        }
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldIntro);
        }
        
    }
    
    public class NamePrompt extends ValidatingPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            try {
                return StringContainer.getMessage(StringType.worldManagerCreateWorldAskWorldname, Utils.joinArrayList(SimpleWorld.getUnloadedWorldsOfType(Environment.NORMAL), ", "));
            } catch (SimplyModularFatalException e) {
                e.printStackTrace();
            }
            return null;
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            return new NetherPrompt();
        }
        
        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            try {
                CommandSender sender = getSender(context);
                if (SimpleWorld.getWorld(input) != null) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldAlreadyExists, input);
                    return false;
                }
                if (SimpleWorld.worldExistsInFile(input) && !SimpleWorld.getWorldMeta(input).getString("environment").equals("NORMAL")) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldExistsNotOfWorldtype, "normal");
                    return false;
                }
                if (!SimpleWorld.isValidWorldname(input)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldInvalidWorldname, input);
                    return false;
                }
                UniverseGeneratorOptions ugo = new UniverseGeneratorOptions(input);
                if (SimpleWorld.worldExistsInFile(input)) {
                    CompoundTag meta = SimpleWorld.getWorldMeta(input);
                    ugo.setSeed(meta.getString("seed"));
                    ugo.setEnvironment(Environment.valueOf(meta.getString("environment")));
                    ugo.setSpawnStructures(meta.getBoolean("structures"));
                    ugo.setWorldType(WorldType.valueOf(meta.getString("worldtype")));
                    context.setSessionData("metadataExists", true);
                }
                context.setSessionData("Options", ugo);
                return true;
            } catch (SimplyModularFatalException e) {
                e.printStackTrace();
                return false;
            }
        }
    }
    
    public class NetherPrompt extends ValidatingPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            try {
                return StringContainer.getMessage(StringType.worldManagerCreateWorldAskNether, Utils.joinArrayList(SimpleWorld.getUnloadedWorldsOfType(Environment.NETHER), ", "));
            } catch (SimplyModularFatalException e) {
                e.printStackTrace();
            }
            return null;
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            return new EndPrompt();
        }
        
        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            try {
                if (input.equalsIgnoreCase("skip")) { return true; }
                CommandSender sender = getSender(context);
                if (SimpleWorld.getWorld(input) != null) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldAlreadyExists, input);
                    return false;
                }
                if (SimpleWorld.worldExistsInFile(input) && !SimpleWorld.getWorldMeta(input).getString("environment").equals("NETHER")) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldExistsNotOfWorldtype, "nether");
                    return false;
                }
                if (!SimpleWorld.isValidWorldname(input)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldInvalidWorldname, input);
                    return false;
                }
                if (getUGO(context).getName().equals(input)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldAskNetherDecline, input);
                    return false;
                }
                getUGO(context).setNether(input);
                return true;
            } catch (SimplyModularFatalException e) {
                e.printStackTrace();
                return false;
            }
        }
    }
    
    public class EndPrompt extends ValidatingPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            try {
                return StringContainer.getMessage(StringType.worldManagerCreateWorldAskEnd, Utils.joinArrayList(SimpleWorld.getUnloadedWorldsOfType(Environment.THE_END), ", "));
            } catch (SimplyModularFatalException e) {
                e.printStackTrace();
            }
            return null;
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            return new PVPPrompt();
        }
        
        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            try {
                if (input.equalsIgnoreCase("skip")) { return true; }
                CommandSender sender = getSender(context);
                if (SimpleWorld.getWorld(input) != null) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldAlreadyExists, input);
                    return false;
                }
                if (SimpleWorld.worldExistsInFile(input) && !SimpleWorld.getWorldMeta(input).getString("environment").equals("THE_END")) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldExistsNotOfWorldtype, "the_end");
                    return false;
                }
                if (!SimpleWorld.isValidWorldname(input)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldInvalidWorldname, input);
                    return false;
                }
                if (getUGO(context).getName().equals(input) || getUGO(context).getNetherName().equals(input)) {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldAskEndDecline, input);
                    return false;
                }
                getUGO(context).setEndName(input);
                return true;
            } catch (SimplyModularFatalException e) {
                e.printStackTrace();
                return false;
            }
        }
    }
    
    public class PVPPrompt extends BooleanPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskPVP);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, boolean input) {
            getUGO(context).setPVP(input);
            return exists(context) ? new DifficultyPrompt() : new GameModePrompt();
        }
    }
    
    public class GameModePrompt extends ValidatingPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskGameMode);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            GameMode gm = GameMode.valueOf(input.toUpperCase());
            getUGO(context).setGameMode(gm);
            return new DifficultyPrompt();
        }
        
        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            switch (input.toUpperCase()) {
                case "CREATIVE":
                    return true;
                case "SURVIVAL":
                    return true;
                case "ADVENTURE":
                    return true;
                default:
                    return false;
            }
        }
    }
    
    public class DifficultyPrompt extends ValidatingPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskDifficulty);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            Difficulty d = Difficulty.valueOf(input.toUpperCase());
            getUGO(context).setDifficulty(d);
            return new HostileMobPrompt();
        }
        
        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            switch (input.toUpperCase()) {
                case "PEACEFUL":
                    return true;
                case "EASY":
                    return true;
                case "NORMAL":
                    return true;
                case "HARD":
                    return true;
                default:
                    return false;
            }
        }
    }
    
    public class HostileMobPrompt extends NumericPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskHostileMobs);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, Number input) {
            getUGO(context).setHostileMobSpawnLimit(input.intValue());
            return new NeutralMobPrompt();
        }
        
    }
    
    public class NeutralMobPrompt extends NumericPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskNeutralMobs);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, Number input) {
            getUGO(context).setNeutralMobSpawnLimit(input.intValue());
            return new BackpackPrompt();
        }
    }
    
    public class BackpackPrompt extends BooleanPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskBackpacks);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, boolean input) {
            getUGO(context).setBackpacks(input);
            return new EnderchestPrompt();
        }
        
    }
    
    public class EnderchestPrompt extends BooleanPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskEnderchests);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, boolean input) {
            getUGO(context).setEnderchest(input);
            if (exists(context)) {
                context.setSessionData("completed", true);
                return null;
            } else return new WorldTypePrompt();
            //return exists(context) ? null : new WorldTypePrompt();
        }
        
    }
    
    public class WorldTypePrompt extends ValidatingPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskWorldType);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, String input) {
            WorldType type = WorldType.valueOf(input.toUpperCase());
            Debug.logDebug("worldtype set to " + type.name());
            getUGO(context).setWorldType(type);
            return new StructuresPrompt();
        }
        
        @Override
        protected boolean isInputValid(ConversationContext context, String input) {
            switch (input.toUpperCase()) {
                case "AMPLIFIED":
                    return true;
                case "NORMAL":
                    return true;
                case "LARGE_BIOMES":
                    return true;
                case "FLAT":
                    return true;
                default:
                    return false;
            }
        }
    }
    
    public class StructuresPrompt extends BooleanPrompt {
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskStructures);
        }
        
        @Override
        protected Prompt acceptValidatedInput(ConversationContext context, boolean input) {
            getUGO(context).setSpawnStructures(input);
            return new SeedPrompt();
        }
    }
    
    public class SeedPrompt extends StringPrompt {
        
        @Override
        public Prompt acceptInput(ConversationContext context, String input) {
            getUGO(context).setSeed(input);
            context.setSessionData("completed", true);
            return null;
        }
        
        @Override
        public String getPromptText(ConversationContext context) {
            return StringContainer.getMessage(StringType.worldManagerCreateWorldAskSeed);
        }
        
    }
    
    @Override
    public void conversationAbandoned(ConversationAbandonedEvent abandonedEvent) {
        creatingWorld = false;
        final CommandSender sender = getSender(abandonedEvent.getContext());
        if (!(boolean) abandonedEvent.getContext().getSessionData("completed")) {
            StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldFailedOrCancelled);
            return;
        }
        final UniverseGeneratorOptions ugo = getUGO(abandonedEvent.getContext());
        Bukkit.getScheduler().runTask(SimplyModular.getSM(), new Runnable() {
            
            @Override
            public void run() {
                ConfigurationSection section = SimpleWorldManager.getManager().getConfig().createSection("worlds." + ugo.getName());
                section.set("pvp", ugo.getPVP());
                section.set("gamemode", ugo.getGameMode().name());
                section.set("difficulty", ugo.getDifficulty().name());
                section.set("hostilemobs", ugo.gethostileMobSpawnCount());
                section.set("neutralmobs", ugo.getNeutralMobSpawnCount());
                section.set("nether", ugo.getNetherName());
                section.set("end", ugo.getEndName());
                section.set("backpack", ugo.getBackpack());
                section.set("enderchest", ugo.getEnderchest());
                section.set("worldtype", ugo.getWorldType().name());
                Debug.logDebug("worldtype set to " + ugo.getWorldType().name());
                section.set("structures", ugo.getStructures());
                section.set("seed", ugo.getSeed());
                try {
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateSettingUpWorld, section.getName());
                    SimpleWorld.setupUniverse(section);
                    SimpleWorldManager.getManager().saveWorldsConfig();
                    StringContainer.sendMessage(sender, StringType.worldManagerCreateWorldCreated);
                } catch (SimplyModularFatalException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
