/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimplePermissions;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRule;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jnbt.*;

public class UserAddPerm extends SimpleCommand {
    
    public UserAddPerm(SimpleModule module) {
        super(module, SimpleCommandMetadata.useraddperm);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
        OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender, true);
        if (target == null) { return true; }
        String permission = args[1];
        User user = getUser(target.getName());
        ListTag perms = user.getCompoundTag(module).getList("personalPermissions");
        ArrayList<String> actualPerms = new ArrayList<String>();
        for (Tag t : perms.getValue()) {
            actualPerms.add(((StringTag) t).getValue());
        }
        if (actualPerms.contains(permission)) {
            StringContainer.sendMessage(sender, StringType.permissionPersonalAddPermissionPermissionExists);
            return true;
        }
        if (permission.startsWith("-")) {
            String peerm = permission.substring(1, permission.length());
            actualPerms.remove(peerm);
            actualPerms.add(permission);
        } else {
            actualPerms.remove("-" + permission);
            actualPerms.add(permission);
        }
        perms = new ListTag("personalPermissions", StringTag.class);
        for (String perm : actualPerms) {
            perms.addTag(new StringTag(perm, perm));
        }
        user.getCompoundTag(module).setList("personalPermissions", perms);
        user.save();
        if (target.isOnline()) {
            SimplePermissions.getPermissionsManager().reloadPermissionsForPlayer((Player) target);
        }
        StringContainer.sendMessage(sender, StringType.permissionPersonalAddPermission, permission, target.getName());
        return true;
    }
    
    protected void setTabComplete(SimpleTabRuleset ruleset) {
        ruleset.addTabRule(0, new SimpleTabRule() {
            public List<String> tab(CommandSender sender, SimpleCommandMetadata cmd, String argBit, String[] args) throws SimplyModularFatalModuleException {
                ArrayList<String> players = new ArrayList<String>();
                for (Player player : Bukkit.getOnlinePlayers()) {
                    Group g = getGroupByPlayer(player);
                    if (sender.hasPermission(g.getPerm2add2())) {
                        players.add(player.getName());
                    }
                }
                return players;
            }
        });
    }
    
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
