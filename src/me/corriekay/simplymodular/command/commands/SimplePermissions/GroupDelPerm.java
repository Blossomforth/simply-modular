/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimplePermissions;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRule;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;

public class GroupDelPerm extends SimpleCommand {
    
    public GroupDelPerm(SimpleModule module) {
        super(module, SimpleCommandMetadata.groupdelperm);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
        Group group;
        World world;
        String permission;
        group = getGroupByName(args[0]);
        if (args.length > 2) {
            world = Bukkit.getWorld(args[1]);
            if (world == null) {
                StringContainer.sendMessage(sender, StringType.worldNotFound, args[1]);
                return true;
            }
            permission = args[2];
        } else {
            world = null;
            permission = args[1];
        }
        String path = "Groups." + group.getName() + "." + (world == null ? "globalPermissions" : "worldPermissions." + world.getName());
        FileConfiguration config = SimplePermissions.getPermissionsManager().getConfig();
        java.util.List<String> perms = config.getStringList(path);
        if (!perms.contains(permission)) {
            StringContainer.sendMessage(sender, StringType.permissionRemoveGroupPermissionDoesNotExist);
            return true;
        }
        perms.remove(permission);
        config.set(path, perms);
        SimplePermissions.getPermissionsManager().saveConfig();
        SimplePermissions.getPermissionsManager().reloadPermissions();
        StringContainer.sendMessage(sender, StringType.permissionRemoveGroupPermission, permission, group.getName(), (world == null ? "Global" : world.getName()));
        return true;
    }
    
    protected void setTabComplete(SimpleTabRuleset ruleset) {
        ruleset.addTabRule(0, new SimpleTabRule() {
            public List<String> tab(CommandSender sender, SimpleCommandMetadata cmd, String argBit, String[] args) throws SimplyModularFatalModuleException {
                ArrayList<String> groups = new ArrayList<String>();
                for (Group g : SimplePermissions.getPermissionsManager().getGroups()) {
                    if (sender.hasPermission(g.getPerm2add2())) {
                        groups.add(g.getName());
                    }
                }
                return groups;
            }
        });
    }
    
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        
        argRuleset.addRule(new SimpleArgumentRule(null, false, true) {
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
                Group group = getGroupByName(args[0]);
                if (group == null) {
                    StringContainer.sendMessage(sender, StringType.permissionNoGroupFound, args[0]);
                    return false;
                }
                return true;
            }
        });
        argRuleset.addRule(new SimpleArgumentRule(StringContainer.getMessage(StringType.denyNoPermission), true, true) {
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
                Group group = getGroupByName(args[0]);
                return sender.hasPermission(group.getPerm2add2());
            }
        });
    }
    
}
