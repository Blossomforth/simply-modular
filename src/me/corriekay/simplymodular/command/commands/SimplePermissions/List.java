/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimplePermissions;

import java.util.ArrayList;
import java.util.HashMap;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.optional.SimpleInvisibility.SimpleInvisibility;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class List extends SimpleCommand {
    public List(SimpleModule module) {
        super(module, SimpleCommandMetadata.list);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
        int numberPlayers = 0;
        HashMap<String, ArrayList<String>> usermap = new HashMap<String, ArrayList<String>>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            boolean canSee;
            if (SimpleInvisibility.getModule() != null && sender instanceof Player && !((Player) sender).getName().equals(player.getName())) {
                canSee = SimpleInvisibility.getModule().canCurrentlySee((Player) sender, player);
            } else {
                canSee = true;
            }
            if (canSee) {
                numberPlayers++;
                Group g = getGroupByPlayer(player);
                ArrayList<String> players = usermap.get(g.getName());
                if (players == null) {
                    players = new ArrayList<String>();
                    usermap.put(g.getName(), players);
                }
                if (!players.contains(player.getDisplayName())) {
                    players.add(player.getDisplayName());
                }
            }
        }
        StringContainer.sendMessage(sender, StringType.listNumberPlayers, numberPlayers, Bukkit.getServer().getMaxPlayers());
        for (String gName : SimplePermissions.getPermissionsManager().getConfig().getStringList("listOrder")) {
            if (!(usermap.get(gName) == null || usermap.get(gName).size() <= 0)) {
                Group group = getGroupByName(gName);
                sender.sendMessage(group.getListName() + ChatColor.WHITE + ": " + Utils.joinStringArray(usermap.get(group.getName()).toArray(new String[] {}), ChatColor.WHITE + ", "));
            }
        }
        return true;
    }
    
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
