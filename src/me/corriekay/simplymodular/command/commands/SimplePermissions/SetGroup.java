/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimplePermissions;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRule;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRule;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.events.GroupChangeEvent;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetGroup extends SimpleCommand {
    
    public SetGroup(SimpleModule module) {
        super(module, SimpleCommandMetadata.setgroup);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
        OfflinePlayer player = this.getOnlineOfflinePlayer(args[0], sender, true);
        if (player == null) { return true; }
        User user = getUser(player.getName());
        Group g = getGroupByName(user.getCompoundTag(module).getString("group"));
        if (!sender.hasPermission(g.getPerm2add2())) {
            StringContainer.sendMessage(sender, StringType.denyNoPermission);
            return true;
        }
        if (args[1].equalsIgnoreCase(g.getName())) {
            StringContainer.sendMessage(sender, StringType.groupSetDenyAlreadyInGroup, player.getName(), g.getName());
            return true;
        }
        Group g2 = getGroupByName(args[1]);
        if (g2.isRequiresConsole() && sender instanceof Player) {
            StringContainer.sendMessage(sender, StringType.groupSetDenyRequiresConsole);
            return true;
        }
        user.getCompoundTag(module).setString("group", g2.getName());
        user.save();
        for (Player player2 : Bukkit.getOnlinePlayers()) {
            if (!player2.getName().equals(player.getName())) {
                if (player2.hasPermission(this.getPermission())) {
                    StringContainer.sendMessage(player2, StringType.groupSetApproveMessage, player.getName(), g.getName(), g2.getName());
                }
            }
        }
        StringContainer.sendMessage(Bukkit.getConsoleSender(), StringType.groupSetApproveMessage, player.getName(), g.getName(), g2.getName());
        if (player.isOnline()) {
            StringContainer.sendMessage((Player) player, StringType.groupSetApprovePlayerMessage, g2.getName(), g.getName());
            SimplePermissions.getPermissionsManager().reloadPermissionsForPlayer((Player) player);
        }
        GroupChangeEvent gce = new GroupChangeEvent(user, g, g2, player.getName());
        Bukkit.getPluginManager().callEvent(gce);
        return true;
    }
    
    protected void setTabComplete(SimpleTabRuleset ruleset) {
        ruleset.addTabRule(1, new SimpleTabRule() {
            public List<String> tab(CommandSender sender, SimpleCommandMetadata cmd, String argBit, String[] args) throws SimplyModularFatalModuleException {
                List<String> returns = new ArrayList<String>();
                for (Group g : SimplePermissions.getPermissionsManager().getGroups()) {
                    if (sender.hasPermission(g.getPerm2add2())) {
                        returns.add(g.getName());
                    }
                }
                return returns;
            }
            
        });
    }
    
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(null, true, true) {
            public boolean checkRule(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
                String gString = args[args.length - 1];
                Group group = getGroupByName(gString);
                if (group == null) { return false; }
                return sender.hasPermission(group.getPerm2add2());
            }
            
            public String getFailureMessage(CommandSender sender, String[] args) throws SimplyModularFatalModuleException {
                Group group = getGroupByName(args[args.length - 1]);
                return group == null ? StringContainer.getMessage(StringType.permissionNoGroupFound, args[args.length - 1]) : StringContainer.getMessage(StringType.denyNoPermission);
            }
        });
    }
}
