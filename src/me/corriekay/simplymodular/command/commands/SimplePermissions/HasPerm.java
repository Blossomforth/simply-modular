/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimplePermissions;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Permission;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HasPerm extends SimpleCommand {
    
    public HasPerm(SimpleModule module) {
        super(module, SimpleCommandMetadata.hasperm);
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) {
        Player player = getOnlinePlayer(args[0], sender);
        if (player == null) { return true; }
        Permission permission = SimplePermissions.getPermissionsManager().getPlayerPermissionHistory(player, args[1]);
        if (permission != null) {
            sender.sendMessage("History for permission " + args[1] + " for player " + player.getName() + "\n");
            for (String history : permission.getHistory()) {
                player.sendMessage(history);
            }
        }
        sender.sendMessage("Permission: " + args[1] + ". Flagging: " + player.hasPermission(args[1]));
        return true;
    }
    
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
