/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleInvisibility;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.optional.SimpleInvisibility.SimpleInvisibility;
import me.corriekay.simplymodular.utils.SimplyModularFatalException;
import me.corriekay.simplymodular.utils.SimplyModularFatalModuleException;

import org.bukkit.entity.Player;

public class Hide extends SimpleCommand {
    
    private final String fakehide;
    
    public Hide(String alias, SimpleModule module) throws SimplyModularFatalException {
        super(module, SimpleCommandMetadata.hide);
        fakehide = alias;
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        boolean silent = !label.equals(fakehide);
        SimpleInvisibility.getModule().toggleInvisibility(player, silent, true, false);
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
