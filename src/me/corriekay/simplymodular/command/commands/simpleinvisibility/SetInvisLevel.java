/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.SimpleInvisibility;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.*;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.SimplePermissions.Group;
import me.corriekay.simplymodular.module.core.SimplePermissions.SimplePermissions;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.optional.SimpleInvisibility.SimpleInvisibility;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.command.CommandSender;
import org.jnbt.CompoundTag;

public class SetInvisLevel extends SimpleCommand {
    
    private String defaultinvislevelAlias;
    
    public SetInvisLevel(String alias, SimpleModule module) throws SimplyModularFatalException {
        super(module, SimpleCommandMetadata.setinvislevel);
        defaultinvislevelAlias = alias;
    }
    
    public boolean optionalCommand(CommandSender sender, String label, String[] args) throws SimplyModularFatalModuleException {
        String target = getOfflinePlayer(args[0], sender, true);
        if (target == null) { return true; }
        User user = getUser(target);
        CompoundTag tag = user.getCompoundTag(module);
        if (label.equals(defaultinvislevelAlias)) {
            if (!tag.getBoolean("manualInvisLevel")) {
                StringContainer.sendMessage(sender, StringType.invisibilityDefaultInvisLevelAlreadyDefault, target);
            } else {
                tag.setBoolean("manualInvisLevel", false);
                Group g = getGroupByName(user.getCompoundTag(SimplePermissions.getPermissionsManager()).getString("group"));
                tag.setInt("invisibilityLevel", g.getInvisibilityLevel());
                StringContainer.sendMessage(sender, StringType.invisibilityDefaultInvisLevel, target, g.getInvisibilityLevel());
                SimpleInvisibility.getModule().calculateInvisibilityAll(false, true);
            }
        } else {
            int level = Integer.parseInt(args[1]);
            if (tag.getInt("invisibilityLevel") == level && tag.getBoolean("manualInvisLevel")) {
                StringContainer.sendMessage(sender, StringType.invisibilitySetInvisLevelSameLevel, target, level);
            } else {
                tag.setBoolean("manualInvisLevel", true);
                tag.setInt("invisibilityLevel", level);
                SimpleInvisibility.getModule().calculateInvisibilityAll(false, true);
                StringContainer.sendMessage(sender, StringType.invisibilitySetInvisLevel, target, level);
            }
        }
        user.save();
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {
        argRuleset.addRule(new SimpleArgumentRule(StringContainer.getMessage(StringType.denyMinArgs), true, true) {
            public boolean checkRule(CommandSender sender, String label, String[] args) {
                if (!label.equals(defaultinvislevelAlias)) {
                    if (args.length < 2) { return false; }
                }
                return true;
            }
        });
        argRuleset.addRule(new IntegerValidator(StringContainer.getMessage(StringType.denyNotNumber), true, true, 1) {
            public boolean checkRule(CommandSender sender, String label, String[] args) {
                if (label.equals(defaultinvislevelAlias)) {
                    Debug.logDebug("check");
                    return true;
                }
                return super.checkRule(sender, label, args);
            }
        });
    }
}
