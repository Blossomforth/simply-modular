/**    Simply Modular is a bukkit plugin designed to help administrate and run minecraft servers.
*    Copyright (C) 2014  Travis Barrett
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package me.corriekay.simplymodular.command.commands.UserManager;

import me.corriekay.simplymodular.command.SimpleCommand;
import me.corriekay.simplymodular.command.SimpleCommandMetadata;
import me.corriekay.simplymodular.command.validation.arguments.SimpleArgumentRuleset;
import me.corriekay.simplymodular.command.validation.tabbing.SimpleTabRuleset;
import me.corriekay.simplymodular.module.SimpleModule;
import me.corriekay.simplymodular.module.core.usermanager.User;
import me.corriekay.simplymodular.module.core.usermanager.UserManager;
import me.corriekay.simplymodular.utils.*;
import me.corriekay.simplymodular.utils.StringContainer.StringType;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.jnbt.*;

public class NicknameCommand extends SimpleCommand {
    
    public NicknameCommand(SimpleModule module) {
        super(module, SimpleCommandMetadata.nick);
    }
    
    public boolean playerCommand(Player player, String label, String[] args) throws SimplyModularFatalModuleException {
        String nickname = Utils.joinStringArray(args, " ");
        nickname = nickname.replaceAll("&k", "").replaceAll("&0", "");
        
        nickname = ChatColor.translateAlternateColorCodes('&', nickname);
        
        User user = UserManager.getManager().getUser(player);
        CompoundTag tag = user.getCompoundTag(module);
        
        ListTag nickHistory = tag.getList("nicknameHistory");
        boolean alreadyExists = true;
        for (Tag nicktag : nickHistory.getValue()) {
            String n = ((StringTag) nicktag).getValue();
            if (n.equals(nickname)) {
                alreadyExists = true;
                break;
            }
        }
        if (!alreadyExists) {
            nickHistory.addTag(new StringTag("", nickname));
        }
        tag.setString("nickname", nickname);
        player.setDisplayName(nickname);
        StringContainer.sendMessage(player, StringType.nicknameSet, nickname);
        user.save();
        return true;
    }
    
    @Override
    protected void setTabComplete(SimpleTabRuleset ruleset) {}
    
    @Override
    protected void setArgValidator(SimpleArgumentRuleset argRuleset) {}
    
}
